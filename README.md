# WHATWG-Encoding

C++ implementation of [WHATWG Encoding Standard](https://encoding.spec.whatwg.org/)  
Base on specification [last updated 2 September 2018](https://encoding.spec.whatwg.org/commit-snapshots/fe4934c2eb49a2e0b3a630c35b9fa23f7cc16fc0/)

# Requirement
- C++17 compliant compiler
- [Boost library](https://www.boost.org/)
- [CMake](https://cmake.org/) 3.3+
- [WHATWG Infra library](https://gitlab.com/stream9/whatwg_infra)
- [Doxygen](http://doxygen.nl/) (if you want generate API documentation)

# Getting Started
1. Download source code or git clone or git submodule etc..
2. Import into your project's CMakeLists.txt with add_subdirectory()
3. Add "**whatwg_encoding**" target to your project with target_link_libraries()

# [API Document](https://stream9.gitlab.io/whatwg_encoding/index.html)

# Note
- Stream API isn't implemented yet.

# TODO
- Implement stream API
- More test
