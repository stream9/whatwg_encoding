#include <whatwg/encoding/text_encoder.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::encoding::testing {

BOOST_AUTO_TEST_SUITE(text_encoder_)

    BOOST_AUTO_TEST_CASE(construction)
    {
        text_encoder const e;

        BOOST_CHECK_EQUAL(e.encoding_name(), "utf-8");
    }

    BOOST_AUTO_TEST_CASE(encode)
    {
        text_encoder e;

        auto r = e.encode(U"日本語");

        BOOST_CHECK_EQUAL(r, u8"日本語");
    }

BOOST_AUTO_TEST_SUITE_END() // text_encoder_

} // namespace whatwg::encoding::testing
