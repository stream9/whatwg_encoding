#include <whatwg/encoding/stream.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::encoding::testing {

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(stream_t_)

    BOOST_AUTO_TEST_CASE(push_)
    {
        stream_t os;
        os.push(U'a');

        BOOST_CHECK(os.to_string() == U"a");
    }

    BOOST_AUTO_TEST_CASE(to_string_)
    {
        stream_t os;
        os.push(U'a');

        auto const copy = os.to_string();
        BOOST_CHECK(copy == U"a");

        auto const moved = std::move(os).to_string();
        BOOST_CHECK(moved == U"a");
        BOOST_CHECK(os.to_string() == U"");
    }

    BOOST_AUTO_TEST_CASE(push_result)
    {
        stream_t os;
        result_t r = U'a';

        push(os, r);
        BOOST_CHECK(os.to_string() == U"a");
    }

BOOST_AUTO_TEST_SUITE_END() // stream_t_

BOOST_AUTO_TEST_SUITE(byte_stream_t_)

    BOOST_AUTO_TEST_CASE(push_)
    {
        byte_stream_t os;
        os.push('a');

        BOOST_CHECK(os.to_string() == "a");
    }

    BOOST_AUTO_TEST_CASE(to_string_)
    {
        byte_stream_t os;
        os.push('a');

        auto const copy = os.to_string();
        BOOST_CHECK(copy == "a");

        auto const moved = std::move(os).to_string();
        BOOST_CHECK(moved == "a");
        BOOST_CHECK(os.to_string() == "");
    }

    BOOST_AUTO_TEST_CASE(push_result)
    {
        byte_stream_t os;

        result_t r { "1234" };

        push(os, r);
        BOOST_CHECK(os.to_string() == "1234");
    }

BOOST_AUTO_TEST_SUITE_END() // stream_t_

BOOST_AUTO_TEST_SUITE(stream_t_)

    BOOST_AUTO_TEST_CASE(prepend_end_of_stream)
    {
        stream_t is;

        BOOST_CHECK(is.read() == end_of_stream);
        prepend(is, end_of_stream);
        BOOST_CHECK(is.read() == end_of_stream);
    }

BOOST_AUTO_TEST_SUITE_END() // stream_t_

BOOST_AUTO_TEST_SUITE(basic_stream_)

    BOOST_AUTO_TEST_CASE(constructor_)
    {
        byte_stream_t const s1;
        BOOST_REQUIRE(s1.empty());

        byte_stream_t const s2 { "123" };
        BOOST_REQUIRE_EQUAL(s2.size(), 3);
        BOOST_CHECK_EQUAL(s2.to_string_view(), "123");
    }

    BOOST_AUTO_TEST_CASE(read_)
    {
        byte_stream_t s { "\x01\x02\x03"sv };

        BOOST_CHECK_EQUAL(read(s), 0x01u);
        BOOST_CHECK_EQUAL(s.to_string_view(), "\x02\x03"sv);
    }

    BOOST_AUTO_TEST_CASE(push_)
    {
        byte_stream_t s;

        push(s, 0x00u);
        push(s, 0x01u);
        push(s, 0x02u);

        BOOST_CHECK_EQUAL(s.to_string_view(), "\x00\x01\x02"sv);
    }

    BOOST_AUTO_TEST_CASE(prepend_)
    {
        byte_stream_t s;

        prepend(s, 0x00u);
        prepend(s, 0x01u);
        prepend(s, 0x02u);
        BOOST_CHECK_EQUAL(s.to_string_view(), "\x02\x01\x00"sv);

        read(s);
        prepend(s, 0x03u);
        BOOST_CHECK_EQUAL(s.to_string_view(), "\x03\x01\x00"sv);
    }

    BOOST_AUTO_TEST_CASE(to_string_)
    {
        code_point_stream_t s;
        push(s, 0x01u);

        auto const copy = s.to_string();
        BOOST_CHECK(copy == U"\x01"sv);

        auto const moved = std::move(s).to_string();
        BOOST_CHECK(moved == U"\x01"sv);
        BOOST_CHECK(s.empty());

        push(s, 0x02u);
        BOOST_CHECK(s.to_string_view() == U"\x02"sv);
    }

BOOST_AUTO_TEST_SUITE_END() // basic_stream_

} // namespace whatwg::encoding::testing
