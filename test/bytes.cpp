#include <whatwg/encoding/bytes.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::encoding::testing {

BOOST_AUTO_TEST_SUITE(bytes_t_)

    BOOST_AUTO_TEST_CASE(initial)
    {
        bytes_t const b;

        BOOST_CHECK(b.to_string_view().empty());
    }

    BOOST_AUTO_TEST_CASE(append_)
    {
        bytes_t b;

        append(b, '1');
        BOOST_CHECK_EQUAL(b.to_string_view().size(), 1);
        BOOST_CHECK_EQUAL(b.to_string_view(), "1");

        append(b, '2');
        BOOST_CHECK_EQUAL(b.to_string_view().size(), 2);
        BOOST_CHECK_EQUAL(b.to_string_view(), "12");

        append(b, '3');
        BOOST_CHECK_EQUAL(b.to_string_view().size(), 3);
        BOOST_CHECK_EQUAL(b.to_string_view(), "123");

        append(b, '4');
        BOOST_CHECK_EQUAL(b.to_string_view().size(), 4);
        BOOST_CHECK_EQUAL(b.to_string_view(), "1234");
    }

    BOOST_AUTO_TEST_CASE(conversion_to_string_view)
    {
        bytes_t b;

        append(b, '1');
        BOOST_CHECK_EQUAL(b.to_string_view().size(), 1);
        BOOST_CHECK_EQUAL(b.to_string_view(), "1");
        BOOST_CHECK_EQUAL(static_cast<byte_sequence_view_t>(b), "1");
    }

    BOOST_AUTO_TEST_CASE(stream_operator)
    {
        bytes_t b;
        append(b, 'A');

        std::ostringstream oss;
        oss << b;
        BOOST_CHECK_EQUAL(oss.str(), "{ 0x41, }");
    }

BOOST_AUTO_TEST_SUITE_END() // bytes_t_

} // namespace whatwg::encoding::testing
