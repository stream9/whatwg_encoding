#include <whatwg/encoding/result.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::encoding::testing {

BOOST_AUTO_TEST_SUITE(result_)

    BOOST_AUTO_TEST_CASE(finished_)
    {
        result_t const r = finished;

        BOOST_CHECK(r.is_finished());
        BOOST_CHECK(!r.is_continue());
        BOOST_CHECK(!r.is_error());
        BOOST_CHECK(!r.is_tokens());
    }

    BOOST_AUTO_TEST_CASE(continue__)
    {
        result_t const r = continue_;

        BOOST_CHECK(!r.is_finished());
        BOOST_CHECK(r.is_continue());
        BOOST_CHECK(!r.is_error());
        BOOST_CHECK(!r.is_tokens());
    }

    BOOST_AUTO_TEST_CASE(error_without_code_point)
    {
        result_t const r = error;

        BOOST_CHECK(!r.is_finished());
        BOOST_CHECK(!r.is_continue());
        BOOST_CHECK(!r.is_tokens());
        BOOST_CHECK(r.is_error());
        BOOST_CHECK(r.empty());
    }

    BOOST_AUTO_TEST_CASE(error_with_code_point)
    {
        result_t const r { error, U'A' };

        BOOST_CHECK(!r.is_finished());
        BOOST_CHECK(!r.is_continue());
        BOOST_CHECK(!r.is_tokens());
        BOOST_CHECK(r.is_error());

        BOOST_CHECK(!r.empty());
        BOOST_CHECK_EQUAL(r.size(), 1);
        BOOST_CHECK_EQUAL(r[0], U'A');
    }

    BOOST_AUTO_TEST_CASE(one_byte)
    {
        result_t const r = '\xFF';

        BOOST_CHECK(!r.is_finished());
        BOOST_CHECK(!r.is_continue());
        BOOST_CHECK(!r.is_error());
        BOOST_CHECK(r.is_tokens());

        BOOST_CHECK(!r.empty());
        BOOST_CHECK_EQUAL(r[0], U'\xFF');
    }

    BOOST_AUTO_TEST_CASE(one_code_point)
    {
        result_t const r = U'B';

        BOOST_CHECK(!r.is_finished());
        BOOST_CHECK(!r.is_continue());
        BOOST_CHECK(!r.is_error());
        BOOST_CHECK(r.is_tokens());

        BOOST_CHECK(!r.empty());
        BOOST_CHECK_EQUAL(r[0], U'B');
    }

    BOOST_AUTO_TEST_CASE(two_bytes)
    {
        result_t const r = { '\x80', '\x81' };

        BOOST_CHECK(!r.is_finished());
        BOOST_CHECK(!r.is_continue());
        BOOST_CHECK(!r.is_error());
        BOOST_CHECK(r.is_tokens());

        BOOST_CHECK_EQUAL(r.size(), 2);
        BOOST_CHECK_EQUAL(r[0], U'\x80');
        BOOST_CHECK_EQUAL(r[1], U'\x81');
    }

    BOOST_AUTO_TEST_CASE(two_code_points)
    {
        result_t const r = { U'1', U'2' };

        BOOST_CHECK(!r.is_finished());
        BOOST_CHECK(!r.is_continue());
        BOOST_CHECK(!r.is_error());
        BOOST_CHECK(r.is_tokens());

        BOOST_CHECK_EQUAL(r.size(), 2);
        BOOST_CHECK_EQUAL(r[0], U'1');
        BOOST_CHECK_EQUAL(r[1], U'2');
    }

    BOOST_AUTO_TEST_CASE(four_bytes)
    {
        result_t const r = { '\x80', '\x81', '\x82', '\x83' };

        BOOST_CHECK(!r.is_finished());
        BOOST_CHECK(!r.is_continue());
        BOOST_CHECK(!r.is_error());
        BOOST_CHECK(r.is_tokens());

        BOOST_CHECK_EQUAL(r.size(), 4);
        BOOST_CHECK_EQUAL(r[0], U'\x80');
        BOOST_CHECK_EQUAL(r[1], U'\x81');
        BOOST_CHECK_EQUAL(r[2], U'\x82');
        BOOST_CHECK_EQUAL(r[3], U'\x83');
    }

    BOOST_AUTO_TEST_CASE(equal_)
    {
        result_t const a = { 0x01u };
        result_t const b1 = { 0x01u };

        BOOST_CHECK(a != error);
        BOOST_CHECK(a == b1);

        result_t const b2 = { 0x01u, 0x02u };
        BOOST_CHECK(a != b2);
    }

BOOST_AUTO_TEST_SUITE_END() // result_

} // namespace whatwg::encoding::testing
