#include <whatwg/encoding/encoding.hpp>

#include <whatwg/encoding/stream.hpp>
#include <whatwg/encoding/encodings/utf8.hpp>
#include <whatwg/encoding/encodings/single_byte.hpp>
#include <whatwg/encoding/encodings/chinese_simplified.hpp>
#include <whatwg/encoding/encodings/chinese_traditional.hpp>
#include <whatwg/encoding/encodings/euc_jp.hpp>
#include <whatwg/encoding/encodings/iso_2022_jp.hpp>
#include <whatwg/encoding/encodings/shift_jis.hpp>
#include <whatwg/encoding/encodings/euc_kr.hpp>
#include <whatwg/encoding/encodings/replacement.hpp>
#include <whatwg/encoding/encodings/utf16.hpp>
#include <whatwg/encoding/encodings/user_defined.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::encoding::testing {

BOOST_AUTO_TEST_SUITE(get_an_encoding_)

    BOOST_AUTO_TEST_CASE(failure_)
    {
        auto e = get_an_encoding("utf64");
        BOOST_CHECK(!e);

        e = get_an_encoding("zzz");
        BOOST_CHECK(!e);

        e = get_an_encoding("");
        BOOST_CHECK(!e);
    }

    BOOST_AUTO_TEST_CASE(case_insensitive)
    {
        auto e = get_an_encoding("Unicode-1-1-Utf-8");
        BOOST_CHECK(e);

        BOOST_CHECK_EQUAL(e->name(), "UTF-8");
    }

    template<typename Encoder, typename Decoder>
    void test_encoding(
        byte_sequence_view_t const& name,
        std::vector<byte_sequence_view_t> const& labels)
    {
        for (auto const& label: labels) {
            BOOST_TEST_CONTEXT("label: " << label) {
                auto const encoding = get_an_encoding(label);

                BOOST_REQUIRE(encoding);

                BOOST_CHECK_EQUAL(encoding->name(), name);

                auto* const e = encoding->encoder();
                if constexpr (!std::is_same_v<Encoder, void>) {
                    BOOST_CHECK(dynamic_cast<Encoder const*>(e));
                }
                else {
                    BOOST_CHECK(!e);
                }

                auto& d = encoding->decoder();
                BOOST_CHECK(dynamic_cast<Decoder const*>(&d));
            }
        }
    }

    BOOST_AUTO_TEST_CASE(utf_8)
    {
        test_encoding<utf8::encoder, utf8::decoder>(
            "UTF-8", { "unicode-1-1-utf-8", "utf-8", "utf8" }
        );
    }

    static void
    test_single_byte(byte_sequence_view_t const& name,
                     std::vector<byte_sequence_view_t> const& labels)
    {
        test_encoding<single_byte::encoder, single_byte::decoder>(
            name, labels
        );
    }

    BOOST_AUTO_TEST_CASE(ibm866)
    {
        test_single_byte("IBM866", {
            "866", "cp866", "csibm866", "ibm866" });
    }

    BOOST_AUTO_TEST_CASE(iso_8859_2)
    {
        test_single_byte("ISO-8859-2", {
            "csisolatin2", "iso-8859-2", "iso-ir-101", "iso8859-2",
            "iso88592", "iso_8859-2", "iso_8859-2:1987", "l2", "latin2"
        });
    }

    BOOST_AUTO_TEST_CASE(iso_8859_3)
    {
        test_single_byte("ISO-8859-3", {
            "csisolatin3", "iso-8859-3", "iso-ir-109", "iso8859-3",
            "iso88593", "iso_8859-3", "iso_8859-3:1988", "l3", "latin3"
        });
    }

    BOOST_AUTO_TEST_CASE(iso_8859_4)
    {
        test_single_byte("ISO-8859-4", {
            "csisolatin4", "iso-8859-4", "iso-ir-110", "iso8859-4",
            "iso88594", "iso_8859-4", "iso_8859-4:1988", "l4", "latin4"
        });
    }

    BOOST_AUTO_TEST_CASE(iso_8859_5)
    {
        test_single_byte("ISO-8859-5", {
            "csisolatincyrillic", "cyrillic", "iso-8859-5", "iso-ir-144",
            "iso8859-5", "iso88595", "iso_8859-5", "iso_8859-5:1988"
        });
    }

    BOOST_AUTO_TEST_CASE(iso_8859_6)
    {
        test_single_byte("ISO-8859-6", {
            "arabic", "asmo-708", "csiso88596e", "csiso88596i",
            "csisolatinarabic", "ecma-114", "iso-8859-6", "iso-8859-6-e",
            "iso-8859-6-i", "iso-ir-127", "iso8859-6", "iso88596",
            "iso_8859-6", "iso_8859-6:1987"
        });
    }

    BOOST_AUTO_TEST_CASE(iso_8859_7)
    {
        test_single_byte("ISO-8859-7", {
            "csisolatingreek", "ecma-118", "elot_928", "greek", "greek8",
            "iso-8859-7", "iso-ir-126", "iso8859-7", "iso88597", "iso_8859-7",
            "iso_8859-7:1987", "sun_eu_greek"
        });
    }

    BOOST_AUTO_TEST_CASE(iso_8859_8)
    {
        test_single_byte("ISO-8859-8", {
            "csiso88598e", "csisolatinhebrew", "hebrew", "iso-8859-8",
            "iso-8859-8-e", "iso-ir-138", "iso8859-8", "iso88598",
            "iso_8859-8", "iso_8859-8:1988", "visual"
        });
    }

    BOOST_AUTO_TEST_CASE(iso_8859_8_i)
    {
        test_single_byte("ISO-8859-8-I", {
            "csiso88598i", "iso-8859-8-i", "logical"
        });
    }

    BOOST_AUTO_TEST_CASE(iso_8859_10)
    {
        test_single_byte("ISO-8859-10", {
            "csisolatin6", "iso-8859-10", "iso-ir-157", "iso8859-10",
            "iso885910", "l6", "latin6"
        });
    }

    BOOST_AUTO_TEST_CASE(iso_8859_13)
    {
        test_single_byte("ISO-8859-13", {
            "iso-8859-13", "iso8859-13", "iso885913"
        });
    }

    BOOST_AUTO_TEST_CASE(iso_8859_14)
    {
        test_single_byte("ISO-8859-14", {
            "iso-8859-14", "iso8859-14", "iso885914"
        });
    }

    BOOST_AUTO_TEST_CASE(iso_8859_15)
    {
        test_single_byte("ISO-8859-15", {
            "csisolatin9", "iso-8859-15", "iso8859-15", "iso885915",
            "iso_8859-15", "l9"
        });
    }

    BOOST_AUTO_TEST_CASE(iso_8859_16)
    {
        test_single_byte("ISO-8859-16", { "iso-8859-16" });
    }

    BOOST_AUTO_TEST_CASE(koi8_r)
    {
        test_single_byte("KOI8-R", {
            "cskoi8r", "koi", "koi8", "koi8-r", "koi8_r"
        });
    }

    BOOST_AUTO_TEST_CASE(koi8_8u)
    {
        test_single_byte("KOI8-U", {
            "koi8-ru", "koi8-u"
        });
    }

    BOOST_AUTO_TEST_CASE(macintosh)
    {
        test_single_byte("macintosh", {
            "csmacintosh", "mac", "macintosh", "x-mac-roman"
        });
    }

    BOOST_AUTO_TEST_CASE(windows_874)
    {
        test_single_byte("windows-874", {
            "dos-874", "iso-8859-11", "iso8859-11", "iso885911", "tis-620",
            "windows-874"
        });
    }

    BOOST_AUTO_TEST_CASE(windows_1250)
    {
        test_single_byte("windows-1250", {
            "cp1250", "windows-1250", "x-cp1250"
        });
    }

    BOOST_AUTO_TEST_CASE(windows_1251)
    {
        test_single_byte("windows-1251", {
            "cp1251", "windows-1251", "x-cp1251"
        });
    }

    BOOST_AUTO_TEST_CASE(windows_1252)
    {
        test_single_byte("windows-1252", {
            "ansi_x3.4-1968", "ascii", "cp1252", "cp819", "csisolatin1",
            "ibm819", "iso-8859-1", "iso-ir-100", "iso8859-1", "iso88591",
            "iso_8859-1", "iso_8859-1:1987", "l1", "latin1", "us-ascii",
            "windows-1252", "x-cp1252"
        });
    }

    BOOST_AUTO_TEST_CASE(windows_1253)
    {
        test_single_byte("windows-1253", {
            "cp1253", "windows-1253", "x-cp1253"
        });
    }

    BOOST_AUTO_TEST_CASE(windows_1254)
    {
        test_single_byte("windows-1254", {
            "cp1254", "csisolatin5", "iso-8859-9", "iso-ir-148", "iso8859-9",
            "iso88599", "iso_8859-9", "iso_8859-9:1989", "l5", "latin5",
            "windows-1254", "x-cp1254"
        });
    }

    BOOST_AUTO_TEST_CASE(windows_1255)
    {
        test_single_byte("windows-1255", {
            "cp1255", "windows-1255", "x-cp1255"
        });
    }

    BOOST_AUTO_TEST_CASE(windows_1256)
    {
        test_single_byte("windows-1256", {
            "cp1256", "windows-1256", "x-cp1256"
        });
    }

    BOOST_AUTO_TEST_CASE(windows_1257)
    {
        test_single_byte("windows-1257", {
            "cp1257", "windows-1257", "x-cp1257"
        });
    }

    BOOST_AUTO_TEST_CASE(windows_1258)
    {
        test_single_byte("windows-1258", {
            "cp1258", "windows-1258", "x-cp1258"
        });
    }

    BOOST_AUTO_TEST_CASE(x_mac_cryillic)
    {
        test_single_byte("x-mac-cyrillic", {
            "x-mac-cyrillic", "x-mac-ukrainian"
        });
    }

    BOOST_AUTO_TEST_CASE(gbk)
    {
        using namespace chinese;

        test_encoding<gbk_encoder, gbk_decoder>(
            "GBK", {
                "chinese", "csgb2312", "csiso58gb231280", "gb2312",
                "gb_2312", "gb_2312-80", "gbk", "iso-ir-58", "x-gbk"
            }
        );
    }

    BOOST_AUTO_TEST_CASE(gb18030)
    {
        using namespace chinese;

        test_encoding<gb18030_encoder, gb18030_decoder>(
            "gb18030", { "gb18030" }
        );
    }

    BOOST_AUTO_TEST_CASE(big5)
    {
        using namespace chinese;

        test_encoding<big5_encoder, big5_decoder>(
            "Big5", {
                "big5", "big5-hkscs", "cn-big5", "csbig5", "x-x-big5"
            }
        );
    }

    BOOST_AUTO_TEST_CASE(euc_jp)
    {
        using namespace japanese;

        test_encoding<euc_jp_encoder, euc_jp_decoder>(
            "EUC-JP", {
                "cseucpkdfmtjapanese", "euc-jp", "x-euc-jp"
            }
        );
    }

    BOOST_AUTO_TEST_CASE(iso_2022_jp)
    {
        using namespace japanese;

        test_encoding<iso_2022_jp_encoder, iso_2022_jp_decoder>(
            "ISO-2022-JP", {
                "csiso2022jp", "iso-2022-jp"
            }
        );
    }

    BOOST_AUTO_TEST_CASE(shift_jis)
    {
        using namespace japanese;

        test_encoding<shift_jis_encoder, shift_jis_decoder>(
            "Shift_JIS", {
                "csshiftjis", "ms932", "ms_kanji", "shift-jis",
                "shift_jis", "sjis", "windows-31j", "x-sjis"
            }
        );
    }

    BOOST_AUTO_TEST_CASE(euc_kr)
    {
        using namespace korean;

        test_encoding<euc_kr_encoder, euc_kr_decoder>(
            "EUC-KR", {
                "cseuckr", "csksc56011987", "euc-kr", "iso-ir-149",
                "korean", "ks_c_5601-1987", "ks_c_5601-1989", "ksc5601",
                "ksc_5601", "windows-949"
            }
        );
    }

    BOOST_AUTO_TEST_CASE(replacement_)
    {
        test_encoding<void, replacement::decoder>(
            "replacement", {
                "csiso2022kr", "hz-gb-2312", "iso-2022-cn",
                "iso-2022-cn-ext", "iso-2022-kr", "replacement"
            }
        );
    }

    BOOST_AUTO_TEST_CASE(utf16be_)
    {
        test_encoding<void, utf16::utf16be_decoder>(
            "UTF-16BE", { "utf-16be" }
        );
    }

    BOOST_AUTO_TEST_CASE(utf16le_)
    {
        test_encoding<void, utf16::utf16le_decoder>(
            "UTF-16LE", { "utf-16", "utf-16le" }
        );
    }

    BOOST_AUTO_TEST_CASE(user_defined_)
    {
        test_encoding<user_defined::encoder, user_defined::decoder>(
            "x-user-defined", { "x-user-defined" }
        );
    }

BOOST_AUTO_TEST_SUITE_END() // get_an_encoding_

BOOST_AUTO_TEST_SUITE(get_an_output_encoding_)

    BOOST_AUTO_TEST_CASE(utf8)
    {
        auto const e1 = get_an_encoding("utf8");
        BOOST_CHECK(e1);

        auto const& e2 = get_an_output_encoding(*e1);
        BOOST_CHECK_EQUAL(e2.name(), "UTF-8");
    }

    BOOST_AUTO_TEST_CASE(utf16be)
    {
        auto const e1 = get_an_encoding("utf-16be");
        BOOST_CHECK(e1);

        auto const& e2 = get_an_output_encoding(*e1);
        BOOST_CHECK_EQUAL(e2.name(), "UTF-8");
    }

    BOOST_AUTO_TEST_CASE(utf16le)
    {
        auto const e1 = get_an_encoding("utf-16le");
        BOOST_CHECK(e1);

        auto const& e2 = get_an_output_encoding(*e1);
        BOOST_CHECK_EQUAL(e2.name(), "UTF-8");
    }

    BOOST_AUTO_TEST_CASE(replacement)
    {
        auto const e1 = get_an_encoding("replacement");
        BOOST_CHECK(e1);

        auto const& e2 = get_an_output_encoding(*e1);
        BOOST_CHECK_EQUAL(e2.name(), "UTF-8");
    }

BOOST_AUTO_TEST_SUITE_END() // get_an_output_encoding_

BOOST_AUTO_TEST_SUITE(decode_)

    BOOST_AUTO_TEST_CASE(utf8_without_bom)
    {
        auto* const encoding = get_an_encoding("UTF-8");
        BOOST_REQUIRE(encoding);

        byte_stream_t input { "ABC" };

        auto const output = decode(input, *encoding);

        BOOST_CHECK(output.to_string_view() == U"ABC");
    }

    BOOST_AUTO_TEST_CASE(utf8_with_bom)
    {
        auto* const encoding = get_an_encoding("UTF-8");
        BOOST_REQUIRE(encoding);

        byte_stream_t input { "\xEF\xBB\xBF\x01\x02"_sv };

        auto const output = decode(input, *encoding);

        BOOST_CHECK(output.to_string_view() == U"\x01\x02"_sv);
    }

    BOOST_AUTO_TEST_CASE(utf16_without_bom)
    {
        auto* const encoding = get_an_encoding("UTF-16BE");
        BOOST_REQUIRE(encoding);

        byte_stream_t input { "\x00\x01\x00\x02"_sv };

        auto const output = decode(input, *encoding);

        BOOST_CHECK(output.to_string_view() == U"\x01\x02");
    }

    BOOST_AUTO_TEST_CASE(utf16_with_bom)
    {
        auto* const encoding = get_an_encoding("UTF-16BE");
        BOOST_REQUIRE(encoding);

        byte_stream_t input { "\xFE\xFF\x00\x01\x00\x02"_sv };

        auto const output = decode(input, *encoding);

        BOOST_CHECK(output.to_string_view() == U"\x01\x02");
    }

    BOOST_AUTO_TEST_CASE(utf16_bom_only)
    {
        auto* const encoding = get_an_encoding("UTF-16BE");
        BOOST_REQUIRE(encoding);

        byte_stream_t input { "\xFE\xFF"_sv };

        auto const output = decode(input, *encoding);

        BOOST_CHECK(output.to_string_view() == U"");
    }

BOOST_AUTO_TEST_SUITE_END() // decode_

BOOST_AUTO_TEST_SUITE(utf8_decode_)

    BOOST_AUTO_TEST_CASE(without_bom)
    {
        byte_stream_t input { u8"日本語" };

        auto const output = utf8_decode(input);

        BOOST_CHECK(output.to_string_view() == U"日本語");
    }

    BOOST_AUTO_TEST_CASE(with_bom)
    {
        byte_stream_t input { u8"\xEF\xBB\xBF日本語" };

        auto const output = utf8_decode(input);

        BOOST_CHECK(output.to_string_view() == U"日本語");
    }

BOOST_AUTO_TEST_SUITE_END() // utf8_decode_

BOOST_AUTO_TEST_SUITE(utf8_decode_without_bom_)

    BOOST_AUTO_TEST_CASE(without_bom)
    {
        byte_stream_t input { u8"日本語" };

        auto const output = utf8_decode_without_bom(input);

        BOOST_CHECK(output.to_string_view() == U"日本語");
    }

    BOOST_AUTO_TEST_CASE(with_bom)
    {
        byte_stream_t input { u8"\xEF\xBB\xBF日本語" };

        auto const output = utf8_decode_without_bom(input);

        BOOST_CHECK(output.to_string_view() == U"\xFEFF日本語");
    }

BOOST_AUTO_TEST_SUITE_END() // utf8_decode_without_bom_

BOOST_AUTO_TEST_SUITE(utf8_decode_without_bom_or_fail_)

    BOOST_AUTO_TEST_CASE(without_bom)
    {
        byte_stream_t input { u8"日本語" };

        auto const output = utf8_decode_without_bom_or_fail(input);
        BOOST_REQUIRE(output);

        BOOST_CHECK(output->to_string_view() == U"日本語");
    }

    BOOST_AUTO_TEST_CASE(with_bom)
    {
        byte_stream_t input { u8"\xEF\xBB\xBF日本語" };

        auto const output = utf8_decode_without_bom_or_fail(input);
        BOOST_REQUIRE(output);

        BOOST_CHECK(output->to_string_view() == U"\xFEFF日本語");
    }

    BOOST_AUTO_TEST_CASE(error_)
    {
        byte_stream_t input { u8"日本語\xFF"_sv };

        auto const output = utf8_decode_without_bom_or_fail(input);
        BOOST_CHECK(!output);
    }

BOOST_AUTO_TEST_SUITE_END() // utf8_decode_without_bom_or_fail_

BOOST_AUTO_TEST_SUITE(encode_)

    BOOST_AUTO_TEST_CASE(utf8_)
    {
        auto* const encoding = get_an_encoding("UTF-8");
        BOOST_REQUIRE(encoding);

        code_point_stream_t input { U"日本語"_sv };

        auto const output = encode(input, *encoding);

        BOOST_CHECK_EQUAL(output.to_string_view(), u8"日本語"_sv);
    }

    BOOST_AUTO_TEST_CASE(error_)
    {
        auto* const encoding = get_an_encoding("UTF-8");
        BOOST_REQUIRE(encoding);

        code_point_stream_t input { U"\x110000"_sv };

        auto const output = encode(input, *encoding);

        BOOST_CHECK_EQUAL(output.to_string_view(), u8"&#1114112;"_sv);
    }

BOOST_AUTO_TEST_SUITE_END() // encode_

BOOST_AUTO_TEST_SUITE(utf8_encode_)

    BOOST_AUTO_TEST_CASE(success_)
    {
        code_point_stream_t input { U"日本語"_sv };

        auto const output = utf8_encode(input);

        BOOST_CHECK_EQUAL(output.to_string_view(), u8"日本語"_sv);
    }

    BOOST_AUTO_TEST_CASE(error_)
    {
        code_point_stream_t input { U"\x110000"_sv };

        auto const output = utf8_encode(input);

        BOOST_CHECK_EQUAL(output.to_string_view(), u8"&#1114112;"_sv);
    }

BOOST_AUTO_TEST_SUITE_END() // utf8_encode_

} // namespace whatwg::encoding
