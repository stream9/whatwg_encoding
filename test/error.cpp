#include <whatwg/encoding/error.hpp>

#include <system_error>

#include <boost/test/unit_test.hpp>

namespace whatwg::encoding::testing {

BOOST_AUTO_TEST_SUITE(error_)

    BOOST_AUTO_TEST_CASE(system_error_)
    {
        std::system_error e1 { errc::stream_read_error };
        std::system_error e2 { errc::stream_set_error };
        std::system_error e3 { errc::stream_prepend_error };
    }

    BOOST_AUTO_TEST_CASE(error_category_)
    {
        auto const& c = encoding_category();

        BOOST_CHECK_EQUAL(c.name(), "WHATWG Encoding");
        BOOST_CHECK_EQUAL(c.message(-1), "unknown error");
    }

BOOST_AUTO_TEST_SUITE_END() // error_

} // namespace whatwg::encoding::testing
