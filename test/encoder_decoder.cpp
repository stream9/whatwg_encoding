#include <whatwg/encoding/encoder_decoder.hpp>
#include <whatwg/encoding/encoder_decoder.ipp>

#include <whatwg/encoding/encoding.hpp>
#include <whatwg/encoding/stream.hpp>
#include <whatwg/encoding/result.hpp>

#include <iostream>

#include <boost/test/unit_test.hpp>

namespace whatwg::encoding::testing {

BOOST_AUTO_TEST_SUITE(encoder_decoder_)

BOOST_AUTO_TEST_SUITE(process_)

    static auto
    encode(code_point_t const c, optional<error_mode> const err = {})
    {
        auto const e = get_an_encoding("utf8");
        BOOST_REQUIRE(e);

        auto coder = e->encoder();
        BOOST_REQUIRE(coder);

        auto new_encoder = coder->create();
        BOOST_REQUIRE(new_encoder);

        stream_t is;
        byte_stream_t os;

        return std::make_pair(os, process(*new_encoder, c, is, os, err));
    }

    BOOST_AUTO_TEST_CASE(encode_single_byte)
    {
        auto const& [os, r] = encode(U'A');

        BOOST_CHECK(r.is_continue());
        BOOST_CHECK_EQUAL(os.to_string(), "A");
    }

    BOOST_AUTO_TEST_CASE(encode_two_byte_sequence)
    {
        auto const& [os, r] = encode(0x80u);

        BOOST_CHECK(r.is_continue());
        BOOST_CHECK(os.to_string() == "\xC2\x80");
    }

    BOOST_AUTO_TEST_CASE(encode_end_of_stream)
    {
        auto const& [_, r] = encode(end_of_stream);

        BOOST_CHECK(r.is_finished());
    }

    BOOST_AUTO_TEST_CASE(encode_invalid_code_point)
    {
        auto const& [os, r] = encode(0x110000u);

        BOOST_CHECK(r.is_error());
        BOOST_CHECK(r.empty());
    }

    BOOST_AUTO_TEST_CASE(encode_invalid_code_point_html)
    {
        auto const e = get_an_encoding("utf8");
        BOOST_REQUIRE(e);

        auto coder = e->encoder();
        BOOST_REQUIRE(coder);

        auto new_encoder = coder->create();
        BOOST_REQUIRE(new_encoder);

        stream_t is;
        byte_stream_t os;

        auto const r = process(
            *new_encoder, 0x110000u, is, os, error_mode::html);
        BOOST_REQUIRE(r.is_continue());

        BOOST_REQUIRE(is.to_string_view() == U"&#1114112;");
    }

    static auto
    decode(byte_stream_t& is,
           optional<error_mode> err = {})
    {
        auto const e = get_an_encoding("utf8");
        BOOST_REQUIRE(e);

        auto const coder = e->decoder().create();
        BOOST_REQUIRE(coder);

        stream_t os;

        auto const r = process(*coder, is.read(), is, os, err);

        return std::make_pair(os, r);
    }

    BOOST_AUTO_TEST_CASE(decode_ascii)
    {
        byte_stream_t is { "A" };
        auto const& [os, r] = decode(is);

        BOOST_CHECK(r.is_continue());
        BOOST_CHECK(os.to_string() == U"A");
    }

    BOOST_AUTO_TEST_CASE(decode_error_replacement)
    {
        byte_stream_t is { "\xFF" };
        auto const& [os, r] = decode(is);

        BOOST_CHECK(r.is_continue());
        BOOST_CHECK(os.to_string() == U"\xFFFD");
    }

    BOOST_AUTO_TEST_CASE(decode_error_fatal)
    {
        byte_stream_t is { "\xFF" };
        auto const& [os, r] = decode(is, error_mode::fatal);

        BOOST_REQUIRE(r.is_error());
        BOOST_CHECK(r.empty());
    }

BOOST_AUTO_TEST_SUITE_END() // process_

BOOST_AUTO_TEST_SUITE(run_)

    BOOST_AUTO_TEST_CASE(encode_ascii)
    {
        auto const e = get_an_encoding("utf8");
        BOOST_REQUIRE(e);

        stream_t is { U"ABC" };
        byte_stream_t os;

        auto* const encoder = e->encoder();
        BOOST_REQUIRE(encoder);

        auto const r = run(*encoder, is, os);
        BOOST_REQUIRE(r.is_finished());
        BOOST_CHECK_EQUAL(os.to_string(), "ABC");
    }

    BOOST_AUTO_TEST_CASE(encode_error_html)
    {
        auto const e = get_an_encoding("utf8");
        BOOST_REQUIRE(e);

        stream_t is { U"\x110000" };
        byte_stream_t os;

        auto* const encoder = e->encoder();
        BOOST_REQUIRE(encoder);

        auto const r = run(*encoder, is, os, error_mode::html);
        BOOST_REQUIRE(r.is_finished());
        BOOST_CHECK_EQUAL(os.to_string(), "&#1114112;");
    }

    BOOST_AUTO_TEST_CASE(decode_ascii)
    {
        auto const e = get_an_encoding("utf8");
        BOOST_REQUIRE(e);

        byte_stream_t is { "ABC" };
        stream_t os;

        auto const r = run(e->decoder(), is, os);
        BOOST_REQUIRE(r.is_finished());
        BOOST_CHECK(os.to_string() == U"ABC");
    }

BOOST_AUTO_TEST_SUITE_END() // run_

BOOST_AUTO_TEST_SUITE_END() // encoder_decoder_

} // namespace whatwg::encoding::testing
