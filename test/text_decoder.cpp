#include <whatwg/encoding/text_decoder.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::encoding::testing {

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(text_decoder_)

    BOOST_AUTO_TEST_CASE(construct_default)
    {
        text_decoder d;

        BOOST_CHECK_EQUAL(d.encoding_name(), "utf-8");
        BOOST_CHECK(!d.fatal());
        BOOST_CHECK(!d.ignore_bom());
    }

    BOOST_AUTO_TEST_CASE(construct_fatal)
    {
        text_decoder d { "utf-8", text_decoder::options::fatal };

        BOOST_CHECK_EQUAL(d.encoding_name(), "utf-8");
        BOOST_CHECK(d.fatal());
        BOOST_CHECK(!d.ignore_bom());
    }

    BOOST_AUTO_TEST_CASE(construct_ignore_bom)
    {
        text_decoder d { "utf-8", text_decoder::options::ignore_bom };

        BOOST_CHECK_EQUAL(d.encoding_name(), "utf-8");
        BOOST_CHECK(!d.fatal());
        BOOST_CHECK(d.ignore_bom());
    }

    BOOST_AUTO_TEST_CASE(construct_fatal_and_ignore_bom)
    {
        auto const options = text_decoder::options::fatal
                           | text_decoder::options::ignore_bom;

        text_decoder d { "utf-8", options };

        BOOST_CHECK_EQUAL(d.encoding_name(), "utf-8");
        BOOST_CHECK(d.fatal());
        BOOST_CHECK(d.ignore_bom());
    }

    BOOST_AUTO_TEST_CASE(decode_string_without_bom)
    {
        text_decoder d;

        auto r = d.decode(u8"日本語");
        BOOST_CHECK(r == U"日本語");

        r = d.decode();
        BOOST_CHECK(r == U"");
    }

    BOOST_AUTO_TEST_CASE(decode_string_with_bom)
    {
        text_decoder d;

        auto r = d.decode(u8"\xEF\xBB\xBF日本語");
        BOOST_CHECK(r == U"日本語");

        r = d.decode(u8"\xEF\xBB\xBF English");
        BOOST_CHECK(r == U" English");

    }

    BOOST_AUTO_TEST_CASE(decode_stream_without_bom)
    {
        text_decoder d;

        auto const r = d.decode(u8"日本語", true);
        BOOST_CHECK(r == U"日本語");
    }

    BOOST_AUTO_TEST_CASE(decode_stream)
    {
        text_decoder d;

        auto r = d.decode(u8"\xEF\xBB\xBF日本語", true);
        BOOST_CHECK(r == U"日本語");

        // bom seen flag is preserved
        r = d.decode(u8"\xEF\xBB\xBF English", true);
        BOOST_CHECK(r == U"\xFEFF English");

        d.decode();
    }

    BOOST_AUTO_TEST_CASE(decode_utf16)
    {
        text_decoder d { "utf-16le" };

        auto r = d.decode("\x01\x00\x02\x00"sv);
        BOOST_CHECK(r == U"\x01\x02");

        r = d.decode();
        BOOST_CHECK(r == U"");
    }

    BOOST_AUTO_TEST_CASE(decode_single_byte)
    {
        text_decoder d { "ibm866" };

        auto r = d.decode("ABC"sv);
        BOOST_CHECK(r == U"ABC");

        r = d.decode();
        BOOST_CHECK(r == U"");
    }

    BOOST_AUTO_TEST_CASE(range_error_)
    {
        auto is_range_error = [](auto const& ex) {
            return ex.code() == errc::range_error;
        };

        BOOST_CHECK_EXCEPTION(
            do { text_decoder d { "xxx" }; } while (false),
            std::system_error,
            is_range_error
        );

        BOOST_CHECK_EXCEPTION(
            do { text_decoder d { "replacement" }; } while (false),
            std::system_error,
            is_range_error
        );
    }

    BOOST_AUTO_TEST_CASE(type_error_)
    {
        text_decoder d { "utf8", text_decoder::options::fatal };
        auto is_type_error = [](auto const& ex) {
            return ex.code() == errc::type_error;
        };

        BOOST_CHECK_EXCEPTION(
            d.decode("\xFF"),
            std::system_error,
            is_type_error
        );
    }

BOOST_AUTO_TEST_SUITE_END() // text_decoder_

} // namespace whatwg::encoding
