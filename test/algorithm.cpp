#include <whatwg/encoding/algorithm.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::encoding {

BOOST_AUTO_TEST_SUITE(algorithm_)

    BOOST_AUTO_TEST_CASE(between_)
    {
        BOOST_CHECK(between(1, 1, 20));
        BOOST_CHECK(between(20, 1, 20));
        BOOST_CHECK(!between(-1, 1, 20));
        BOOST_CHECK(!between(21, 1, 20));
    }

    BOOST_AUTO_TEST_CASE(to_byte_)
    {
        BOOST_CHECK_EQUAL(to_byte(U'A'), 'A');
        BOOST_CHECK_EQUAL(to_byte(0u), 0);
        BOOST_CHECK_EQUAL(to_byte(0xFFu), static_cast<char>(0xFF));
    }

    BOOST_AUTO_TEST_CASE(is_token_)
    {
        BOOST_CHECK(is_token(0));
        BOOST_CHECK(is_token(0x10FFFFu));
        BOOST_CHECK(!is_token(0x110000u));
        BOOST_CHECK(!is_token(0xFFFFFFFE));
        BOOST_CHECK(is_token(end_of_stream));
    }

BOOST_AUTO_TEST_SUITE_END() // algorithm_

} // namespace whatwg::encoding
