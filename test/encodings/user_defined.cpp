#include <whatwg/encoding/encodings/user_defined.hpp>

#include <whatwg/encoding/stream.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::encoding::user_defined::testing {

BOOST_AUTO_TEST_SUITE(encodings_)

BOOST_AUTO_TEST_SUITE(user_defined_)

    static void
    check_token(result_t const& r, token_t const t)
    {
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE_EQUAL(r.size(), 1);
        BOOST_CHECK_EQUAL(r[0], t);
    }

    static void
    check_tokens(result_t const& r, byte_sequence_view_t const t)
    {
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE_EQUAL(r.size(), t.size());

        for (size_t i = 0; i < r.size(); ++i) {
            BOOST_CHECK_EQUAL(r[i], to_token(t[i]));
        }
    }

    static auto
    decode(byte_sequence_view_t const s)
    {
        user_defined::decoder d;

        byte_stream_t is { s };

        while (true) {
            auto const r = d.handler(is, is.read());

            if (!r.is_continue()) return r;
        }
    }

    BOOST_AUTO_TEST_CASE(decode_empty)
    {
        auto r = decode(""_sv);
        BOOST_REQUIRE(r.is_finished());
    }

    BOOST_AUTO_TEST_CASE(decode_ascii)
    {
        auto r = decode("\x00"_sv);
        check_token(r, 0x00u);

        r = decode("\x7E"_sv);
        check_token(r, 0x7Eu);
    }

    BOOST_AUTO_TEST_CASE(decode_beyond_ascii)
    {
        auto r = decode("\x80"_sv);
        check_token(r, 0xF780u);

        r = decode("\xFF"_sv);
        check_token(r, 0xF7FFu);
    }

    BOOST_AUTO_TEST_CASE(decoder_factory)
    {
        user_defined::decoder c1;

        auto const c2 = c1.create();

        BOOST_CHECK(c2);
    }

    static auto
    encode(string_view_t const s)
    {
        user_defined::encoder e;

        stream_t is { s };

        return e.handler(is, is.read());
    }

    BOOST_AUTO_TEST_CASE(encode_empty)
    {
        auto r = encode(U""_sv);
        BOOST_CHECK(r.is_finished());
    }

    BOOST_AUTO_TEST_CASE(encode_ascii)
    {
        auto r = encode(U"\x00"_sv);
        check_token(r, 0x00u);

        r = encode(U"\x7F"_sv);
        check_token(r, 0x7Fu);

        r = encode(U"\x80"_sv);
        BOOST_CHECK(r.is_error());
        BOOST_REQUIRE_EQUAL(r.size(), 1);
        BOOST_CHECK_EQUAL(r[0], 0x80u);
    }

    BOOST_AUTO_TEST_CASE(encode_beyond_ascii)
    {
        auto r = encode(U"\xF77F"_sv);
        BOOST_CHECK(r.is_error());
        BOOST_REQUIRE_EQUAL(r.size(), 1);
        BOOST_CHECK_EQUAL(r[0], 0xF77Fu);

        r = encode(U"\xF780"_sv);
        check_token(r, 0x80u);

        r = encode(U"\xF7FF"_sv);
        check_token(r, 0xFFu);

        r = encode(U"\xF800"_sv);
        BOOST_CHECK(r.is_error());
        BOOST_REQUIRE_EQUAL(r.size(), 1);
        BOOST_CHECK_EQUAL(r[0], 0xF800u);
    }

    BOOST_AUTO_TEST_CASE(encoder_factory)
    {
        user_defined::encoder c1;

        auto const c2 = c1.create();

        BOOST_CHECK(c2);
    }

BOOST_AUTO_TEST_SUITE_END() // user_defined_

BOOST_AUTO_TEST_SUITE_END() // encodings_

} // namespace whatwg::encoding::user_defined::testing
