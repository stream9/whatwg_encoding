#include <whatwg/encoding/encodings/utf16.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::encoding::utf16::testing {

BOOST_AUTO_TEST_SUITE(encodings_)

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(utf16_)

    template<typename Decoder>
    result_t
    decode(Decoder& d, byte_sequence_view_t const bytes)
    {
        byte_stream_t is { bytes };

        result_t r = continue_;
        do {
            r = d.handler(is, is.read());

        } while (r.is_continue());

        return r;
    }

    BOOST_AUTO_TEST_CASE(decode_basic_multilingual_plane)
    {
        utf16le_decoder d;

        auto r = decode(d, "\0\0"sv);
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE(!r.empty());
        BOOST_CHECK_EQUAL(r[0], 0x0000u);

        r = decode(d, "\xFF\xD7"sv);
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE(!r.empty());
        BOOST_CHECK_EQUAL(r[0], 0xD7FFu);

        r = decode(d, "\x00\xE0"sv);
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE(!r.empty());
        BOOST_CHECK_EQUAL(r[0], 0xE000u);

        r = decode(d, "\xFF\xFF"sv);
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE(!r.empty());
        BOOST_CHECK_EQUAL(r[0], 0xFFFFu);
    }

    BOOST_AUTO_TEST_CASE(decode_supplementary_plane)
    {
        utf16le_decoder d;

        auto r = decode(d, "\x00\xD8\x00\xDC"sv);
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE(!r.empty());
        BOOST_CHECK_EQUAL(r[0], 0x10000u);

        r = decode(d, "\xFF\xDB\xFF\xDF"sv);
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE(!r.empty());
        BOOST_CHECK_EQUAL(r[0], 0x10FFFFu);
    }

    BOOST_AUTO_TEST_CASE(invalid_first_surrogate)
    {
        utf16le_decoder d;

        auto r = decode(d, "\x00\xDC\x00\xD8"sv);
        BOOST_CHECK(r.is_error());
    }

    BOOST_AUTO_TEST_CASE(invalid_second_surrogate)
    {
        utf16le_decoder d;
        byte_stream_t is { "\x00\xD8\x12\x34"sv };

        result_t r = continue_;
        do {
            r = d.handler(is, is.read());

        } while (r.is_continue());

        BOOST_REQUIRE(r.is_error());
        BOOST_CHECK_EQUAL(is.to_string_view(), "\x12\x34"sv);
    }

    BOOST_AUTO_TEST_CASE(invalid_second_surrogate_big_endian)
    {
        utf16be_decoder d;
        byte_stream_t is { "\xD8\x00\x12\x34"sv };

        result_t r = continue_;
        do {
            r = d.handler(is, is.read());

        } while (r.is_continue());

        BOOST_REQUIRE(r.is_error());
        BOOST_CHECK_EQUAL(is.to_string_view(), "\x12\x34"sv);
    }

    BOOST_AUTO_TEST_CASE(decode_end_of_stream)
    {
        utf16le_decoder d;

        auto r = decode(d, ""sv);
        BOOST_CHECK(r.is_finished());

        r = decode(d, "\x00"sv);
        BOOST_CHECK(r.is_error());

        r = decode(d, "\x00\xD8"sv);
        BOOST_CHECK(r.is_error());

        r = decode(d, "\x00\xD8\x00"sv);
        BOOST_CHECK(r.is_error());
    }

    BOOST_AUTO_TEST_CASE(decoder_factory)
    {
        utf16le_decoder c1;

        auto const c2 = c1.create();

        BOOST_CHECK(c2);
    }

BOOST_AUTO_TEST_SUITE_END() // utf16_

BOOST_AUTO_TEST_SUITE_END() // encodings_

} // namespace whatwg::encoding::utf16::testing
