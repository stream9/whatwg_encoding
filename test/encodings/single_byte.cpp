#include <whatwg/encoding/encodings/single_byte.hpp>

#include <vector>

#include <boost/test/unit_test.hpp>

namespace whatwg::encoding::single_byte::testing {

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(encodings_)

BOOST_AUTO_TEST_SUITE(single_byte_)

    static auto
    decode(byte_sequence_view_t const encoding_name,
           byte_sequence_view_t const s)
    {
        single_byte::decoder d { encoding_name };

        byte_stream_t is;
        is = s;

        return d.handler(is, is.read());
    }

    BOOST_AUTO_TEST_CASE(decode_ascii)
    {
        auto r = decode("IBM866", "\x00"sv);
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE_EQUAL(r.size(), 1);
        BOOST_CHECK_EQUAL(r[0], 0x00u);

        r = decode("IBM866", "\x7F"sv);
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE_EQUAL(r.size(), 1);
        BOOST_CHECK_EQUAL(r[0], 0x7Fu);
    }

    BOOST_AUTO_TEST_CASE(decode_not_ascii)
    {
        auto r = decode("IBM866", "\x80"sv);
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE_EQUAL(r.size(), 1);
        BOOST_CHECK_EQUAL(r[0], 0x410u);

        r = decode("IBM866", "\xFF"sv);
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE_EQUAL(r.size(), 1);
        BOOST_CHECK_EQUAL(r[0], 0xA0u);
    }

    BOOST_AUTO_TEST_CASE(decode_end_of_stream)
    {
        const auto r = decode("IBM866", ""sv);
        BOOST_CHECK(r.is_finished());
    }

    BOOST_AUTO_TEST_CASE(decoder_factory)
    {
        single_byte::decoder c1 { "IBM866" };

        auto const c2 = c1.create();

        BOOST_CHECK(c2);
    }

    static auto
    encode(byte_sequence_view_t const encoding_name,
           string_view_t const s)
    {
        single_byte::encoder e { encoding_name };

        stream_t is;
        is = s;

        return e.handler(is, is.read());
    }

    BOOST_AUTO_TEST_CASE(encode_ascii)
    {
        auto r = encode("IBM866", U"\x00"sv);
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE_EQUAL(r.size(), 1);
        BOOST_CHECK_EQUAL(r[0], 0x00u);

        r = encode("IBM866", U"\x7F"sv);
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE_EQUAL(r.size(), 1);
        BOOST_CHECK_EQUAL(r[0], 0x7Fu);
    }

    BOOST_AUTO_TEST_CASE(encode_non_ascii)
    {
        auto r = encode("IBM866", U"\x80"sv);
        BOOST_REQUIRE(r.is_error());

        r = encode("IBM866", U"\xA0"sv);
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE_EQUAL(r.size(), 1);
        BOOST_CHECK_EQUAL(r[0], 0xFFu);

        r = encode("IBM866", U"\x25A0"sv);
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE_EQUAL(r.size(), 1);
        BOOST_CHECK_EQUAL(r[0], 0xFEu);
    }

    BOOST_AUTO_TEST_CASE(encode_end_of_stream)
    {
        const auto r = encode("IBM866", U""sv);
        BOOST_CHECK(r.is_finished());
    }

    static string_t
    decode_string(byte_sequence_view_t const encoding_name,
                  byte_sequence_t& input)
    {
        byte_stream_t is { input };

        single_byte::decoder decoder { encoding_name };

        string_t result;
        byte_sequence_t valid_input;

        token_t c = end_of_stream;
        while ((c = is.read()) != end_of_stream) {
            auto const r = decoder.handler(is, c);

            if (!r.is_error()) {
                BOOST_REQUIRE(r.is_tokens());
                BOOST_REQUIRE_EQUAL(r.size(), 1);
                result.push_back(r[0]);

                valid_input.push_back(c);
            }
        }

        input.swap(valid_input);

        return result;
    }

    static byte_sequence_t
    encode_string(byte_sequence_view_t const encoding_name,
                  string_view_t const input)
    {
        stream_t is { input };

        single_byte::encoder encoder { encoding_name };

        byte_sequence_t result;

        token_t c = end_of_stream;
        while ((c = is.read()) != end_of_stream) {
            auto const r = encoder.handler(is, c);

            BOOST_REQUIRE(r.is_tokens());
            BOOST_REQUIRE_EQUAL(r.size(), 1);
            result.push_back(to_byte(r[0]));
        }

        return result;
    }

    static void
    decode_and_encode(byte_sequence_view_t const encoding_name)
    {
        byte_sequence_t input;
        for (auto c = 0x00; c <= 0xFF; ++c) {
            input.push_back(c);
        }
        BOOST_REQUIRE_EQUAL(input.size(), 256);

        auto const decoded = decode_string(encoding_name, input);
        auto const encoded = encode_string(encoding_name, decoded);

        BOOST_CHECK_EQUAL(encoded, input);
    }

    BOOST_AUTO_TEST_CASE(decode_and_encode_for_all_encodings)
    {
        byte_sequence_view_t const encodings[] = {
            "IBM866", "ISO-8859-2", "ISO-8859-3", "ISO-8859-4",
            "ISO-8859-5", "ISO-8859-6", "ISO-8859-7", "ISO-8859-8",
            "ISO-8859-8-I", "ISO-8859-10", "ISO-8859-13", "ISO-8859-14",
            "ISO-8859-15", "ISO-8859-16", "KOI8-R", "KOI8-U", "macintosh",
            "windows-874", "windows-1250", "windows-1251", "windows-1252",
            "windows-1253", "windows-1254", "windows-1255", "windows-1256",
            "windows-1257", "windows-1258", "x-mac-cyrillic"
        };

        for (auto const name: encodings) {
            decode_and_encode(name);
        }
    }

    BOOST_AUTO_TEST_CASE(encoder_factory)
    {
        single_byte::encoder c1 { "IBM866" };

        auto const c2 = c1.create();

        BOOST_CHECK(c2);
    }

BOOST_AUTO_TEST_SUITE_END() // single_byte_

BOOST_AUTO_TEST_SUITE_END() // encodings_

} // namespace whatwg::encoding::single_byte::testing
