#include <whatwg/encoding/encodings/replacement.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::encoding::replacement::testing {

BOOST_AUTO_TEST_SUITE(encodings_)

BOOST_AUTO_TEST_SUITE(replacement_)

    BOOST_AUTO_TEST_CASE(decoder_end_of_stream)
    {
        replacement::decoder d;
        byte_stream_t is;

        auto const r = d.handler(is, end_of_stream);
        BOOST_CHECK_EQUAL(r, finished);
    }

    BOOST_AUTO_TEST_CASE(decoder_)
    {
        replacement::decoder d;
        byte_stream_t is;

        auto const r1 = d.handler(is, U'1');
        BOOST_CHECK_EQUAL(r1, error);

        auto const r2 = d.handler(is, U'2');
        BOOST_CHECK_EQUAL(r2, finished);
    }

        BOOST_AUTO_TEST_CASE(decoder_factory)
        {
            replacement::decoder c1;

            auto const c2 = c1.create();

            BOOST_CHECK(c2);
        }

BOOST_AUTO_TEST_SUITE_END() // replacement

BOOST_AUTO_TEST_SUITE_END() // encodings

} // namespace whatwg::encoding::replacement
