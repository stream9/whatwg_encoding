#include <whatwg/encoding/encodings/euc_kr.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::encoding::korean::testing {

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(encodings_)

BOOST_AUTO_TEST_SUITE(korean)

    static void
    check_token(result_t const& r, token_t const t)
    {
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE_EQUAL(r.size(), 1);
        BOOST_CHECK_EQUAL(r[0], t);
    }

    static void
    check_tokens(result_t const& r, byte_sequence_view_t const t)
    {
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE_EQUAL(r.size(), t.size());

        for (size_t i = 0; i < r.size(); ++i) {
            BOOST_CHECK_EQUAL(r[i], to_token(t[i]));
        }
    }

    BOOST_AUTO_TEST_SUITE(euc_kr_)

        static auto
        decode(byte_sequence_view_t const s)
        {
            euc_kr_decoder d;

            byte_stream_t is { s };

            while (true) {
                auto const r = d.handler(is, is.read());

                if (!r.is_continue()) return r;
            }
        }

        BOOST_AUTO_TEST_CASE(decode_empty)
        {
            auto r = decode(""sv);
            BOOST_REQUIRE(r.is_finished());
        }

        BOOST_AUTO_TEST_CASE(decode_single_byte)
        {
            auto r = decode("\x00"sv);
            check_token(r, 0x00u);

            r = decode("\x7E"sv);
            check_token(r, 0x7Eu);

            r = decode("\x80"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode("\xFF"sv);
            BOOST_REQUIRE(r.is_error());
        }

        BOOST_AUTO_TEST_CASE(decode_two_bytes)
        {
            auto r = decode("\x81"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode("\x81\x40"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode("\x81\x41"sv);
            check_token(r, 0xAC02u);

            r = decode("\x81\xFE"sv);
            check_token(r, 0xAD13u);

            r = decode("\x81\xFF"sv);
            BOOST_REQUIRE(r.is_error());
        }

        BOOST_AUTO_TEST_CASE(decoder_factory)
        {
            euc_kr_decoder c1;

            auto const c2 = c1.create();

            BOOST_CHECK(c2);
        }

        static auto
        encode(string_view_t const s)
        {
            euc_kr_encoder e;

            stream_t is { s };

            return e.handler(is, is.read());
        }

        BOOST_AUTO_TEST_CASE(encode_empty)
        {
            auto r = encode(U""sv);
            BOOST_CHECK(r.is_finished());
        }

        BOOST_AUTO_TEST_CASE(encode_single_byte)
        {
            auto r = encode(U"\x00"sv);
            check_token(r, 0x00u);

            r = encode(U"\x7F"sv);
            check_token(r, 0x7Fu);

            r = encode(U"\x80"sv);
            BOOST_REQUIRE(r.is_error());
            BOOST_REQUIRE_EQUAL(r.size(), 1);
            BOOST_REQUIRE_EQUAL(r[0], 0x80u);
        }

        BOOST_AUTO_TEST_CASE(encode_two_bytes)
        {
            auto r = encode(U"\xAC02"sv);
            check_tokens(r, "\x81\x41");
        }

        BOOST_AUTO_TEST_CASE(encoder_factory)
        {
            euc_kr_encoder c1;

            auto const c2 = c1.create();

            BOOST_CHECK(c2);
        }

    BOOST_AUTO_TEST_SUITE_END() // euc_kr_

BOOST_AUTO_TEST_SUITE_END() // korean

BOOST_AUTO_TEST_SUITE_END() // encodings_

} // namespace whatwg::encoding::korean::testing
