#include <whatwg/encoding/encodings/chinese_simplified.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::encoding::chinese::testing {

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(encodings_)

BOOST_AUTO_TEST_SUITE(chinese_)

    static void
    check_token(result_t const& r, token_t const t)
    {
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE_EQUAL(r.size(), 1);
        BOOST_CHECK_EQUAL(r[0], t);
    }

    static void
    check_tokens(result_t const& r, byte_sequence_view_t const t)
    {
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE_EQUAL(r.size(), t.size());

        for (size_t i = 0; i < r.size(); ++i) {
            BOOST_CHECK_EQUAL(r[i], to_token(t[i]));
        }
    }

    BOOST_AUTO_TEST_SUITE(gb18030_)

        static auto
        decode(byte_sequence_view_t const s)
        {
            gb18030_decoder d;

            byte_stream_t is { s };

            while (true) {
                auto const r = d.handler(is, is.read());

                if (!r.is_continue()) return r;
            }
        }

        BOOST_AUTO_TEST_CASE(decode_empty)
        {
            auto r = decode(""sv);
            BOOST_REQUIRE(r.is_finished());
        }

        BOOST_AUTO_TEST_CASE(decode_single_byte)
        {
            auto r = decode("\x00"sv);
            check_token(r, 0x00u);

            r = decode("\x7F"sv);
            check_token(r, 0x7Fu);

            r = decode("\x80"sv);
            check_token(r, 0x20ACu);

            r = decode("\x81"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode("\xFE"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode("\xFF"sv);
            BOOST_REQUIRE(r.is_error());
        }

        BOOST_AUTO_TEST_CASE(decode_two_bytes)
        {
            auto r = decode("\x81\x40"sv);
            check_token(r, 0x4E02u);

            r = decode("\x81\x7E"sv);
            check_token(r, 0x4E8Au);

            r = decode("\x81\x7F"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode("\x81\x80"sv);
            check_token(r, 0x4E90u);

            r = decode("\x81\xFE"sv);
            check_token(r, 0x4FA2u);

            r = decode("\x81\xFF"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode("\xFE\x40"sv);
            check_token(r, 0xFA0Cu);

            r = decode("\xFE\x7E"sv);
            check_token(r, 0xE843u);

            r = decode("\xFE\x7F"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode("\xFE\x80"sv);
            check_token(r, 0x4723u);

            r = decode("\xFE\xFE"sv);
            check_token(r, 0xE4C5u);

            r = decode("\xFE\xFF"sv);
            BOOST_REQUIRE(r.is_error());
        }

        BOOST_AUTO_TEST_CASE(decode_four_bytes_1)
        {
            auto r = decode("\x81\x30\x81\x2F"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode("\x81\x30\x81\x30"sv);
            check_token(r, 0x80u);

            r = decode("\x81\x30\x81\x39"sv);
            check_token(r, 0x89u);

            r = decode("\x81\x30\x81\x3A"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode("\x81\x30\x81"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode("\x81\x30"sv);
            BOOST_REQUIRE(r.is_error());
        }

        BOOST_AUTO_TEST_CASE(decode_four_bytes_2)
        {
            auto r = decode("\x81\x30\x80\x30"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode("\x81\x30\x81\x30"sv);
            check_token(r, 0x80u);

            r = decode("\x81\x30\xFE\x30"sv);
            check_token(r, 0x600u);

            r = decode("\x81\x30\xFF\x30"sv);
            BOOST_REQUIRE(r.is_error());
        }

        BOOST_AUTO_TEST_CASE(decode_four_bytes_3)
        {
            auto r = decode("\x81\x2F\x81\x30"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode("\x81\x30\x81\x30"sv);
            check_token(r, 0x80u);

            r = decode("\x81\x39\x81\x30"sv);
            check_token(r, 0x2E91u);

            r = decode("\x81\x3A\x81\x30"sv);
            BOOST_REQUIRE(r.is_error());
        }

        BOOST_AUTO_TEST_CASE(decode_four_bytes_4)
        {
            auto r = decode("\x81\x30\x81\x30"sv);
            check_token(r, 0x80u);

            r = decode("\x84\x30\x81\x30"sv);
            check_token(r, 0xF8FFu);

            r = decode("\x85\x30\x81\x30"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode("\x8F\x30\x81\x30"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode("\x90\x30\x81\x30"sv);
            check_token(r, 0x10000u);

            r = decode("\xE3\x30\x81\x30"sv);
            check_token(r, 0x10F528u);

            r = decode("\xE4\x30\x81\x30"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode("\xFF\x30\x81\x30"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode("\x81\x35\xF4\x37"sv);
            check_token(r, 0xE7C7u);
        }

        BOOST_AUTO_TEST_CASE(decoder_factory)
        {
            gb18030_decoder d;

            auto const copy = d.create();

            BOOST_CHECK(copy);
        }

        static auto
        encode(string_view_t const s)
        {
            gb18030_encoder e;

            stream_t is { s };

            return e.handler(is, is.read());
        }

        BOOST_AUTO_TEST_CASE(encode_empty)
        {
            auto r = encode(U""sv);
            BOOST_CHECK(r.is_finished());
        }

        BOOST_AUTO_TEST_CASE(encode_ascii)
        {
            auto r = encode(U"\x00"sv);
            check_tokens(r, "\x00"sv);

            r = encode(U"\x7F"sv);
            check_tokens(r, "\x7F"sv);
        }

        BOOST_AUTO_TEST_CASE(encode_0080_FFFF)
        {
            auto r = encode(U"\x80"sv);
            check_tokens(r, "\x81\x30\x81\x30"sv);

            r = encode(U"\xA4"sv);
            check_tokens(r, "\xA1\xE8"sv);

            r = encode(U"\x500A"sv);
            check_tokens(r, "\x82\x7E"sv);

            r = encode(U"\x9FA5"sv);
            check_tokens(r, "\xFD\x9B"sv);

            r = encode(U"\xE000"sv);
            check_tokens(r, "\xAA\xA1"sv);

            r = encode(U"\xFFE5"sv);
            check_tokens(r, "\xA3\xA4"sv);

            r = encode(U"\xF000"sv);
            check_tokens(r, "\x83\x38\x96\x37"sv);

            r = encode(U"\xFFFF"sv);
            check_tokens(r, "\x84\x31\xA4\x39"sv);

            r = encode(U"\xE7C7"sv);
            check_tokens(r, "\x81\x35\xF4\x37"sv);
        }

        BOOST_AUTO_TEST_CASE(encode_E5E5)
        {
            auto r = encode(U"\xE5E5"sv);
            BOOST_REQUIRE(r.is_error());
            BOOST_REQUIRE_EQUAL(r.size(), 1);
            BOOST_CHECK_EQUAL(r[0], 0xE5E5u);
        }

    BOOST_AUTO_TEST_SUITE_END() // gb18030_

    BOOST_AUTO_TEST_SUITE(GBK_)

        static auto
        encode(string_view_t const s)
        {
            gbk_encoder e;

            stream_t is { s };

            return e.handler(is, is.read());
        }

        BOOST_AUTO_TEST_CASE(encode_)
        {
            auto r = encode(U"\xA4"sv);
            check_tokens(r, "\xA1\xE8"sv);

            r = encode(U"\x20AC"sv);
            check_tokens(r, "\x80"sv);

            r = encode(U"\x80"sv);
            BOOST_REQUIRE(r.is_error());

            BOOST_REQUIRE_EQUAL(r.size(), 1);
            BOOST_CHECK_EQUAL(r[0], 0x80u);
        }

        BOOST_AUTO_TEST_CASE(encoder_factory)
        {
            gb18030_encoder e;

            auto const copy = e.create();

            BOOST_CHECK(copy);
        }

    BOOST_AUTO_TEST_SUITE_END() // GBK_

BOOST_AUTO_TEST_SUITE_END() // chinese_

BOOST_AUTO_TEST_SUITE_END() // encodings_

} // namespace whatwg::encoding::chinese::testing
