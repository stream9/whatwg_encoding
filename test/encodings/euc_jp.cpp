#include <whatwg/encoding/encodings/euc_jp.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::encoding::japanese::testing {

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(encodings_)

BOOST_AUTO_TEST_SUITE(japanese)

    static void
    check_token(result_t const& r, token_t const t)
    {
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE_EQUAL(r.size(), 1);
        BOOST_CHECK_EQUAL(r[0], t);
    }

    static void
    check_tokens(result_t const& r, byte_sequence_view_t const t)
    {
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE_EQUAL(r.size(), t.size());

        for (size_t i = 0; i < r.size(); ++i) {
            BOOST_CHECK_EQUAL(r[i], to_token(t[i]));
        }
    }

    BOOST_AUTO_TEST_SUITE(euc_jp_)

        static auto
        decode(byte_sequence_view_t const s)
        {
            euc_jp_decoder d;

            byte_stream_t is { s };

            while (true) {
                auto const r = d.handler(is, is.read());

                if (!r.is_continue()) return r;
            }
        }

        BOOST_AUTO_TEST_CASE(decode_empty)
        {
            auto r = decode(""sv);
            BOOST_REQUIRE(r.is_finished());
        }

        BOOST_AUTO_TEST_CASE(decode_single_byte)
        {
            auto r = decode("\x00"sv);
            check_token(r, 0x00u);

            r = decode("\x7F"sv);
            check_token(r, 0x7Fu);

            r = decode("\x80"sv);
            BOOST_CHECK(r.is_error());

            r = decode("\x8E"sv);
            BOOST_CHECK(r.is_error());

            r = decode("\x8F"sv);
            BOOST_CHECK(r.is_error());

            r = decode("\xA1"sv);
            BOOST_CHECK(r.is_error());

            r = decode("\xFE"sv);
            BOOST_CHECK(r.is_error());

            r = decode("\xFF"sv);
            BOOST_CHECK(r.is_error());
        }

        BOOST_AUTO_TEST_CASE(decode_two_bytes)
        {
            auto r = decode("\x8E\xA0"sv);
            BOOST_CHECK(r.is_error());

            r = decode("\x8E\xA1"sv);
            check_token(r, 0xFF61u);

            r = decode("\x8E\xDF"sv);
            check_token(r, 0xFF9Fu);

            r = decode("\xA1\xA1"sv);
            check_token(r, 0x3000u);

            r = decode("\xA1\xFE"sv);
            check_token(r, 0x25C7u);

            r = decode("\xA1\x00"sv);
            BOOST_CHECK(r.is_error());
        }

        BOOST_AUTO_TEST_CASE(decode_three_bytes)
        {
            auto r = decode("\x8F\xA0\xC2"sv);
            BOOST_CHECK(r.is_error());

            r = decode("\x8F\xA2\xC2"sv);
            check_token(r, 0xA1u);
        }

        BOOST_AUTO_TEST_CASE(decoder_factory)
        {
            euc_jp_decoder c1;

            auto const c2 = c1.create();

            BOOST_CHECK(c2);
        }

        static auto
        encode(string_view_t const s)
        {
            euc_jp_encoder e;

            stream_t is { s };

            return e.handler(is, is.read());
        }

        BOOST_AUTO_TEST_CASE(encode_empty)
        {
            auto r = encode(U""sv);
            BOOST_CHECK(r.is_finished());
        }

        BOOST_AUTO_TEST_CASE(encode_ascii)
        {
            auto r = encode(U"\x00"sv);
            check_token(r, 0x00u);

            r = encode(U"\x7F"sv);
            check_token(r, 0x7Fu);

            r = encode(U"\xA5"sv);
            check_token(r, 0x5Cu);

            r = encode(U"\x203"sv);
            check_token(r, 0x7Eu);
        }

        BOOST_AUTO_TEST_CASE(encode_two_bytes)
        {
            auto r = encode(U"\xFF61"sv);
            check_tokens(r, "\x8E\xA1"sv);

            r = encode(U"\xFF9F"sv);
            check_tokens(r, "\x8E\xDF"sv);

            r = encode(U"\xFFA0"sv);
            BOOST_REQUIRE(r.is_error());
            BOOST_REQUIRE_EQUAL(r.size(), 1);
            BOOST_CHECK_EQUAL(r[0], 0xFFA0u);

            r = encode(U"\x2212"sv);
            check_tokens(r, "\xA1\xDD"sv);
        }

        BOOST_AUTO_TEST_CASE(encoder_factory)
        {
            euc_jp_encoder c1;

            auto const c2 = c1.create();

            BOOST_CHECK(c2);
        }

    BOOST_AUTO_TEST_SUITE_END() // euc_jp_

BOOST_AUTO_TEST_SUITE_END() // japanese

BOOST_AUTO_TEST_SUITE_END() // encodings_

} // namespace whatwg::encoding::japanese
