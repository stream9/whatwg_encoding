#include <whatwg/encoding/encodings/chinese_traditional.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::encoding::chinese {

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(encodings_)

BOOST_AUTO_TEST_SUITE(chinese_)

    static void
    check_token(result_t const& r, token_t const t)
    {
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE_EQUAL(r.size(), 1);
        BOOST_CHECK_EQUAL(r[0], t);
    }

    static void
    check_tokens(result_t const& r, byte_sequence_view_t const t)
    {
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE_EQUAL(r.size(), t.size());

        for (size_t i = 0; i < r.size(); ++i) {
            BOOST_CHECK_EQUAL(r[i], to_token(t[i]));
        }
    }

    BOOST_AUTO_TEST_SUITE(big5_)

        static auto
        decode(byte_sequence_view_t const s)
        {
            big5_decoder d;

            byte_stream_t is { s };

            while (true) {
                auto const r = d.handler(is, is.read());

                if (!r.is_continue()) return r;
            }
        }

        BOOST_AUTO_TEST_CASE(decode_empty)
        {
            auto r = decode(""sv);
            BOOST_REQUIRE(r.is_finished());
        }

        BOOST_AUTO_TEST_CASE(decode_single_byte)
        {
            auto r = decode("\x00"sv);
            check_token(r, 0x00u);

            r = decode("\x7F"sv);
            check_token(r, 0x7Fu);

            r = decode("\x80"sv);
            BOOST_CHECK(r.is_error());

            r = decode("\x81"sv);
            BOOST_CHECK(r.is_error());

            r = decode("\xFE"sv);
            BOOST_CHECK(r.is_error());

            r = decode("\xFF"sv);
            BOOST_CHECK(r.is_error());
        }

        BOOST_AUTO_TEST_CASE(decode_two_bytes)
        {
            auto r = decode("\xA1\x40"sv);
            check_token(r, 0x3000u);

            r = decode("\xA1\x7E"sv);
            check_token(r, 0xFE5Au);

            r = decode("\xA1\x7F"sv);
            BOOST_CHECK(r.is_error());

            r = decode("\xA1\xA0"sv);
            BOOST_CHECK(r.is_error());

            r = decode("\xA1\xA1"sv);
            check_token(r, 0xFE5Bu);

            r = decode("\xA1\xFE"sv);
            check_token(r, 0xFF0Fu);

            r = decode("\xA1\xFF"sv);
            BOOST_CHECK(r.is_error());
        }

        BOOST_AUTO_TEST_CASE(decode_to_two_code_point)
        {
            auto r = decode("\x88\x62"sv);
            BOOST_REQUIRE(r.is_tokens());
            BOOST_REQUIRE_EQUAL(r.size(), 2);
            BOOST_CHECK_EQUAL(r[0], 0xCAu);
            BOOST_CHECK_EQUAL(r[1], 0x304u);

            r = decode("\x88\x64"sv);
            BOOST_REQUIRE(r.is_tokens());
            BOOST_REQUIRE_EQUAL(r.size(), 2);
            BOOST_CHECK_EQUAL(r[0], 0xCAu);
            BOOST_CHECK_EQUAL(r[1], 0x30Cu);

            r = decode("\x88\xA3"sv);
            BOOST_REQUIRE(r.is_tokens());
            BOOST_REQUIRE_EQUAL(r.size(), 2);
            BOOST_CHECK_EQUAL(r[0], 0xEAu);
            BOOST_CHECK_EQUAL(r[1], 0x304u);

            r = decode("\x88\xA5"sv);
            BOOST_REQUIRE(r.is_tokens());
            BOOST_REQUIRE_EQUAL(r.size(), 2);
            BOOST_CHECK_EQUAL(r[0], 0xEAu);
            BOOST_CHECK_EQUAL(r[1], 0x30Cu);
        }

        BOOST_AUTO_TEST_CASE(decoder_factory)
        {
            big5_decoder c1;

            auto const c2 = c1.create();

            BOOST_CHECK(c2);
        }

        static auto
        encode(string_view_t const s)
        {
            big5_encoder e;

            stream_t is { s };

            return e.handler(is, is.read());
        }

        BOOST_AUTO_TEST_CASE(encode_empty)
        {
            auto r = encode(U""sv);
            BOOST_CHECK(r.is_finished());
        }

        BOOST_AUTO_TEST_CASE(encode_ascii)
        {
            auto r = encode(U"\x00"sv);
            check_token(r, 0x00u);

            r = encode(U"\x7F"sv);
            check_token(r, 0x7Fu);
        }

        BOOST_AUTO_TEST_CASE(encode_beyond_ascii)
        {
            auto r = encode(U"\xA6"sv);
            BOOST_REQUIRE(r.is_error());
            BOOST_REQUIRE_EQUAL(r.size(), 1);
            BOOST_CHECK_EQUAL(r[0], 0xA6u);

            r = encode(U"\xA7"sv);
            check_tokens(r, "\xA1\xB1"sv);

            r = encode(U"\xFFED"sv);
            check_tokens(r, "\xF9\xFE"sv);

            r = encode(U"\x2550"sv);
            check_tokens(r, "\xF9\xF9"sv);

            r = encode(U"\x255E"sv);
            check_tokens(r, "\xF9\xE9"sv);

            r = encode(U"\x2561"sv);
            check_tokens(r, "\xF9\xEB"sv);

            r = encode(U"\x256A"sv);
            check_tokens(r, "\xF9\xEA"sv);

            r = encode(U"\x5341"sv);
            check_tokens(r, "\xA4\x51"sv);

            r = encode(U"\x5345"sv);
            check_tokens(r, "\xA4\xCA"sv);
        }

        BOOST_AUTO_TEST_CASE(encoder_factory)
        {
            big5_encoder c1;

            auto const c2 = c1.create();

            BOOST_CHECK(c2);
        }

    BOOST_AUTO_TEST_SUITE_END() // big5_

BOOST_AUTO_TEST_SUITE_END() // chinese_

BOOST_AUTO_TEST_SUITE_END() // encodings_

} // namespace whatwg::encoding::chinese
