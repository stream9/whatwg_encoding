#include <whatwg/encoding/encodings/utf8.hpp>

#include <whatwg/encoding/result.hpp>
#include <whatwg/encoding/stream.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::encoding::utf8::testing {

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(encodings_)

BOOST_AUTO_TEST_SUITE(utf8_)

    BOOST_AUTO_TEST_SUITE(decoder_)

        static void read_decode_check(decoder& d,
                                      byte_stream_t& is,
                                      result_t const expected)
        {
            auto const r = d.handler(is, is.read());
            BOOST_CHECK_EQUAL(r, expected);
        }

        BOOST_AUTO_TEST_CASE(end_of_stream_)
        {
            utf8::decoder d;

            byte_stream_t is;

            read_decode_check(d, is, finished);
        }

        BOOST_AUTO_TEST_CASE(single_byte)
        {
            utf8::decoder d;
            byte_stream_t is;

            is = "\0"sv;
            read_decode_check(d, is, 0);

            is = "\x7F"sv;
            read_decode_check(d, is, U'\x7F');
        }

        BOOST_AUTO_TEST_CASE(two_bytes_sequence)
        {
            utf8::decoder d;
            byte_stream_t is;

            is = "\xC2\x80"sv;
            read_decode_check(d, is, continue_);
            read_decode_check(d, is, U'\x80');

            is = "\xDF\xBF"sv;
            read_decode_check(d, is, continue_);
            read_decode_check(d, is, U'\x7FF');
        }

        BOOST_AUTO_TEST_CASE(three_bytes_sequence)
        {
            utf8::decoder d;
            byte_stream_t is;

            is = "\xE0\xA0\x80"sv;
            read_decode_check(d, is, continue_);
            read_decode_check(d, is, continue_);
            read_decode_check(d, is, U'\x800');

            is = "\xED\x80\x9F"sv; // corner case in the source code
            read_decode_check(d, is, continue_);
            read_decode_check(d, is, continue_);
            read_decode_check(d, is, U'\xD01F');

            is = "\xEF\xBF\xBF"sv;
            read_decode_check(d, is, continue_);
            read_decode_check(d, is, continue_);
            read_decode_check(d, is, U'\xFFFF');
        }

        BOOST_AUTO_TEST_CASE(four_bytes_sequence)
        {
            utf8::decoder d;
            byte_stream_t is;

            is = "\xF0\x90\x80\x80"sv;
            read_decode_check(d, is, continue_);
            read_decode_check(d, is, continue_);
            read_decode_check(d, is, continue_);
            read_decode_check(d, is, U'\x10000');

            is = "\xF4\x8F\xBF\xBF"sv;
            read_decode_check(d, is, continue_);
            read_decode_check(d, is, continue_);
            read_decode_check(d, is, continue_);
            read_decode_check(d, is, U'\x10FFFF');
        }

        BOOST_AUTO_TEST_CASE(invalid_fist_byte)
        {
            utf8::decoder d;
            byte_stream_t is;

            is = "\xFF"sv;
            read_decode_check(d, is, error);
        }

        BOOST_AUTO_TEST_CASE(invalid_second_byte)
        {
            utf8::decoder d;
            byte_stream_t is;

            is = "\xC2\xC0"sv;
            read_decode_check(d, is, continue_);
            read_decode_check(d, is, error);
        }

        BOOST_AUTO_TEST_CASE(premature_end_of_stream)
        {
            utf8::decoder d;
            byte_stream_t is;

            is = "\xC2"sv;
            read_decode_check(d, is, continue_);
            read_decode_check(d, is, error);
        }

    BOOST_AUTO_TEST_SUITE_END() // decoder_

    BOOST_AUTO_TEST_SUITE(encoder_)

        static void check_bytes(result_t const& r, char const c)
        {
            BOOST_CHECK(r.is_tokens());

            BOOST_CHECK_EQUAL(r.size(), 1);
            BOOST_CHECK_EQUAL(r[0], to_token(c));
        }

        static void check_bytes(result_t const& r, char const c1, char const c2)
        {
            BOOST_CHECK(r.is_tokens());

            BOOST_REQUIRE_EQUAL(r.size(), 2);
            BOOST_CHECK_EQUAL(r[0], to_token(c1));
            BOOST_CHECK_EQUAL(r[1], to_token(c2));
        }

        static void check_bytes(result_t const& r,
            char const c1, char const c2, char const c3)
        {
            BOOST_CHECK(r.is_tokens());

            BOOST_REQUIRE_EQUAL(r.size(), 3);
            BOOST_CHECK_EQUAL(r[0], to_token(c1));
            BOOST_CHECK_EQUAL(r[1], to_token(c2));
            BOOST_CHECK_EQUAL(r[2], to_token(c3));
        }

        static void check_bytes(result_t const& r,
            char const c1, char const c2, char const c3, char const c4)
        {
            BOOST_CHECK(r.is_tokens());

            BOOST_REQUIRE_EQUAL(r.size(), 4);
            BOOST_CHECK_EQUAL(r[0], to_token(c1));
            BOOST_CHECK_EQUAL(r[1], to_token(c2));
            BOOST_CHECK_EQUAL(r[2], to_token(c3));
            BOOST_CHECK_EQUAL(r[3], to_token(c4));
        }

        BOOST_AUTO_TEST_CASE(end_of_stream_)
        {
            utf8::encoder e;
            stream_t is;

            auto const r = e.handler(is, end_of_stream);

            BOOST_CHECK(r.is_finished());
        }

        BOOST_AUTO_TEST_CASE(ascii_code_point)
        {
            utf8::encoder e;
            stream_t is;

            auto r = e.handler(is, 0);
            check_bytes(r, 0);

            r = e.handler(is, U'\x7F');
            check_bytes(r, '\x7F');

            r = e.handler(is, U'\x80');
            BOOST_REQUIRE(r.is_tokens());
            BOOST_CHECK_NE(r.size(), 1);
        }

        BOOST_AUTO_TEST_CASE(two_byte_sequence)
        {
            utf8::encoder e;
            stream_t is;

            auto r = e.handler(is, U'\x80');
            check_bytes(r, '\xC2', '\x80');

            r = e.handler(is, U'\x7FF');
            check_bytes(r, '\xDF', '\xBF');

            r = e.handler(is, U'\x800');
            BOOST_REQUIRE(r.is_tokens());
            BOOST_CHECK_NE(r.size(), 2);
        }

        BOOST_AUTO_TEST_CASE(three_byte_sequence)
        {
            utf8::encoder e;
            stream_t is;

            auto r = e.handler(is, U'\x800');
            check_bytes(r, '\xE0', '\xA0', '\x80');

            r = e.handler(is, U'\xFFFF');
            check_bytes(r, '\xEF', '\xBF', '\xBF');

            r = e.handler(is, U'\x10000');
            BOOST_REQUIRE(r.is_tokens());
            BOOST_CHECK_NE(r.size(), 3);
        }

        BOOST_AUTO_TEST_CASE(four_byte_sequence)
        {
            utf8::encoder e;
            stream_t is;

            auto r = e.handler(is, U'\x10000');
            check_bytes(r, '\xF0', '\x90', '\x80', '\x80');

            r = e.handler(is, U'\x10FFFF');
            check_bytes(r, '\xF4', '\x8F', '\xBF', '\xBF');
        }

        BOOST_AUTO_TEST_CASE(invalid_token)
        {
            utf8::encoder e;
            stream_t is;

            auto const r = e.handler(is, U'\x110000');
            BOOST_REQUIRE(r.is_error());

            BOOST_REQUIRE_EQUAL(r.size(), 1);
            BOOST_CHECK_EQUAL(r[0], U'\x110000');
        }

    BOOST_AUTO_TEST_SUITE_END() // encoder_

BOOST_AUTO_TEST_SUITE_END() // utf8_

BOOST_AUTO_TEST_SUITE_END() // encodings_

} // namespace whatwg::encoding::utf8::testing
