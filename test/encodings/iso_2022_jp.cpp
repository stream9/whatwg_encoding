#include <whatwg/encoding/encodings/iso_2022_jp.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::encoding::japanese::testing {

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(encodings_)

BOOST_AUTO_TEST_SUITE(japanese)

    static void
    check_token(result_t const& r, token_t const t)
    {
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE_EQUAL(r.size(), 1);
        BOOST_CHECK_EQUAL(r[0], t);
    }

    static void
    check_tokens(result_t const& r, byte_sequence_view_t const t)
    {
        BOOST_REQUIRE(r.is_tokens());
        BOOST_REQUIRE_EQUAL(r.size(), t.size());

        for (size_t i = 0; i < r.size(); ++i) {
            BOOST_CHECK_EQUAL(r[i], to_token(t[i]));
        }
    }

    BOOST_AUTO_TEST_SUITE(iso_2022_jp_)

        static auto
        decode(decoder& d, byte_sequence_view_t const s)
        {
            byte_stream_t is { s };

            while (true) {
                auto const r = d.handler(is, is.read());

                if (!r.is_continue()) return r;
            }
        }

        BOOST_AUTO_TEST_CASE(decode_empty)
        {
            iso_2022_jp_decoder d;

            auto r = decode(d, ""sv);
            BOOST_REQUIRE(r.is_finished());
        }

        BOOST_AUTO_TEST_CASE(decode_ascii)
        {
            iso_2022_jp_decoder d;

            auto r = decode(d, "\x00"sv);
            check_token(r, 0x00u);

            r = decode(d, "\x7F"sv);
            check_token(r, 0x7Fu);

            r = decode(d, "\x80"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode(d, "\x0E"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode(d, "\x0F"sv);
            BOOST_REQUIRE(r.is_error());
        }

        BOOST_AUTO_TEST_CASE(decode_escape)
        {
            iso_2022_jp_decoder d;

            auto r = decode(d, "\x1B\x00"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode(d, "\x1B\x28\x42"sv); // to ascii
            BOOST_REQUIRE(r.is_finished());

            r = decode(d, "\x1B\x28\x4A"sv); // to roman
            BOOST_REQUIRE(r.is_error());

            r = decode(d, "\x00"sv); // reset output flag

            r = decode(d, "\x1B\x28\x4A"sv); // to roman
            BOOST_REQUIRE(r.is_finished());

            r = decode(d, "\x00"sv); // reset output flag

            r = decode(d, "\x1B\x28\x49"sv); // to katakana
            BOOST_REQUIRE(r.is_finished());

            r = decode(d, "\x00"sv); // reset output flag

            r = decode(d, "\x1B\x24\x40"sv); // to lead byte
            BOOST_REQUIRE(r.is_finished());

            r = decode(d, "\x00"sv); // reset output flag

            r = decode(d, "\x1B\x24\x42"sv); // to lead byte
            BOOST_REQUIRE(r.is_finished());

            r = decode(d, "\x00"sv); // reset output flag

            r = decode(d, "\x1B\x24\x00"sv);
            BOOST_REQUIRE(r.is_error());
        }

        BOOST_AUTO_TEST_CASE(decode_roman)
        {
            iso_2022_jp_decoder d;

            auto r = decode(d, "\x1B\x28\x4A"sv); // to roman
            BOOST_REQUIRE(r.is_finished());

            r = decode(d, "\x5C"sv);
            check_token(r, 0xA5u);

            r = decode(d, "\x7E"sv);
            check_token(r, 0x203Eu);

            r = decode(d, "\x00"sv);
            check_token(r, 0x00u);

            r = decode(d, "\x7F"sv);
            check_token(r, 0x7Fu);

            r = decode(d, "\x0E"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode(d, "\x0F"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode(d, "\x1B\x28\x42"sv); // to ascii
            BOOST_REQUIRE(r.is_finished());
        }

        BOOST_AUTO_TEST_CASE(decode_katakana)
        {
            iso_2022_jp_decoder d;

            auto r = decode(d, "\x1B\x28\x49"sv); // to katakana
            BOOST_REQUIRE(r.is_finished());

            r = decode(d, "\x20"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode(d, "\x21"sv);
            check_token(r, 0xFF61u);

            r = decode(d, "\x5F"sv);
            check_token(r, 0xFF9Fu);

            r = decode(d, "\x60"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode(d, "\x1B\x28\x42"sv); // to ascii
            BOOST_REQUIRE(r.is_finished());
        }

        BOOST_AUTO_TEST_CASE(decode_two_bytes)
        {
            iso_2022_jp_decoder d;

            auto r = decode(d, "\x1B\x24\x40"sv); // to lead byte
            BOOST_REQUIRE(r.is_finished());

            r = decode(d, "\x20"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode(d, "\x21"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode(d, "\x21\x20"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode(d, "\x21\x21"sv);
            check_token(r, 0x3000u);

            r = decode(d, "\x7E\x21"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode(d, "\x7E\x1B"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode(d, "\x7F"sv);
            BOOST_REQUIRE(r.is_error());

            r = decode(d, "\x1B\x28\x42"sv); // to ascii
            BOOST_REQUIRE(r.is_finished());
        }

        BOOST_AUTO_TEST_CASE(decoder_factory)
        {
            iso_2022_jp_decoder c1;

            auto const c2 = c1.create();

            BOOST_CHECK(c2);
        }

        static auto
        encode(string_view_t const s)
        {
            iso_2022_jp_encoder e;

            stream_t is { s };

            return e.handler(is, is.read());
        }

        BOOST_AUTO_TEST_CASE(encode_empty)
        {
            auto r = encode(U""sv);
            BOOST_CHECK(r.is_finished());
        }

        BOOST_AUTO_TEST_CASE(encode_ascii)
        {
            auto r = encode(U"\x00"sv);
            check_token(r, 0x00u);

            r = encode(U"\x7F"sv);
            check_token(r, 0x7Fu);

            r = encode(U"\x0E"sv);
            BOOST_REQUIRE(r.is_error());
            BOOST_REQUIRE_EQUAL(r.size(), 1);
            BOOST_REQUIRE_EQUAL(r[0], 0xFFFDu);

            r = encode(U"\x0F"sv);
            BOOST_REQUIRE(r.is_error());
            BOOST_REQUIRE_EQUAL(r.size(), 1);
            BOOST_REQUIRE_EQUAL(r[0], 0xFFFDu);

            r = encode(U"\x1B"sv);
            BOOST_REQUIRE(r.is_error());
            BOOST_REQUIRE_EQUAL(r.size(), 1);
            BOOST_REQUIRE_EQUAL(r[0], 0xFFFDu);
        }

        BOOST_AUTO_TEST_CASE(encode_roman)
        {
            iso_2022_jp_encoder e;

            stream_t is { U"\xA5"sv };

            auto r = e.handler(is, is.read());
            check_tokens(r, "\x1B\x28\x4A"sv); // roman

            r = e.handler(is, is.read());
            check_tokens(r, "\x5C"sv);

            is = U"\x203E"sv;
            r = e.handler(is, is.read());
            check_tokens(r, "\x7E"sv);

            is = U"\x00"sv;
            r = e.handler(is, is.read());
            check_tokens(r, "\x00"sv);

            is = U"\x7F"sv;
            r = e.handler(is, is.read());
            check_tokens(r, "\x7F"sv);

            is = U"\x80"sv;
            r = e.handler(is, is.read());
            BOOST_CHECK(r.is_error());

            is = U"\x5C"sv;
            r = e.handler(is, is.read());
            check_tokens(r, "\x1B\x28\x42"sv); // ascii

            is = U"\x203E"sv;
            r = e.handler(is, is.read());
            check_tokens(r, "\x1B\x28\x4A"sv); // roman

            is = U"\x7E"sv;
            r = e.handler(is, is.read());
            check_tokens(r, "\x1B\x28\x42"sv); // ascii
        }

        BOOST_AUTO_TEST_CASE(encode_katakana)
        {
            iso_2022_jp_encoder e;

            stream_t is;

            is = U"\xFF61"sv;
            auto r = e.handler(is, is.read());
            check_tokens(r, "\x1B\x24\x42"sv); // jis0208

            r = e.handler(is, is.read());
            check_tokens(r, "\x21\x23"sv);

            is = U"\xFF9F"sv;
            r = e.handler(is, is.read());
            check_tokens(r, "\x21\x2C"sv);

            is = U""sv;
            r = e.handler(is, is.read());
            check_tokens(r, "\x1B\x28\x42"sv); // ascii
        }

        BOOST_AUTO_TEST_CASE(encode_jis0208)
        {
            iso_2022_jp_encoder e;

            stream_t is;

            is = U"\x2212"sv;
            auto r = e.handler(is, is.read());
            check_tokens(r, "\x1B\x24\x42"sv); // jis0208
        }

        BOOST_AUTO_TEST_CASE(encoder_factory)
        {
            iso_2022_jp_encoder c1;

            auto const c2 = c1.create();

            BOOST_CHECK(c2);
        }

    BOOST_AUTO_TEST_SUITE_END() // iso_2022_jp_

BOOST_AUTO_TEST_SUITE_END() // japanese

BOOST_AUTO_TEST_SUITE_END() // encodings_

} // namespace whatwg::encoding::japanese::testing
