find_package(Boost 1.67 REQUIRED COMPONENTS
    unit_test_framework
)

add_definitions(-DBOOST_TEST_DYN_LINK)

add_executable(test_whatwg_encoding
    main.cpp
    error.cpp
    algorithm.cpp
    stream.cpp
    result.cpp
    encoding.cpp
    index.cpp
    encoder_decoder.cpp
    encodings/utf8.cpp
    encodings/replacement.cpp
    encodings/utf16.cpp
    encodings/single_byte.cpp
    encodings/chinese_simplified.cpp
    encodings/chinese_traditional.cpp
    encodings/euc_jp.cpp
    encodings/iso_2022_jp.cpp
    encodings/shift_jis.cpp
    encodings/euc_kr.cpp
    encodings/user_defined.cpp
    text_decoder.cpp
    text_encoder.cpp
)

target_include_directories(test_whatwg_encoding PRIVATE
    ${PROJECT_SOURCE_DIR}
)

target_link_libraries(test_whatwg_encoding
    ${Boost_LIBRARIES}
    whatwg_encoding
)

if (TEST_COVERAGE)
    add_custom_target(test_whatwg_encoding_coverage
        COMMAND find . -name '*.gcda' -delete
        COMMAND test_whatwg_encoding
        COMMAND ${CMAKE_COMMAND} -E make_directory coverage
        COMMAND gcovr --html-details
                    --root ${PROJECT_SOURCE_DIR}
                    --exclude ${PROJECT_SOURCE_DIR}/test
                    --exclude ${PROJECT_SOURCE_DIR}/third_party
                    --exclude ${PROJECT_SOURCE_DIR}/src/convert_index.cpp
                    -o coverage/whatwg_encoding_test.html
        COMMAND xdg-open coverage/whatwg_encoding_test.html &
        WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
        DEPENDS test_whatwg_encoding
    )
endif()
