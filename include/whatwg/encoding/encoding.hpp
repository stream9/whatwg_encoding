//! @file
#ifndef WHATWG_ENCODING_ENCODING_HPP
#define WHATWG_ENCODING_ENCODING_HPP

#include "encoder_decoder.hpp"
#include "stream_fwd.hpp"
#include "types.hpp"

namespace whatwg::encoding {

// 4. Encodings
class encoding
{
public:
    using encoder_t = whatwg::encoding::encoder;
    using decoder_t = whatwg::encoding::decoder;

public:
    enum class id {
        utf_8, ibm866, iso_8859_2, iso_8859_3, iso_8859_4, iso_8859_5,
        iso_8859_6, iso_8859_7, iso_8859_8, iso_8859_8_i, iso_8859_10,
        iso_8859_13, iso_8859_14, iso_8859_15, iso_8859_16, koi8_r,
        koi8_u, macintosh, windows_874, windows_1250, windows_1251,
        windows_1252, windows_1253, windows_1254, windows_1255,
        windows_1256, windows_1257, windows_1258, x_mac_cyrillic, gbk,
        gb18030, big5, euc_jp, iso_2022_jp, shift_jis, euc_kr,
        replacement, utf_16be, utf_16le, x_user_defined, count
    };

public:
    encoding(id);

    byte_sequence_view_t name() const;

    encoder_t const* encoder() const;
    decoder_t const& decoder() const;

private:
    id m_id;
    mutable encoder_ptr m_encoder;
    mutable decoder_ptr m_decoder;
};

//! @brief Get an encoding object that correspond to the given label.
//! @return pointer to the encoding, nullptr if there isn't one.
//!
//! §[4.2. Names and labels](https://encoding.spec.whatwg.org/#names-and-labels)
encoding const* get_an_encoding(byte_sequence_view_t label);

//! @brief Get an out encoding object that correspond to the given encoding.
//!
//! §[4.3 Output encodings](https://encoding.spec.whatwg.org/#output-encodings)
encoding const& get_an_output_encoding(encoding const&);

//! @brief Decode a byte stream
//!
//! @param fallback      fallback encoding
//!
//! @return code point stream which contains decoded string
//!
//! - Encoding is determined by examining byte order mark in the input stream and
//! if it can't recognize BOM use given encoding as a fallback.
//! - Error will be replaced by replacement character.
//!
//! §[6. Hooks for standards](https://encoding.spec.whatwg.org/#specification-hooks)
code_point_stream_t
    decode(byte_stream_t&, encoding const& fallback);

//! @brief Decode a byte stream with UTF-8 encoding
//!
//! @return code point stream which contains decoded string
//!
//! - UTF-8 BOM in the input stream will be skipped.
//! - Error will be replaced by replacement character.
//!
//! §[6. Hooks for standards](https://encoding.spec.whatwg.org/#specification-hooks)
code_point_stream_t
    utf8_decode(byte_stream_t&);

//! @brief Decode a byte stream with UTF-8 encoding
//!
//! @return code point stream which contains decoded string
//!
//! - It assumes the input doesn't have a BOM. If there is an UTF-8 BOM in the
//! input, it will be decoded as a normal data.
//! - Error will be replaced by replacement character.
//!
//! §[6. Hooks for standards](https://encoding.spec.whatwg.org/#specification-hooks)
code_point_stream_t
    utf8_decode_without_bom(byte_stream_t&);

//! @brief Decode a byte stream with UTF-8 encoding
//!
//! @return decoded stream on success, null otherwise.
//!
//! It assumes the input doesn't have a BOM. If there is an UTF-8 BOM in the
//! input, it will be regarded as an error.
//!
//! §[6. Hooks for standards](https://encoding.spec.whatwg.org/#specification-hooks)
optional<code_point_stream_t>
    utf8_decode_without_bom_or_fail(byte_stream_t&);

//! @brief Encode a code point stream with given encoding.
//!
//! @return byte stream that contains encoded bytes
//!
//! An encoding must not be "replacement", "UTF-16BE" or "UTF-16LE"
//!
//! §[6. Hooks for standards](https://encoding.spec.whatwg.org/#specification-hooks)
byte_stream_t
    encode(code_point_stream_t&, encoding const&);

//! @brief Encode a code point stream with UTF-8 encoding.
//!
//! @return byte stream that contains encoded bytes
//!
//! §[6. Hooks for standards](https://encoding.spec.whatwg.org/#specification-hooks)
byte_stream_t
    utf8_encode(code_point_stream_t&);

} // namespace whatwg::encoding

namespace std {

template<>
class numeric_limits<whatwg::encoding::encoding::id>
    : public numeric_limits<
        std::underlying_type_t<whatwg::encoding::encoding::id> >
{};

} // namespace std

#endif // WHATWG_ENCODING_ENCODING_HPP
