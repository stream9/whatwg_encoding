//! @file
#ifndef WHATWG_ENCODING_ENCODER_DECODER_HPP
#define WHATWG_ENCODING_ENCODER_DECODER_HPP

#include "error.hpp"
#include "stream_fwd.hpp"
#include "types.hpp"
#include "result.hpp"

#include <memory>

namespace whatwg::encoding {

// 4.1 Encoders and Decoders
class encoder;
using encoder_ptr = std::unique_ptr<encoder>;

class encoder
{
public:
    virtual ~encoder() = default;
    virtual result_t handler(stream_t&, token_t code_point) = 0;
    virtual encoder_ptr create() const = 0;
};

class decoder;
using decoder_ptr = std::unique_ptr<decoder>;

class decoder
{
public:
    virtual ~decoder() = default;
    virtual result_t handler(byte_stream_t&, token_t byte) = 0;
    virtual decoder_ptr create() const = 0;
};

//! @brief run an encoding's decoder or encoder for all tokens in given input stream
//!
//! @tparam Codec   encoder or decoder
//! @tparam IStream input stream
//! @tparam OStream output stream
//!
//! @return [result_t](@ref whatwg::encoding::result_t):
//!     either 'finished' or 'error'
//!
//! §[4.1. Encoders and decoders](https://encoding.spec.whatwg.org/#encoders-and-decoders)
template<typename Codec, typename IStream, typename OStream>
result_t run(Codec const& encoder_decoder,
             IStream& input,
             OStream& output,
             optional<error_mode> mode = {});

//! @brief process a token with encoder or decoder
//!
//! @tparam Codec   encoder or decoder
//! @tparam Token   token
//! @tparam IStream input stream
//! @tparam OStream output stream
//!
//! @return [result_t](@ref whatwg::encoding::result_t):
//!     either 'finished' or 'error' or 'continue'
//!
//! - token might be push back to the input stream for further processing
//! - successfully processed token will be appended to the output stream
//!
//! §[4.1. Encoders and decoders](https://encoding.spec.whatwg.org/#encoders-and-decoders)
template<typename Codec, typename Token,
         typename IStream, typename OStream>
result_t process(Codec& encoder_decoder_instance,
                 Token token,
                 IStream& input,
                 OStream& output,
                 optional<error_mode> mode = {});

} // namespace whatwg::encoding

#endif // WHATWG_ENCODING_ENCODER_DECODER_HPP
