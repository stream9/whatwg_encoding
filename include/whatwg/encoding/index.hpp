#ifndef WHATWG_ENCODING_INDEX_HPP
#define WHATWG_ENCODING_INDEX_HPP

#include "types.hpp"

#include <cstdint>
#include <unordered_map>

namespace whatwg::encoding {

using pointer_t = uint32_t;

class code_point_index
{
public:
    code_point_index(byte_sequence_view_t encoding_name);

    optional<code_point_t> find(pointer_t) const;

private:
    std::unordered_map<pointer_t, code_point_t> const* m_map; // non-null
};

class pointer_index
{
public:
    pointer_index(byte_sequence_view_t encoding_name);

    optional<pointer_t> find(code_point_t) const;

private:
    std::unordered_multimap<code_point_t, pointer_t> const* m_map; // non-null
};

// free function
optional<code_point_t>
    index_code_point(code_point_index const&, pointer_t);

optional<pointer_t>
    index_pointer(pointer_index const&, code_point_t);

optional<code_point_t>
    index_gb18030_ranges_code_point(pointer_t);

optional<pointer_t>
    index_gb18030_ranges_pointer(code_point_t);

optional<pointer_t>
    index_shift_jis_pointer(code_point_t);

optional<pointer_t>
    index_big5_pointer(code_point_t code_point);

} // namespace whatwg::encoding

#endif // WHATWG_ENCODING_INDEX_HPP
