#ifndef WHATWG_ENCODING_TEXT_DECODER_HPP
#define WHATWG_ENCODING_TEXT_DECODER_HPP

#include "encoder_decoder.hpp"
#include "stream.hpp"
#include "text_decoder_common.hpp"
#include "types.hpp"

namespace whatwg::encoding {

//! @brief High level API for text decoding.
//!
//! Based on §[7.2. Interface TextDecoder](https://encoding.spec.whatwg.org/#interface-textdecoder)
class text_decoder : public text_decoder_common
{
public:
    enum class options {
        no_option = 0,

        //! throw type_error on error, use replacement character otherwise
        fatal = 1,

        //TODO what is this flag's meaning?
        ignore_bom = 2,
    };

public:
    //! @name Constructor
    //! @{

    //! @param label encoding label
    //! @exception range_error when label can't be recognized or "replacement"
    text_decoder(byte_sequence_view_t label = "utf-8",
                 options = {});
    //! @}
    //! @name Command
    //! @{

    //! @brief Decode byte sequence.
    //!
    //! @param stream indicate input sequence is a part of a stream. it has
    //!               to be false when input is the last piece of a stream.
    //!
    //! @return decoded string
    //!
    //! @exception type_error when fatal() is true and a input has an error.
    string_t decode(byte_sequence_view_t input = "",
                    bool stream = false);
    //! @}

private:
    decoder_ptr m_decoder; // not-null
    byte_stream_t m_stream;
    bool m_do_not_flush_flag = false;
};

inline text_decoder::options
operator&(text_decoder::options const lhs, text_decoder::options const rhs)
{
    return static_cast<text_decoder::options>(
        static_cast<int>(lhs) & static_cast<int>(rhs)
    );
}

inline text_decoder::options
operator|(text_decoder::options const lhs, text_decoder::options const rhs)
{
    return static_cast<text_decoder::options>(
        static_cast<int>(lhs) | static_cast<int>(rhs)
    );
}

inline bool
test(text_decoder::options const flags, text_decoder::options const bits)
{
    return static_cast<bool>(flags & bits);
}

} // namespace whatwg::encoding

#endif // WHATWG_ENCODING_TEXT_DECODER_HPP
