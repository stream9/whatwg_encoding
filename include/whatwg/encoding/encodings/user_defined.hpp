#ifndef WHATWG_ENCODING_USER_DEFINED_DECODER_HPP
#define WHATWG_ENCODING_USER_DEFINED_DECODER_HPP

#include "../encoder_decoder.hpp"
#include "../result.hpp"
#include "../stream_fwd.hpp"
#include "../types.hpp"

namespace whatwg::encoding::user_defined {

// 14.5.1 x-user-defined decoder
class decoder : public whatwg::encoding::decoder
{
public:
    decoder() = default;

    result_t handler(byte_stream_t&, token_t byte) override;

    decoder_ptr create() const override;
};

// 14.5.2 x-user-defined encoder
class encoder : public whatwg::encoding::encoder
{
public:
    encoder() = default;

    result_t handler(stream_t&, token_t code_point) override;

    encoder_ptr create() const override;
};

} // namespace whatwg::encoding::user_defined

#endif // WHATWG_ENCODING_USER_DEFINED_DECODER_HPP

