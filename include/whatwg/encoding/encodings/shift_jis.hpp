#ifndef WHATWG_ENCODING_ENCODINGS_SHIFT_JIS_HPP
#define WHATWG_ENCODING_ENCODINGS_SHIFT_JIS_HPP

#include "../encoder_decoder.hpp"
#include "../result.hpp"
#include "../stream.hpp"
#include "../types.hpp"
#include "../index.hpp"

namespace whatwg::encoding::japanese {

// 12.3.1. Shift_JIS decoder
class shift_jis_decoder : public whatwg::encoding::decoder
{
public:
    shift_jis_decoder();

    result_t handler(byte_stream_t&, token_t byte) override;

    decoder_ptr create() const override;

private:
    code_point_index m_index_jis0208;
    token_t m_shift_jis_lead = 0x00u;
};

// 12.3.2. Shift_JIS encoder
class shift_jis_encoder : public whatwg::encoding::encoder
{
public:
    shift_jis_encoder();

    result_t handler(stream_t&, token_t code_point) override;

    encoder_ptr create() const override;
};

} // namespace whatwg::encoding::japanese

#endif // WHATWG_ENCODING_ENCODINGS_SHIFT_JIS_HPP
