#ifndef WHATWG_ENCODING_ENCODINGS_UTF16_HPP
#define WHATWG_ENCODING_ENCODINGS_UTF16_HPP

#include "../encoder_decoder.hpp"
#include "../result.hpp"
#include "../stream.hpp"
#include "../types.hpp"

namespace whatwg::encoding::utf16 {

// 14.2.1. shared UTF-16 decoder
class shared_decoder : public whatwg::encoding::decoder
{
public:
    shared_decoder(bool utf16be_decoder_flag);

    result_t handler(byte_stream_t&, token_t byte) override;

    decoder_ptr create() const override;

private:
    optional<token_t> m_utf16_lead_byte;
    optional<token_t> m_utf16_lead_surrogate;
    bool m_utf16be_decoder_flag = false;
};

// 14.3.1 UTF-16BE decoder
class utf16be_decoder : public shared_decoder
{
public:
    utf16be_decoder();
};

// 14.4.1 UTF-16LE decoder
class utf16le_decoder : public shared_decoder
{
public:
    utf16le_decoder();
};

} // namespace whatwg::encoding::utf16

#endif // WHATWG_ENCODING_ENCODINGS_UTF16_HPP
