#ifndef WHATWG_ENCODING_ENCODINGS_UTF8_HPP
#define WHATWG_ENCODING_ENCODINGS_UTF8_HPP

#include "../encoder_decoder.hpp"
#include "../result.hpp"
#include "../stream_fwd.hpp"
#include "../types.hpp"

namespace whatwg::encoding::utf8 {

// 8.1.1 UTF-8 decoder
class decoder : public whatwg::encoding::decoder
{
public:
    decoder() = default;

    result_t handler(byte_stream_t&, token_t byte) override;

    decoder_ptr create() const override;

private:
    code_point_t m_utf8_code_point = 0;
    int  m_utf8_bytes_seen = 0;
    int  m_utf8_bytes_needed = 0;
    token_t m_utf8_lower_boundary = 0x80u;
    token_t m_utf8_upper_boundary = 0xBFu;
};

// 8.1.2 UTF-8 encoder
class encoder : public whatwg::encoding::encoder
{
public:
    encoder() = default;

    result_t handler(stream_t&, token_t code_point) override;

    encoder_ptr create() const override;
};

} // namespace whatwg::encoding::utf8

#endif // WHATWG_ENCODING_ENCODINGS_UTF8_HPP
