#ifndef WHATWG_ENCODING_REPLACEMENT_DECODER_HPP
#define WHATWG_ENCODING_REPLACEMENT_DECODER_HPP

#include "../encoder_decoder.hpp"
#include "../result.hpp"
#include "../stream.hpp"
#include "../types.hpp"

namespace whatwg::encoding::replacement {

// 14.1.1 replacement decoder
class decoder : public whatwg::encoding::decoder
{
public:
    decoder() = default;

    result_t handler(byte_stream_t&, token_t byte) override;

    decoder_ptr create() const override;

private:
    bool m_replacement_error_returned_flag = false;
};

} // namespace whatwg::encoding::replacement

#endif // WHATWG_ENCODING_REPLACEMENT_DECODER_HPP

