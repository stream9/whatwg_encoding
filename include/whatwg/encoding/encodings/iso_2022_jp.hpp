#ifndef WHATWG_ENCODING_ENCODINGS_ISO_2022_JP_HPP
#define WHATWG_ENCODING_ENCODINGS_ISO_2022_JP_HPP

#include "../encoder_decoder.hpp"
#include "../result.hpp"
#include "../stream.hpp"
#include "../types.hpp"
#include "../index.hpp"

namespace whatwg::encoding::japanese {

// 12.2.1. ISO-2022-JP decoder
class iso_2022_jp_decoder : public whatwg::encoding::decoder
{
public:
    iso_2022_jp_decoder();

    result_t handler(byte_stream_t&, token_t byte) override;

    decoder_ptr create() const override;

private:
    enum class state {
        ascii, roman, katakana, lead_byte, trail_byte, escape_start, escape };

    code_point_index m_index_jis0208;
    state m_iso_2022_jp_decoder_state = state::ascii;
    state m_iso_2022_jp_decoder_output_state = state::ascii;
    token_t m_iso_2022_jp_lead = 0x00u;
    bool m_iso_2022_jp_output_flag = false;
};

// 12.2.2. ISO-2022-JP encoder
class iso_2022_jp_encoder : public whatwg::encoding::encoder
{
public:
    iso_2022_jp_encoder();

    result_t handler(stream_t&, token_t code_point) override;

    encoder_ptr create() const override;

private:
    enum class state { ascii, roman, jis0208 };

    pointer_index m_index_jis0208;
    code_point_index m_index_iso_2022_jp_katakana;
    state m_iso_2022_jp_encoder_state = state::ascii;
};

} // namespace whatwg::encoding::japanese

#endif // WHATWG_ENCODING_ENCODINGS_ISO_2022_JP_HPP
