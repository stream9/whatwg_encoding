#ifndef WHATWG_ENCODING_ENCODINGS_EUC_KR_HPP
#define WHATWG_ENCODING_ENCODINGS_EUC_KR_HPP

#include "../encoder_decoder.hpp"
#include "../result.hpp"
#include "../stream.hpp"
#include "../types.hpp"
#include "../index.hpp"

namespace whatwg::encoding::korean {

// 13.1.1. EUC-KR decoder
class euc_kr_decoder : public whatwg::encoding::decoder
{
public:
    euc_kr_decoder();

    result_t handler(byte_stream_t&, token_t byte) override;

    decoder_ptr create() const override;

private:
    code_point_index m_index_euc_kr;
    token_t m_euc_kr_lead = 0x00u;
};

// 13.1.2. EUC-KR encoder
class euc_kr_encoder : public whatwg::encoding::encoder
{
public:
    euc_kr_encoder();

    result_t handler(stream_t&, token_t code_point) override;

    encoder_ptr create() const override;

private:
    pointer_index m_index_euc_kr;
};

} // namespace whatwg::encoding::korean

#endif // WHATWG_ENCODING_ENCODINGS_EUC_KR_HPP
