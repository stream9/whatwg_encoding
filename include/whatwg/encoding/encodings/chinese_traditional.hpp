#ifndef WHATWG_ENCODING_ENCODINGS_CHINESE_TRADITIONAL_HPP
#define WHATWG_ENCODING_ENCODINGS_CHINESE_TRADITIONAL_HPP

#include "../encoder_decoder.hpp"
#include "../result.hpp"
#include "../stream.hpp"
#include "../types.hpp"
#include "../index.hpp"

namespace whatwg::encoding::chinese {

// 11.1.1. Big5 decoder
class big5_decoder : public whatwg::encoding::decoder
{
public:
    big5_decoder();

    result_t handler(byte_stream_t&, token_t byte) override;

    decoder_ptr create() const override;

private:
    code_point_index m_index;
    token_t m_big5_lead = 0x00;
};

// 11.1.2. Big5 encoder
class big5_encoder : public whatwg::encoding::encoder
{
public:
    big5_encoder();

    result_t handler(stream_t&, token_t code_point) override;

    encoder_ptr create() const override;
};

} // namespace whatwg::encoding::chinese

#endif // WHATWG_ENCODING_ENCODINGS_CHINESE_TRADITIONAL_HPP
