#ifndef WHATWG_ENCODING_ENCODINGS_SINGLE_BYTE_HPP
#define WHATWG_ENCODING_ENCODINGS_SINGLE_BYTE_HPP

#include "../encoder_decoder.hpp"
#include "../result.hpp"
#include "../stream.hpp"
#include "../types.hpp"
#include "../index.hpp"

#include <functional>

namespace whatwg::encoding::single_byte {

// 9.1. single-byte decoder
class decoder : public whatwg::encoding::decoder
{
public:
    decoder(byte_sequence_view_t encoding_name);

    result_t handler(byte_stream_t&, token_t byte) override;

    decoder_ptr create() const override;

private:
    code_point_index m_index;
};

// 9.2. single-byte encoder
class encoder : public whatwg::encoding::encoder
{
public:
    encoder(byte_sequence_view_t encoding_name);

    result_t handler(stream_t&, token_t code_point) override;

    encoder_ptr create() const override;

private:
    pointer_index m_index;
};

} // namespace whatwg::encoding::single_byte

#endif // WHATWG_ENCODING_ENCODINGS_SINGLE_BYTE_HPP
