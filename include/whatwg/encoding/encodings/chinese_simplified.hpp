#ifndef WHATWG_ENCODING_ENCODINGS_CHINESE_SIMPLIFIED_HPP
#define WHATWG_ENCODING_ENCODINGS_CHINESE_SIMPLIFIED_HPP

#include "../encoder_decoder.hpp"
#include "../result.hpp"
#include "../stream.hpp"
#include "../types.hpp"
#include "../index.hpp"

namespace whatwg::encoding::chinese {

// 10.2.1. gb18030 decoder
class gb18030_decoder : public whatwg::encoding::decoder
{
public:
    gb18030_decoder();

    result_t handler(byte_stream_t&, token_t byte) override;

    decoder_ptr create() const override;

private:
    code_point_index m_index;
    token_t m_gb18030_first = 0x00u;
    token_t m_gb18030_second = 0x00u;
    token_t m_gb18030_third = 0x00u;
};

// 10.2.1. gb18030 encoder
class gb18030_encoder : public whatwg::encoding::encoder
{
public:
    gb18030_encoder(bool gbk_flag = false);

    result_t handler(stream_t&, token_t code_point) override;

    encoder_ptr create() const override;

private:
    pointer_index m_index;
    bool m_gbk_flag;
};

// 10.1.1. GBK decoder
class gbk_decoder : public gb18030_decoder {};

// 10.1.2. GBK encoder
class gbk_encoder : public gb18030_encoder
{
public:
    gbk_encoder();
};

} // namespace whatwg::encoding::chinese

#endif // WHATWG_ENCODING_ENCODINGS_CHINESE_SIMPLIFIED_HPP
