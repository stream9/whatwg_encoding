#ifndef WHATWG_ENCODING_ENCODINGS_EUC_JP_HPP
#define WHATWG_ENCODING_ENCODINGS_EUC_JP_HPP

#include "../encoder_decoder.hpp"
#include "../result.hpp"
#include "../stream.hpp"
#include "../types.hpp"
#include "../index.hpp"

namespace whatwg::encoding::japanese {

// 12.1.1. EUC-JP decoder
class euc_jp_decoder : public whatwg::encoding::decoder
{
public:
    euc_jp_decoder();

    result_t handler(byte_stream_t&, token_t byte) override;

    decoder_ptr create() const override;

private:
    code_point_index m_index_jis0208;
    code_point_index m_index_jis0212;
    token_t m_euc_jp_lead = 0x00u;
    bool m_euc_jp_jis0212_flag = false;
};

// 12.1.2. EUC-JP encoder
class euc_jp_encoder : public whatwg::encoding::encoder
{
public:
    euc_jp_encoder();

    result_t handler(stream_t&, token_t code_point) override;

    encoder_ptr create() const override;

private:
    pointer_index m_index;
};

} // namespace whatwg::encoding::japanese

#endif // WHATWG_ENCODING_ENCODINGS_EUC_JP_HPP
