//! @file
#ifndef WHATWG_ENCODING_TOKEN_HPP
#define WHATWG_ENCODING_TOKEN_HPP

#include "algorithm.hpp"
#include "types.hpp"

#include <array>
#include <iosfwd>

namespace whatwg::encoding {

//! @brief Denote 'finished' state of result_t
struct finished_t {};
constexpr finished_t finished = {};

//! @brief Denote 'continue' state of result_t
struct continue_t {};
constexpr continue_t continue_ = {};

//! @brief Denote 'error' state of result_t
struct error_t {};
constexpr error_t error = {};

//! @brief Result of encoder / decoder
//!
//! Result is ether one or more tokens, 'finished', 'continue' or
//! 'error'. 'error' might come with a code point which causes the error.
class result_t
{
public:
    using buffer_t = std::array<uint32_t, 4>;
    using const_iterator = buffer_t::const_iterator;

public:
    //! @name Constructor
    //! @{
    result_t(finished_t);
    result_t(continue_t);
    result_t(error_t);
    result_t(error_t, code_point_t);

    result_t(token_t);
    result_t(token_t, token_t);
    result_t(token_t, token_t, token_t);
    result_t(token_t, token_t, token_t, token_t);

    template<typename Char,
        typename = std::enable_if_t<std::is_same_v<Char, char>> >
    result_t(Char const c)
        : result_t { to_token(c) }
    {}

    template<typename Char,
        typename = std::enable_if_t<std::is_same_v<Char, char>> >
    result_t(Char const c1, Char const c2)
        : result_t {
            to_token(c1),
            to_token(c2),
          }
    {}

    template<typename Char,
        typename = std::enable_if_t<std::is_same_v<Char, char>> >
    result_t(Char const c1, Char const c2, Char const c3)
        : result_t {
            to_token(c1),
            to_token(c2),
            to_token(c3),
          }
    {}

    template<typename Char,
        typename = std::enable_if_t<std::is_same_v<Char, char>> >
    result_t(Char const c1, Char const c2, Char const c3, Char const c4)
        : result_t {
            to_token(c1),
            to_token(c2),
            to_token(c3),
            to_token(c4),
          }
    {}

    explicit result_t(byte_sequence_view_t);
    //! @}

    //! @name Query
    //! @{

    //! @brief Return true if result doesn't have any tokens, false otherwise.
    bool empty() const;

    //! @brief Return number of tokens result contains.
    size_t size() const;

    //! @brief Return i th token.
    //!
    //! Behavior is undefined if i >= size().
    token_t at(size_t const i) const;

    //! @brief Return true if result is 'finished'
    bool is_finished() const;

    //! @brief Return true if result is 'continue'
    bool is_continue() const;

    //! @brief Return true if result is 'error'
    bool is_error() const;

    //! @brief Return true if result is one or more tokens.
    bool is_tokens() const;
    //! @}

    //! @name Iterator
    //! @{

    //! @brief begin iterator to tokens
    const_iterator begin() const;

    //! @brief end iterator to tokens
    const_iterator end() const;
    //! @}

    //! @name Operator
    //! @{

    //! @brief Same as at()
    token_t operator[](size_t const i) const;

    bool operator==(result_t const& rhs) const;
    bool operator!=(result_t const& rhs) const;
    //! @}

private:
    void print_tokens(std::ostream&) const;

    friend std::ostream& operator<<(std::ostream&, result_t const&);

private:
    enum class type : uint16_t {
        finished,
        continue_,
        error,
        tokens,
    };

    buffer_t m_buf; // 16 bytes
    type m_type;                  // 2 bytes
    uint16_t m_length = 0;        // 2 bytes
};

std::ostream& operator<<(std::ostream&, finished_t const&);
std::ostream& operator<<(std::ostream&, continue_t const&);
std::ostream& operator<<(std::ostream&, error_t const&);

} // namespace whatwg::encoding

#endif // WHATWG_ENCODING_TOKEN_HPP
