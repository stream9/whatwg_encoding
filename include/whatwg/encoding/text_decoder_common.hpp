#ifndef WATWG_ENCODING_TEXT_DECODER_COMMON_HPP
#define WATWG_ENCODING_TEXT_DECODER_COMMON_HPP

#include "encoding.hpp"
#include "error.hpp"
#include "stream_fwd.hpp"
#include "types.hpp"

namespace whatwg::encoding {

//! @brief Base class for text decoding
//!
//! Based on §[7.1. Interface mixin TextDecoderCommon](https://encoding.spec.whatwg.org/#interface-mixin-textdecodercommon)
class text_decoder_common
{
public:
    using encoding_t = whatwg::encoding::encoding;
    using error_mode_t = whatwg::encoding::error_mode;

public:
    //! @name Constructor
    //! @{
    text_decoder_common();
    virtual ~text_decoder_common() {}
    //! @}

    //! @name Query
    //! @{

    //! @brief Return encoding's name in ASCII lowercase.
    byte_sequence_t encoding_name() const;

    //! @brief Return true when fatal option is specified, false otherwise.
    bool fatal() const;

    //! @brief Return true when ignore_bom option is specified, false otherwise.
    bool ignore_bom() const;
    //! @}

protected:
    encoding_t const& encoding() const;
    void encoding(encoding_t const&);

    void ignore_bom(bool);

    void bom_seen(bool);

    error_mode_t error_mode() const;
    void error_mode(error_mode_t);

    string_t serialize_stream(stream_t&);

private:
    encoding_t const* m_encoding; // non-null
    bool m_ignore_bom_flag = false;
    bool m_bom_seen_flag = false;
    error_mode_t m_error_mode = error_mode::replacement;
};

} // namespace whatwg::encoding

#endif // WATWG_ENCODING_TEXT_DECODER_COMMON_HPP
