#ifndef WHATWG_ENCODING_ALGORITHM_HPP
#define WHATWG_ENCODING_ALGORITHM_HPP

#include "types.hpp"

#include <cassert>

#include <boost/numeric/conversion/cast.hpp>

#include <whatwg/infra/code_point.hpp>

namespace whatwg::encoding {

template<typename T, typename Min, typename Max>
bool
between(T const v, Min const min, Max const max)
{
    assert(min < max); // LCOV_EXCL_LINE

    return min <= v && v <= max;
}

inline byte_t
to_byte(code_point_t const c)
{
    assert(c <= 0xFFu);
    return static_cast<byte_t>(c);
}

inline token_t
to_token(byte_t const c)
{
    return static_cast<unsigned char>(c);
}

inline token_t
to_token(token_t const c)
{
    return c;
}

inline bool is_token(token_t const t)
{
    return infra::is_valid_code_point(t) || t == end_of_stream;
}

template<typename T>
size_t
to_size(T const v)
{
    return boost::numeric_cast<size_t>(v);
}

} // namespace whatwg::encoding

#endif // WHATWG_ENCODING_ALGORITHM_HPP
