#ifndef WHATWG_ENCODING_STREAM_FWD_HPP
#define WHATWG_ENCODING_STREAM_FWD_HPP

#include "types.hpp"

namespace whatwg::encoding {

class result_t;

template<typename Traits> class basic_stream;

struct stream_traits;
struct byte_stream_traits;

using byte_stream_t = basic_stream<byte_stream_traits>;
using code_point_stream_t = basic_stream<stream_traits>;
using stream_t = code_point_stream_t;

template<typename Traits>
token_t read(basic_stream<Traits>&);

template<typename Traits, typename Buffer>
void read(basic_stream<Traits>&, Buffer&, size_t n);

template<typename Traits>
void push(basic_stream<Traits>&, token_t);

template<typename Traits>
void push(basic_stream<Traits>&, result_t const&);

template<typename Traits>
void push(basic_stream<Traits>&, typename Traits::string_view_type);

template<typename Traits>
void prepend(basic_stream<Traits>&, token_t);

template<typename Traits, typename ...Tokens>
void prepend(basic_stream<Traits>&, token_t, Tokens...);

template<typename Traits>
void prepend(basic_stream<Traits>&, typename Traits::string_view_t);

void prepend(stream_t&, string_view_t);

} // namespace whatwg::encoding

#endif // WHATWG_ENCODING_STREAM_FWD_HPP
