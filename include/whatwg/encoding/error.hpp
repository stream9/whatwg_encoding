#ifndef WHATWG_ENCODING_ERROR_HPP
#define WHATWG_ENCODING_ERROR_HPP

#include <system_error>

namespace whatwg::encoding {

enum class error_mode {
    replacement,
    fatal,
    html,
};

enum class errc {
    type_error,
    range_error,
    stream_read_error,
    stream_set_error,
    stream_prepend_error,
};

std::error_category const& encoding_category();

inline std::error_code
    make_error_code(errc const e) noexcept
{
    return std::error_code { static_cast<int>(e), encoding_category() };
}

} // namespace whatwg::encoding

namespace std {

template<>
struct is_error_code_enum<whatwg::encoding::errc> : true_type {};

} // namespace std

#endif // WHATWG_ENCODING_ERROR_HPP
