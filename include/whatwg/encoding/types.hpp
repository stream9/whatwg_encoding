#ifndef WHATWG_ENCODING_TYPES_HPP
#define WHATWG_ENCODING_TYPES_HPP

#include <optional>

#include <whatwg/infra/byte_sequence.hpp>
#include <whatwg/infra/code_point.hpp>
#include <whatwg/infra/string.hpp>

namespace whatwg::encoding {

using infra::byte_t;
using infra::code_point_t;

using token_t = uint32_t;

token_t constexpr end_of_stream = 0xFFFFFFFFu;

using std::optional;
constexpr auto null = std::nullopt;

using infra::string_view_t;

using infra::string_t;

using infra::byte_sequence_t;
using infra::byte_sequence_view_t;

using infra::operator""_s;
using infra::operator""_sv;

} // namespace whatwg::encoding

#endif // WHATWG_ENCODING_TYPES_HPP
