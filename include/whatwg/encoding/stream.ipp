#include "stream.hpp"

#include "algorithm.hpp"
#include "error.hpp"

#include <type_traits>

namespace whatwg::encoding {

/*
 * basic_stream
 */
template<typename Traits>
basic_stream<Traits>::basic_stream() = default;

template<typename Traits>
basic_stream<Traits>::
basic_stream(string_view_type const s)
    : m_buf { s }
{}

template<typename Traits>
basic_stream<Traits>& basic_stream<Traits>::
operator=(string_view_type const s)
{
    m_buf = s;
    m_index = 0;

    return *this;
}

template<typename Traits>
size_t basic_stream<Traits>::
size() const
{
    return m_buf.size() - m_index;
}

template<typename Traits>
bool basic_stream<Traits>::
empty() const
{
    return m_index == m_buf.size();
}

template<typename Traits>
token_t basic_stream<Traits>::
read()
{
    using char_type = typename Traits::char_type;

    if (m_index == m_buf.size()) {
        return end_of_stream;
    }
    else {
        auto const c = m_buf[m_index];
        ++m_index;
        return static_cast<std::make_unsigned_t<char_type>>(c);
    }
}

template<typename Traits>
void basic_stream<Traits>::
push(token_t const t)
{
    using char_type = typename Traits::char_type;

    if constexpr (std::is_same_v<char_type, char>) {
        m_buf.push_back(to_byte(t));
    }
    else {
        m_buf.push_back(t);
    }
}

template<typename Traits>
void basic_stream<Traits>::
prepend(token_t const t)
{
    if (t == end_of_stream) return;

    using char_type = typename Traits::char_type;

    char_type c;
    if constexpr (std::is_same_v<char_type, char>) {
        c = to_byte(t);
    }
    else {
        c = t;
    }

    if (m_index == 0) {
        m_buf.insert(0, 1, c);
    }
    else {
        --m_index;
        m_buf[m_index] = c;
    }
}

template<typename Traits>
typename basic_stream<Traits>::string_view_type basic_stream<Traits>::
to_string_view() const
{
    return string_view_type(m_buf).substr(m_index);
}

template<typename Traits>
typename basic_stream<Traits>::string_type basic_stream<Traits>::
to_string() const &
{
    return m_buf;
}

template<typename Traits>
typename basic_stream<Traits>::string_type basic_stream<Traits>::
to_string() &&
{
    auto result = std::move(m_buf);
    m_buf = string_type();
    m_index = 0;

    return result;
}

/*
 * free function
 */
template<typename Traits>
token_t
read(basic_stream<Traits>& s)
{
    return s.read();
}

template<typename Traits, typename Buffer>
void
read(basic_stream<Traits>& stream, Buffer& buf, size_t n)
{
    using char_type = typename Traits::char_type;

    while (true) {
        auto const t = read(stream);
        if (t == end_of_stream) break;

        append(buf, static_cast<char_type>(t));
        if (buf.size() == n) break;
    }
}

template<typename Traits>
void
push(basic_stream<Traits>& s, token_t const t)
{
    s.push(t);
}

template<typename Traits>
void
push(basic_stream<Traits>& s, result_t const& r)
{
    assert(r.is_tokens());

    for (auto const token: r) {
        s.push(token);
    }
}

template<typename Traits>
void
push(basic_stream<Traits>& s, typename Traits::string_view_type const sv)
{
    for (auto const c: sv) {
        s.push(to_token(c));
    }
}

template<typename Traits>
void
prepend(basic_stream<Traits>& s, token_t const t)
{
    s.prepend(t);
}

template<typename Traits, typename ...Tokens>
void
prepend(basic_stream<Traits>& stream, token_t const token, Tokens... tokens)
{
    prepend(stream, tokens...);
    stream.prepend(token);
}

template<typename Traits>
void
prepend(basic_stream<Traits>& is, typename Traits::string_view_type const s)
{
    auto it = s.rbegin();
    auto const end = s.rend();
    for (; it != end; ++it) {
        is.prepend(to_token(*it));
    }
}

inline void
prepend(byte_stream_t& is, byte_sequence_view_t const s)
{
    prepend<byte_stream_traits>(is, s);
}

inline void
prepend(stream_t& is, string_view_t const s)
{
    prepend<stream_traits>(is, s);
}

} // namespace whatwg::encoding
