#ifndef WHATWG_ENCODING_TEXT_ENCODER_COMMON_HPP
#define WHATWG_ENCODING_TEXT_ENCODER_COMMON_HPP

#include "types.hpp"

namespace whatwg::encoding {

//! @brief Base class for text encoding
//!
//! Based on §[7.3. Interface mixin TextEncoderCommon](https://encoding.spec.whatwg.org/#interface-mixin-textencodercommon)
class text_encoder_common
{
public:
    //! @name Constructor
    //! @{
    virtual ~text_encoder_common() {}
    //! @}

    //! @name Query
    //! @{

    //! @brief Return encoding's name in ASCII lowercase.
    byte_sequence_view_t encoding_name() const { return "utf-8"; }
    //! @}
};

} // namespace whatwg::encoding

#endif // WHATWG_ENCODING_TEXT_ENCODER_COMMON_HPP
