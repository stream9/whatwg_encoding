#ifndef WHATWG_ENCODING_EXCEPTION_HPP
#define WHATWG_ENCODING_EXCEPTION_HPP

#include <boost/preprocessor.hpp>

#if (__cpp_exceptions)
    #define THROW(x) throw x;
    #define TRY try
    #define CATCH(x) catch (x)
#else
    #define LOCATION __FILE__ ": " BOOST_PP_STRINGIZE(__LINE__)
    #define THROW(x) assert(false && LOCATION);
    #define TRY if (true)
    #define CATCH(x) if (false)
#endif

#endif // WHATWG_ENCODING_EXCEPTION_HPP
