#ifndef WHATWG_ENCODING_ENCODER_DECODER_IPP
#define WHATWG_ENCODING_ENCODER_DECODER_IPP

#include "encoder_decoder.hpp"

#include <algorithm>
#include <cassert>
#include <type_traits>

namespace whatwg::encoding {

template<typename T>
constexpr bool is_decoder_v = std::is_base_of_v<decoder, T>;

inline string_t
to_shortest_digit(result_t const& result)
{
    assert(result.is_error());
    assert(!result.empty());

    auto to_digit = [](auto c) { return U'0' + c; };

    auto const c = result[0];

    string_t digits;

    for (auto t = c; t != 0; t /= 10) {
        digits.push_back(to_digit(t % 10));
    }

    std::reverse(digits.begin(), digits.end());

    return digits;
}

template<typename Codec, typename IStream, typename OStream>
result_t
run(Codec const& encoder_decoder,
    IStream& input,
    OStream& output,
    optional<error_mode> mode)
{
    if (!mode) {
        if constexpr (is_decoder_v<Codec>) {
            mode = error_mode::replacement;
        }
        else {
            mode = error_mode::fatal;
        }
    }

    auto const encoder_decoder_instance = encoder_decoder.create();
    assert(encoder_decoder_instance);

    while (true) {
        auto const& result = process(
            *encoder_decoder_instance,
            input.read(),
            input,
            output,
            mode
        );

        if (!result.is_continue()) return result;
    }
}

template<typename Codec, typename Token,
         typename IStream, typename OStream>
result_t
process(Codec& encoder_decoder_instance,
        Token const token,
        IStream& input,
        OStream& output,
        optional<error_mode> mode)
{
    if (!mode) {
        if constexpr (is_decoder_v<Codec>) {
            mode = error_mode::replacement;
        }
        else {
            mode = error_mode::fatal;
        }
    }

    auto const& result =
        encoder_decoder_instance.handler(input, token);

    if (result.is_continue() || result.is_finished()) {
        return result;
    }
    else if (result.is_tokens()) {
        push(output, result);
    }
    else if (result.is_error()) {
        if constexpr (is_decoder_v<Codec>) {
            if (*mode == error_mode::replacement) {
                push(output, 0xFFFDu);
            }
            else {
                assert(*mode == error_mode::fatal);
                return error;
            }
        }
        else { // encoder
            if (*mode == error_mode::html) {
                prepend(input, 0x3Bu);
                prepend(input, to_shortest_digit(result));
                prepend(input, 0x23u);
                prepend(input, 0x26u);
            }
            else {
                assert(*mode == error_mode::fatal);
                return error;
            }
        }
    }

    return continue_;
}

} // namespace whatwg::encoding

#endif // WHATWG_ENCODING_ENCODER_DECODER_IPP
