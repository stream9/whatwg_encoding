#ifndef WHATWG_ENCODING_INDEX_DATA_HPP
#define WHATWG_ENCODING_INDEX_DATA_HPP

#include "types.hpp"

#include <cstdint>

#include <boost/range/iterator_range.hpp>

namespace whatwg::encoding {

using index_data_range = boost::iterator_range<uint32_t const (*)[2]>;

index_data_range index_data(byte_sequence_view_t filename);

} // namespace whatwg::encoding

#endif // WHATWG_ENCODING_INDEX_DATA_HPP
