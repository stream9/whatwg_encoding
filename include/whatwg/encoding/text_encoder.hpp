#ifndef WHATWG_ENCODING_TEXT_ENCODER_HPP
#define WHATWG_ENCODING_TEXT_ENCODER_HPP

#include "text_encoder_common.hpp"
#include "encodings/utf8.hpp"

namespace whatwg::encoding {

//! @brief High level API for text encoding.
//!
//! Based on §[7.4. Interface TextEncoder](https://encoding.spec.whatwg.org/#interface-textencoder)
class text_encoder : public text_encoder_common
{
public:
    //! @name Constructor
    //! @{
    text_encoder();

    //! @}
    //! @name Command
    //! @{

    //! @brief Encode string.
    //!
    //! @return encoded byte sequence
    //!
    byte_sequence_t encode(string_view_t input = U"");
    //! @}

private:
    utf8::encoder m_encoder;
};

} // namespace whatwg::encoding

#endif // WHATWG_ENCODING_TEXT_ENCODER_HPP
