#ifndef WHATWG_ENCODING_STREAM_HPP
#define WHATWG_ENCODING_STREAM_HPP

#include "result.hpp"
#include "stream_fwd.hpp"
#include "types.hpp"

namespace whatwg::encoding {

struct stream_traits
{
    using string_type = string_t;
    using string_view_type = string_view_t;
    using char_type = string_type::value_type;
};

struct byte_stream_traits
{
    using string_type = byte_sequence_t;
    using string_view_type = byte_sequence_view_t;
    using char_type = string_type::value_type;
};

/*
 * basic_stream
 */
template<typename Traits>
class basic_stream
{
    using string_type = typename Traits::string_type;
    using string_view_type = typename Traits::string_view_type;
public:
    basic_stream();
    basic_stream(string_view_type);

    basic_stream& operator=(string_view_type);

    // query
    size_t size() const;
    bool empty() const;
    token_t read();

    // modifier
    void push(token_t);
    void prepend(token_t);

    // converter
    string_view_type to_string_view() const;
    string_type to_string() const &;
    string_type to_string() &&;

private:
    string_type m_buf;
    size_t m_index = 0;
};

} // namespace whatwg::encoding

#include "stream.ipp"

#endif // WHATWG_ENCODING_STREAM_HPP
