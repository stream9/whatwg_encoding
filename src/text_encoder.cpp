#include "text_encoder.hpp"

#include "stream.hpp"
#include "encoder_decoder.hpp"
#include "encoder_decoder.ipp"

namespace whatwg::encoding {

text_encoder::text_encoder() = default;

byte_sequence_t text_encoder::
encode(string_view_t const input_/*= ""*/)
{
    stream_t input { input_ };
    byte_stream_t output;

    while (true) {
        auto const token = read(input);

        auto const result = process(m_encoder, token, input, output);

        if (result.is_finished()) {
            return output.to_string();
        }
    }
}

} // namespace whatwg::encoding
