#include "text_decoder_common.hpp"

#include "stream.hpp"

#include <cassert>

#include <boost/algorithm/string.hpp>

namespace whatwg::encoding {

static byte_sequence_t
to_lower(byte_sequence_view_t const s)
{
    byte_sequence_t result { s };

    boost::algorithm::to_lower(result);

    return result;
}

template<typename T1, typename T2>
bool
equals_to_any_of(T1 const& lhs, T2 const& rhs)
{
    return lhs == rhs;
}

template<typename T1, typename T2, typename... Rest>
bool
equals_to_any_of(T1 const& lhs, T2 const& rhs, Rest&&... rest)
{
    return lhs == rhs || equals_to_any_of(lhs, rest...);
}

/*
 * text_decoder_common
 */
using encoding_t = text_decoder_common::encoding_t;
using error_mode_t = text_decoder_common::error_mode_t;

text_decoder_common::text_decoder_common() = default;

byte_sequence_t text_decoder_common::
encoding_name() const
{
    return to_lower(m_encoding->name());
}

bool text_decoder_common::
fatal() const
{
    return m_error_mode == error_mode::fatal;
}

bool text_decoder_common::
ignore_bom() const
{
    return m_ignore_bom_flag;
}

encoding_t const& text_decoder_common::
encoding() const
{
    assert(m_encoding);
    return *m_encoding;
}

void text_decoder_common::
encoding(encoding_t const& e)
{
    m_encoding = &e;
}

void text_decoder_common::
ignore_bom(bool const flag)
{
    m_ignore_bom_flag = flag;
}

void text_decoder_common::
bom_seen(bool const flag)
{
    m_bom_seen_flag = flag;
}

error_mode_t text_decoder_common::
error_mode() const
{
    return m_error_mode;
}

void text_decoder_common::
error_mode(error_mode_t const mode)
{
    m_error_mode = mode;
}

string_t text_decoder_common::
serialize_stream(stream_t& stream)
{
    assert(m_encoding);
    string_t output;

    while (true) {
        auto const token = read(stream);

        if (equals_to_any_of(m_encoding->name(),
                             "UTF-8", "UTF-16BE", "UTF-16LE") &&
            !m_bom_seen_flag)
        {
            if (token == 0xFEFFu) {
                m_bom_seen_flag = true;
            }
            else if (token != end_of_stream) {
                m_bom_seen_flag = true;
                append(output, token);
            }
            else {
                return output;
            }
        }
        else if (token != end_of_stream) {
            append(output, token);
        }
        else {
            return output;
        }
    }
}

} // namespace whatwg::encoding
