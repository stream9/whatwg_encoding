#include "encodings/utf16.hpp"

#include <memory>

namespace whatwg::encoding::utf16 {

/*
 * shared_decoder
 */
shared_decoder::
shared_decoder(bool const utf16be_decoder_flag)
    : m_utf16be_decoder_flag { utf16be_decoder_flag }
{}

result_t shared_decoder::
handler(byte_stream_t& stream, token_t const byte)
{
    if (byte == end_of_stream) {
        if (m_utf16_lead_byte || m_utf16_lead_surrogate) {
            m_utf16_lead_byte = null;
            m_utf16_lead_surrogate = null;
            return error;
        }
        else {
            assert(!m_utf16_lead_byte && !m_utf16_lead_surrogate);
            return finished;
        }
    }

    if (!m_utf16_lead_byte) {
        m_utf16_lead_byte = byte;
        return continue_;
    }

    assert(m_utf16_lead_byte);

    auto const code_unit = m_utf16be_decoder_flag
        ? (*m_utf16_lead_byte << 8) + byte
        : (byte << 8) + *m_utf16_lead_byte;

    m_utf16_lead_byte = null;

    if (m_utf16_lead_surrogate) {
        auto const lead_surrogate = *m_utf16_lead_surrogate;
        m_utf16_lead_surrogate = null;

        if (between(code_unit, 0xDC00u, 0xDFFFu)) {
            auto const code_point = 0x10000u +
                ((lead_surrogate - 0xD800u) << 10) + (code_unit - 0xDC00u);

            return code_point;
        }

        // illegal second surrogate
        auto const byte1 = code_unit >> 8;
        auto const byte2 = code_unit & 0x00FFu;

        if (m_utf16be_decoder_flag) {
            prepend(stream, byte1, byte2);
        }
        else {
            prepend(stream, byte2, byte1);
        }

        return error;
    }

    if (between(code_unit, 0xD800u, 0xDBFFu)) {
        m_utf16_lead_surrogate = code_unit;
        return continue_;
    }

    if (between(code_unit, 0xDC00u, 0xDFFFu)) return error;

    return code_point_t { code_unit };
}

decoder_ptr shared_decoder::
create() const
{
    return std::make_unique<shared_decoder>(m_utf16be_decoder_flag);
}

/*
 * utf16be_decoder
 */
utf16be_decoder::utf16be_decoder() : shared_decoder { true } {}

/*
 * utf16le_decoder
 */
utf16le_decoder::utf16le_decoder() : shared_decoder { false } {}

} // namespace whatwg::encoding::utf16
