#include "encodings/chinese_simplified.hpp"

#include "index.hpp"

#include <whatwg/infra/byte.hpp>
#include <whatwg/infra/code_point.hpp>

namespace whatwg::encoding::chinese {

/*
 * gb18030_decoder
 */
gb18030_decoder::
gb18030_decoder()
    : m_index { "gb18030" }
{}

result_t gb18030_decoder::
handler(byte_stream_t& stream, token_t const byte)
{
    if (byte == end_of_stream) {
        if (m_gb18030_third == 0x00u &&
            m_gb18030_second == 0x00u && m_gb18030_first == 0x00u)
        {
            return finished;
        }
        else {
            return error;
        }
    }

    if (m_gb18030_third != 0x00u) {
        if (!between(byte, 0x30u, 0x39u)) {
            prepend(stream, m_gb18030_second, m_gb18030_third, byte);

            m_gb18030_first = m_gb18030_second = m_gb18030_third = 0x00u;

            return error;
        }

        auto const code_point =
            index_gb18030_ranges_code_point(
                  ((m_gb18030_first - 0x81u) * (10u * 126u * 10u))
                + ((m_gb18030_second - 0x30u) * (10u * 126u))
                + ((m_gb18030_third - 0x81u) * 10u) + byte - 0x30u );

        if (code_point == null) return error;

        return *code_point;
    }

    if (m_gb18030_second != 0x00u) {
        if (between(byte, 0x81u, 0xFEu)) {
            m_gb18030_third = byte;
            return continue_;
        }

        prepend(stream, m_gb18030_second, byte);

        m_gb18030_first = m_gb18030_second = 0x00u;

        return error;
    }

    if (m_gb18030_first != 0x00u) {
        if (between(byte, 0x30u, 0x39u)) {
            m_gb18030_second = byte;
            return continue_;
        }

        auto const lead = m_gb18030_first;
        optional<token_t> pointer = null;
        m_gb18030_first = 0x00u;

        auto const offset = byte < 0x7Fu ? 0x40u : 0x41u;

        if (between(byte, 0x40u, 0x7Eu) || between(byte, 0x80u, 0xFEu)) {
            pointer = (lead - 0x81u) * 190u + (byte - offset);
        }

        auto const code_point =
            pointer == null ? null : index_code_point(m_index, *pointer);

        if (code_point != null) return *code_point;

        if (infra::is_ascii_byte(to_byte(byte))) {
            prepend(stream, byte);
        }

        return error;
    }

    if (infra::is_ascii_byte(to_byte(byte))) return code_point_t { byte };

    if (byte == 0x80u) return U'\x20AC';

    if (between(byte, 0x81u, 0xFEu)) {
        m_gb18030_first = byte;
        return continue_;
    }

    return error;
}

decoder_ptr gb18030_decoder::
create() const
{
    return std::make_unique<gb18030_decoder>();
}

/*
 * gb18030_encoder
 */
gb18030_encoder::
gb18030_encoder(bool const gbk_flag/* = false*/)
    : m_index { "gb18030" }
    , m_gbk_flag { gbk_flag }
{}

result_t gb18030_encoder::
handler(stream_t&, token_t const code_point)
{
    if (code_point == end_of_stream) return finished;

    if (infra::is_ascii_code_point(code_point)) {
        return to_byte(code_point);
    }

    if (code_point == U'\xE5E5') {
        return { error, code_point };
    }

    if (m_gbk_flag && code_point == U'\x20AC') {
        return '\x80';
    }

    auto pointer = index_pointer(m_index, code_point);

    if (pointer != null) {
        auto const lead = *pointer / 190u + 0x81u;
        auto const trail = *pointer % 190u;

        auto const offset = trail < 0x3Fu ? 0x40u : 0x41u;

        return { to_byte(lead), to_byte(trail + offset) };
    }

    if (m_gbk_flag) return { error, code_point };

    pointer = index_gb18030_ranges_pointer(code_point);
    assert(pointer);

    auto const byte1 = *pointer / (10u * 126u * 10u);
    pointer = *pointer % (10u * 126u * 10u);

    auto const byte2 = *pointer / (10u * 126u);
    pointer = *pointer % (10u * 126u);

    auto const byte3 = *pointer / 10u;

    auto const byte4 = *pointer % 10u;

    return {
        to_byte(byte1 + 0x81u),
        to_byte(byte2 + 0x30u),
        to_byte(byte3 + 0x81u),
        to_byte(byte4 + 0x30u)
    };
}

encoder_ptr gb18030_encoder::
create() const
{
    return std::make_unique<gb18030_encoder>();
}

/*
 * GBK encoder
 */
gbk_encoder::
gbk_encoder()
    : gb18030_encoder { true }
{}

} // namespace whatwg::encoding::chinese
