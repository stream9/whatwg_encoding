#include "encodings/replacement.hpp"

#include <memory>

namespace whatwg::encoding::replacement {

result_t decoder::
handler(byte_stream_t&, token_t const byte)
{
    if (byte == end_of_stream) return finished;

    if (!m_replacement_error_returned_flag) {
        m_replacement_error_returned_flag = true;
        return error;
    }

    return finished;
}

decoder_ptr decoder::
create() const
{
    return std::make_unique<replacement::decoder>();
}

} // namespace whatwg::encoding::replacement
