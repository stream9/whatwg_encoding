#include "encodings/utf8.hpp"

#include "algorithm.hpp"
#include "stream.hpp"

#include <cassert>

#include <whatwg/infra/code_point.hpp>

namespace whatwg::encoding::utf8 {

/*
 * decoder
 */
result_t decoder::
handler(byte_stream_t& stream, token_t const byte)
{
    assert(byte <= 0xFFu || byte == end_of_stream); // LCOV_EXCL_LINE

    if (byte == end_of_stream && m_utf8_bytes_needed != 0) {
        m_utf8_bytes_needed = 0;
        return error;
    }

    if (byte == end_of_stream) return finished;

    if (m_utf8_bytes_needed == 0) {
        if (between(byte, 0x00u, 0x7Fu)) {
            return static_cast<code_point_t>(byte);
        }
        else if (between(byte, 0xC2u, 0xDFu)) {
            m_utf8_bytes_needed = 1;
            m_utf8_code_point = byte & 0x1Fu;
        }
        else if (between(byte, 0xE0u, 0xEFu)) {
            if (byte == 0xE0u) {
                m_utf8_lower_boundary = 0xA0u;
            }
            if (byte == 0xEDu) {
                m_utf8_upper_boundary = 0x9Fu;
            }

            m_utf8_bytes_needed = 2;
            m_utf8_code_point = byte & 0xFu;
        }
        else if (between(byte, 0xF0u, 0xF4u)) {
            if (byte == 0xF0u) {
                m_utf8_lower_boundary = 0x90u;
            }
            if (byte == 0xF4u) {
                m_utf8_upper_boundary = 0x8Fu;
            }

            m_utf8_bytes_needed = 3;
            m_utf8_code_point = byte & 0x7u;
        }
        else {
            return error;
        }

        return continue_;
    } // if (m_utf8_bytes_needed == 0)

    if (!between(byte, m_utf8_lower_boundary, m_utf8_upper_boundary)) {
        m_utf8_code_point = 0;
        m_utf8_bytes_needed = 0;
        m_utf8_bytes_seen = 0;
        m_utf8_lower_boundary = 0x80u;
        m_utf8_upper_boundary = 0xBFu;

        prepend(stream, byte);

        return error;
    }

    m_utf8_lower_boundary = 0x80u;
    m_utf8_upper_boundary = 0xBFu;

    m_utf8_code_point = (m_utf8_code_point << 6) | (byte & 0x3Fu);

    ++m_utf8_bytes_seen;

    if (m_utf8_bytes_seen != m_utf8_bytes_needed) return continue_;

    auto const code_point = m_utf8_code_point;
    m_utf8_code_point = 0;
    m_utf8_bytes_needed = 0;
    m_utf8_bytes_seen = 0;

    return code_point;
}

decoder_ptr decoder::
create() const
{
    return std::make_unique<utf8::decoder>();
}

/*
 * encoder
 */
// different from spec, return error if token is invalid
result_t encoder::
handler(stream_t&, token_t const code_point)
{
    //assert(is_token(code_point));

    if (code_point == end_of_stream) return finished;

    if (infra::is_ascii_code_point(code_point)) {
        return to_byte(code_point);
    }

    int count = 0;
    auto offset = 0u;
    if (code_point <= 0x7FFu) {
        count = 1;
        offset = 0xC0;
    }
    else if (between(code_point, 0x800u, 0xFFFFu)) {
        count = 2;
        offset = 0xE0;
    }
    else if (between(code_point, 0x10000u, 0x10FFFFu)) {
        count = 3;
        offset = 0xF0;
    }
    else {
        return { error, code_point }; // invalid token
    }

    byte_sequence_t bytes;
    append(bytes, to_byte((code_point >> (6 * count)) + offset));

    while (count > 0) {
        auto const temp = code_point >> (6 * (count - 1));
        append(bytes, to_byte(0x80 | (temp & 0x3F)));
        --count;
    }

    return result_t { bytes };
}

encoder_ptr encoder::
create() const
{
    return std::make_unique<utf8::encoder>();
}

} // namespace whatwg::encoding::utf8
