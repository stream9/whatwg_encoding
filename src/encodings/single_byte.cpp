#include "encodings/single_byte.hpp"

#include <whatwg/infra/byte.hpp>
#include <whatwg/infra/code_point.hpp>

namespace whatwg::encoding::single_byte {

/*
 * decoder
 */
decoder::
decoder(byte_sequence_view_t const encoding_name)
    : m_index { encoding_name }
{}

result_t decoder::
handler(byte_stream_t&, token_t const byte)
{
    if (byte == end_of_stream) return finished;

    if (infra::is_ascii_byte(to_byte(byte))) return code_point_t { byte };

    auto const code_point = index_code_point(m_index, byte - 0x80u);

    if (code_point == null) return error;

    return *code_point;
}

decoder_ptr decoder::
create() const
{
    return std::make_unique<single_byte::decoder>(*this);
}

/*
 * encoder
 */
encoder::
encoder(byte_sequence_view_t const encoding_name)
    : m_index { encoding_name }
{}

result_t encoder::
handler(stream_t&, token_t const code_point)
{
    if (code_point == end_of_stream) return finished;

    if (infra::is_ascii_code_point(code_point)) {
        return to_byte(code_point);
    }

    auto const pointer = index_pointer(m_index, code_point);

    if (pointer == null) return { error, code_point };

    return to_byte(*pointer + 0x80u);
}

encoder_ptr encoder::
create() const
{
    return std::make_unique<single_byte::encoder>(*this);
}

} // namespace whatwg::encoding::single_byte
