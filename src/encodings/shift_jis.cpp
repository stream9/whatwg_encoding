#include "encodings/shift_jis.hpp"

#include <memory>

#include <whatwg/infra/byte.hpp>
#include <whatwg/infra/code_point.hpp>

namespace whatwg::encoding::japanese {

/*
 * shift_jis_decoder
 */
shift_jis_decoder::
shift_jis_decoder()
    : m_index_jis0208 { "jis0208" }
{}

result_t shift_jis_decoder::
handler(byte_stream_t& stream, token_t const byte)
{
    if (byte == end_of_stream) {
        if (m_shift_jis_lead != 0x00u) {
            m_shift_jis_lead = 0x00u;
            return error;
        }
        else {
            return finished;
        }
    }

    if (m_shift_jis_lead != 0x00u) {
        auto const lead = m_shift_jis_lead;
        optional<pointer_t> pointer = null;
        m_shift_jis_lead = 0x00u;

        auto const offset = byte < 0x7Fu ? 0x40u : 0x41u;
        auto const lead_offset = lead < 0xA0u ? 0x81u : 0xC1u;

        if (between(byte, 0x40u, 0x7Eu) || between(byte, 0x80u, 0xC1u)) {
            pointer = (lead - lead_offset) * 188u + byte - offset;

            if (between(*pointer, 8836u, 10715u)) {
                return code_point_t { 0xE000u - 8836u + *pointer };
            }
        }

        optional<code_point_t> const code_point =
            pointer == null ? null : index_code_point(m_index_jis0208, *pointer);

        if (code_point != null) {
            return *code_point;
        }

        if (infra::is_ascii_byte(to_byte(byte))) {
            prepend(stream, byte);
        }

        return error;
    }

    if (infra::is_ascii_byte(to_byte(byte)) || byte == 0x80u) {
        return code_point_t { byte };
    }

    if (between(byte, 0xA1u, 0xDFu)) {
        return code_point_t { 0xFF61u - 0xA1u + byte };
    }

    if (between(byte, 0x81u, 0x9Fu) || between(byte, 0xE0u, 0xFCu)) {
        m_shift_jis_lead = byte;
        return continue_;
    }

    return error;
}

decoder_ptr shift_jis_decoder::
create() const
{
    return std::make_unique<shift_jis_decoder>();
}

/*
 * shift_jis_encoder
 */
shift_jis_encoder::shift_jis_encoder() = default;

result_t shift_jis_encoder::
handler(stream_t&, token_t code_point)
{
    if (code_point == end_of_stream) return finished;

    if (infra::is_ascii_code_point(code_point) || code_point == 0x80u) {
        return to_byte(code_point);
    }

    if (code_point == 0xA5u) return '\x5C';
    if (code_point == 0x203Eu) return '\x7E';

    if (between(code_point, 0xFF61u, 0xFF9Fu)) {
        return to_byte(code_point - 0xFF61u + 0xA1u);
    }

    if (code_point == 0x2212u) code_point = 0xFF0Du;

    auto const pointer = index_shift_jis_pointer(code_point);

    if (pointer == null) return { error, code_point };

    auto const lead = *pointer / 188u;
    auto const lead_offset = lead < 0x1Fu ? 0x81u : 0xC1u;

    auto const trail = *pointer % 188u;
    auto const offset = trail < 0x3Fu ? 0x40u : 0x41u;

    return {
        to_byte(lead + lead_offset),
        to_byte(trail + offset)
    };
}

encoder_ptr shift_jis_encoder::
create() const
{
    return std::make_unique<shift_jis_encoder>();
}

} // namespace whatwg::encoding::japanese
