#include "encodings/user_defined.hpp"

#include "algorithm.hpp"
#include "stream.hpp"

#include <memory>

#include <whatwg/infra/byte.hpp>
#include <whatwg/infra/code_point.hpp>

namespace whatwg::encoding::user_defined {

/*
 * decoder
 */
result_t decoder::
handler(byte_stream_t&, token_t const byte)
{
    if (byte == end_of_stream) return finished;

    if (infra::is_ascii_byte(to_byte(byte))) return code_point_t { byte };

    return code_point_t { 0xF780u + byte - 0x80u };
}

decoder_ptr decoder::
create() const
{
    return std::make_unique<user_defined::decoder>();
}

/*
 * encoder
 */
result_t encoder::
handler(stream_t&, token_t const code_point)
{
    if (code_point == end_of_stream) return finished;

    if (infra::is_ascii_code_point(code_point)) return to_byte(code_point);

    if (between(code_point, 0xF780u, 0xF7FFu)) {
        return to_byte(code_point - 0xF780u + 0x80u);
    }

    return { error, code_point };
}

encoder_ptr encoder::
create() const
{
    return std::make_unique<user_defined::encoder>();
}

} // namespace whatwg::encoding::user_defined
