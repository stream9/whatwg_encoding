#include "encodings/euc_jp.hpp"

#include <memory>

#include <whatwg/infra/byte.hpp>
#include <whatwg/infra/code_point.hpp>

namespace whatwg::encoding::japanese {

static bool
both_are_in_the_range(token_t const t1, token_t const t2,
                      token_t const from, token_t const to)
{
    return between(t1, from, to) && between(t2, from, to);
}

/*
 * euc_jp_decoder
 */
euc_jp_decoder::
euc_jp_decoder()
    : m_index_jis0208 { "jis0208" }
    , m_index_jis0212 { "jis0212" }
{}

result_t euc_jp_decoder::
handler(byte_stream_t& stream, token_t const byte)
{
    if (byte == end_of_stream) {
        if (m_euc_jp_lead != 0x00u) {
            m_euc_jp_lead = 0x00u;
            return error;
        }
        else {
            return finished;
        }
    }

    if (m_euc_jp_lead == 0x8Eu && between(byte, 0xA1u, 0xDFu)) {
        m_euc_jp_lead = 0x00;
        return code_point_t { 0xFF61u - 0xA1u + byte };
    }

    if (m_euc_jp_lead == 0x8Fu && between(byte, 0xA1u, 0xFEu)) {
        m_euc_jp_jis0212_flag = true;
        m_euc_jp_lead = byte;
        return continue_;
    }

    if (m_euc_jp_lead != 0x00u) {
        auto const lead = m_euc_jp_lead;
        m_euc_jp_lead = 0x00u;

        optional<code_point_t> code_point = null;

        if (both_are_in_the_range(lead, byte, 0xA1u, 0xFEu)) {
            code_point = index_code_point(
                !m_euc_jp_jis0212_flag ? m_index_jis0208 : m_index_jis0212,
                (lead - 0xA1u) * 94u + byte - 0xA1u
            );
        }

        m_euc_jp_jis0212_flag = false;

        if (code_point != null) return *code_point;

        if (infra::is_ascii_byte(to_byte(byte))) {
            prepend(stream, byte);
        }

        return error;
    }

    if (infra::is_ascii_byte(to_byte(byte))) return code_point_t { byte };

    if (byte == 0x8Eu || byte == 0x8Fu || between(byte, 0xA1u, 0xFEu)) {
        m_euc_jp_lead = byte;
        return continue_;
    }

    return error;
}

decoder_ptr euc_jp_decoder::
create() const
{
    return std::make_unique<euc_jp_decoder>();
}

/*
 * euc_jp_encoder
 */
euc_jp_encoder::
euc_jp_encoder()
    : m_index { "jis0208" }
{}

result_t euc_jp_encoder::
handler(stream_t&, token_t code_point)
{
    if (code_point == end_of_stream) return finished;

    if (infra::is_ascii_code_point(code_point)) return to_byte(code_point);

    if (code_point == 0xA5u) return '\x5C';

    if (code_point == 0x203u) return '\x7E';

    if (between(code_point, 0xFF61u, 0xFF9Fu)) {
        return { '\x8E', to_byte(code_point - 0xFF61u + 0xA1u) };
    }

    if (code_point == 0x2212u) code_point = 0xFF0Du;

    auto const pointer = index_pointer(m_index, code_point);

    if (pointer == null) return { error, code_point };

    auto const lead = *pointer / 94u + 0xA1u;
    auto const trail = *pointer % 94u + 0xA1u;

    return { to_byte(lead), to_byte(trail) };
}

encoder_ptr euc_jp_encoder::
create() const
{
    return std::make_unique<euc_jp_encoder>();
}

} // namespace whatwg::encoding::japanese
