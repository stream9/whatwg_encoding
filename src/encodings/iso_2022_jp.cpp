#include "encodings/iso_2022_jp.hpp"

#include <whatwg/infra/code_point.hpp>

namespace whatwg::encoding::japanese {

iso_2022_jp_decoder::
iso_2022_jp_decoder()
    : m_index_jis0208 { "jis0208" }
{}

result_t iso_2022_jp_decoder::
handler(byte_stream_t& stream, token_t const byte)
{
    switch (m_iso_2022_jp_decoder_state) {
        case state::ascii:
            if (byte == 0x1Bu) {
                m_iso_2022_jp_decoder_state = state::escape_start;
                return continue_;
            }
            else if (byte != 0x0Eu && byte != 0x0Fu &&
                     between(byte, 0x00u, 0x7Fu)) // byte != 0x1Bu is implied
            {
                m_iso_2022_jp_output_flag = false;
                return code_point_t { byte };
            }
            else if (byte == end_of_stream) {
                return finished;
            }
            else {
                m_iso_2022_jp_output_flag = false;
                return error;
            }
            break;
        case state::roman:
            if (byte == 0x1Bu) {
                m_iso_2022_jp_decoder_state = state::escape_start;
                return continue_;
            }
            else if (byte == 0x5Cu) {
                m_iso_2022_jp_output_flag = false;
                return U'\xA5';
            }
            else if (byte == 0x7Eu) {
                m_iso_2022_jp_output_flag = false;
                return U'\x203E';
            }
            else if (byte != 0x0Eu && byte != 0x0Fu &&
                  // byte != 0x1Bu && byte != 0x5Cu && byte != 0x7Eu &&
                     between(byte, 0x00u, 0x7Fu))
            {
                m_iso_2022_jp_output_flag = false;
                return code_point_t { byte };
            }
            else if (byte == end_of_stream) {
                return finished;
            }
            else {
                m_iso_2022_jp_output_flag = false;
                return error;
            }
            break;
        case state::katakana:
            if (byte == 0x1Bu) {
                m_iso_2022_jp_decoder_state = state::escape_start;
                return continue_;
            }
            else if (between(byte, 0x21u, 0x5Fu)) {
                m_iso_2022_jp_output_flag = false;
                return code_point_t { 0xFF61u - 0x21u + byte };
            }
            else if (byte == end_of_stream) {
                return finished;
            }
            else {
                m_iso_2022_jp_output_flag = false;
                return error;
            }
            break;
        case state::lead_byte:
            if (byte == 0x1Bu) {
                m_iso_2022_jp_decoder_state = state::escape_start;
                return continue_;
            }
            else if (between(byte, 0x21u, 0x7Eu)) {
                m_iso_2022_jp_output_flag = false;
                m_iso_2022_jp_lead = byte;
                m_iso_2022_jp_decoder_state = state::trail_byte;
                return continue_;
            }
            else if (byte == end_of_stream) {
                return finished;
            }
            else {
                m_iso_2022_jp_output_flag = false;
                return error;
            }
            break;
        case state::trail_byte:
            if (byte == 0x1Bu) {
                m_iso_2022_jp_decoder_state = state::escape_start;
                return error;
            }
            else if (between(byte, 0x21u, 0x7Eu)) {
                m_iso_2022_jp_decoder_state = state::lead_byte;
                auto const pointer =
                    (m_iso_2022_jp_lead - 0x21u) * 94u + byte - 0x21u;

                auto const code_point =
                            index_code_point(m_index_jis0208, pointer);

                if (code_point == null) return error;

                return code_point_t { *code_point };
            }
            else if (byte == end_of_stream) {
                m_iso_2022_jp_decoder_state = state::lead_byte;
                prepend(stream, byte);

                return error;
            }
            else {
                m_iso_2022_jp_decoder_state = state::lead_byte;
                return error;
            }
            break;
        case state::escape_start:
            if (byte == 0x24u || byte == 0x28u) {
                m_iso_2022_jp_lead = byte;
                m_iso_2022_jp_decoder_state = state::escape;

                return continue_;
            }

            prepend(stream, byte);

            m_iso_2022_jp_output_flag = false;
            m_iso_2022_jp_decoder_state = m_iso_2022_jp_decoder_output_state;

            return error;
        default:
        case state::escape: {
            auto const lead = m_iso_2022_jp_lead;
            m_iso_2022_jp_lead = 0x00u;

            optional<state> state_ = null;

            if (lead == 0x28u && byte == 0x42u) state_ = state::ascii;

            if (lead == 0x28u && byte == 0x4Au) state_ = state::roman;

            if (lead == 0x28u && byte == 0x49u) state_ = state::katakana;

            if (lead == 0x24u && (byte == 0x40u || byte == 0x42u)) {
                state_ = state::lead_byte;
            }

            if (state_ != null) {
                m_iso_2022_jp_decoder_state =
                    m_iso_2022_jp_decoder_output_state = *state_;

                auto const output_flag = m_iso_2022_jp_output_flag;

                m_iso_2022_jp_output_flag = true;

                if (!output_flag) {
                    return continue_;
                }
                else {
                    return error;
                }
            }

            prepend(stream, lead);

            m_iso_2022_jp_output_flag = false;
            m_iso_2022_jp_decoder_state = m_iso_2022_jp_decoder_output_state;

            return error;
        }
    }
}

decoder_ptr iso_2022_jp_decoder::
create() const
{
    return std::make_unique<iso_2022_jp_decoder>();
}

iso_2022_jp_encoder::
iso_2022_jp_encoder()
    : m_index_jis0208 { "jis0208" }
    , m_index_iso_2022_jp_katakana { "ISO-2022-JP katakana" }
{}

result_t iso_2022_jp_encoder::
handler(stream_t& stream, token_t code_point)
{
    if (code_point == end_of_stream) {
        if (m_iso_2022_jp_encoder_state != state::ascii) {
            prepend(stream, code_point);
            m_iso_2022_jp_encoder_state = state::ascii;

            return { '\x1B', '\x28', '\x42' };
        }
        else {
            return finished;
        }
    }

    if ((m_iso_2022_jp_encoder_state == state::ascii ||
         m_iso_2022_jp_encoder_state == state::roman) &&
        (code_point == 0x0Eu || code_point == 0x0Fu || code_point == 0x1Bu))
    {
        return { error, 0xFFFDu };
    }

    if (m_iso_2022_jp_encoder_state == state::ascii &&
        infra::is_ascii_code_point(code_point))
    {
        return to_byte(code_point);
    }

    if (m_iso_2022_jp_encoder_state == state::roman) {
        if (infra::is_ascii_code_point(code_point) &&
            code_point != 0x5Cu && code_point != 0x7Eu)
        {
            return to_byte(code_point);
        }

        if (code_point == 0xA5u) return '\x5C';

        if (code_point == 0x203Eu) return '\x7E';
    }


    if (infra::is_ascii_code_point(code_point)) {
        assert(m_iso_2022_jp_encoder_state != state::ascii); // implied by logic

        prepend(stream, code_point);
        m_iso_2022_jp_encoder_state = state::ascii;

        return { '\x1B', '\x28', '\x42' };
    }

    if (code_point == 0xA5u || code_point == 0x203Eu) {
        assert(m_iso_2022_jp_encoder_state != state::roman); // implied by logic

        prepend(stream, code_point);
        m_iso_2022_jp_encoder_state = state::roman;

        return { '\x1B', '\x28', '\x4A' };
    }

    if (code_point == 0x2212u) code_point = 0xFF0Du;

    if (between(code_point, 0xFF61u, 0xFF9Fu)) {
        auto const code_point_ = index_code_point(
            m_index_iso_2022_jp_katakana, code_point - 0xFF61u);

        assert(code_point_);

        code_point = *code_point_;
    }

    auto const pointer = index_pointer(m_index_jis0208, code_point);

    if (pointer == null) return { error, code_point };

    if (m_iso_2022_jp_encoder_state != state::jis0208) {
        prepend(stream, code_point);
        m_iso_2022_jp_encoder_state = state::jis0208;

        return { '\x1B', '\x24', '\x42' };
    }

    auto const lead = *pointer / 94u + 0x21u;
    auto const trail = *pointer % 94u + 0x21u;

    return { to_byte(lead), to_byte(trail) };
}

encoder_ptr iso_2022_jp_encoder::
create() const
{
    return std::make_unique<iso_2022_jp_encoder>();
}

} // namespace whatwg::encoding::japanese
