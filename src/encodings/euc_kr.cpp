#include "encodings/euc_kr.hpp"

#include <whatwg/infra/byte.hpp>
#include <whatwg/infra/code_point.hpp>

namespace whatwg::encoding::korean {

/*
 * euc_kr_decoder
 */
euc_kr_decoder::
euc_kr_decoder()
    : m_index_euc_kr { "EUC-KR" }
{}

result_t euc_kr_decoder::
handler(byte_stream_t& stream, token_t const byte)
{
    if (byte == end_of_stream) {
        if (m_euc_kr_lead != 0x00u) {
            m_euc_kr_lead = 0x00u;
            return error;
        }
        else {
            return finished;
        }
    }

    if (m_euc_kr_lead != 0x00u) {
        auto const lead = m_euc_kr_lead;
        optional<pointer_t> pointer = null;

        m_euc_kr_lead = 0x00u;

        if (between(byte, 0x41u, 0xFEu)) {
            pointer = (lead - 0x81u) * 190 + (byte - 0x41);
        }

        optional<code_point_t> const code_point =
            pointer == null ? null : index_code_point(m_index_euc_kr, *pointer);

        if (code_point != null) return *code_point;

        if (infra::is_ascii_byte(to_byte(byte))) {
            prepend(stream, byte);
        }

        return error;
    }

    if (infra::is_ascii_byte(to_byte(byte))) return code_point_t { byte };

    if (between(byte, 0x81u, 0xFEu)) {
        m_euc_kr_lead = byte;
        return continue_;
    }

    return error;
}

decoder_ptr euc_kr_decoder::
create() const
{
    return std::make_unique<euc_kr_decoder>();
}

/*
 * euc_kr_encoder
 */
euc_kr_encoder::
euc_kr_encoder()
    : m_index_euc_kr { "EUC-KR" }
{}

result_t euc_kr_encoder::
handler(stream_t&, token_t const code_point)
{
    if (code_point == end_of_stream) return finished;

    if (infra::is_ascii_code_point(code_point)) return to_byte(code_point);

    auto const pointer = index_pointer(m_index_euc_kr, code_point);

    if (pointer == null) return { error, code_point };

    auto const lead = *pointer / 190u + 0x81u;
    auto const trail = *pointer % 190u + 0x41u;

    return { to_byte(lead), to_byte(trail) };
}

encoder_ptr euc_kr_encoder::
create() const
{
    return std::make_unique<euc_kr_encoder>();
}

} // namespace whatwg::encoding::korean
