#include "encodings/chinese_traditional.hpp"

#include <whatwg/infra/byte.hpp>
#include <whatwg/infra/code_point.hpp>

namespace whatwg::encoding::chinese {

/*
 * big5_decoder
 */
big5_decoder::
big5_decoder()
    : m_index { "Big5" }
{}

result_t big5_decoder::
handler(byte_stream_t& stream, token_t const byte)
{
    if (byte == end_of_stream) {
        if (m_big5_lead != 0x00u) {
            m_big5_lead = 0x00u;
            return error;
        }
        else {
            return finished;
        }
    }

    if (m_big5_lead != 0x00u) {
        auto const lead = m_big5_lead;
        optional<pointer_t> pointer = null;
        m_big5_lead = 0x00u;

        auto const offset = byte < 0x7Fu ? 0x40u : 0x62u;

        if (between(byte, 0x40u, 0x7Eu) || between(byte, 0xA1u, 0xFEu)) {
            pointer = (lead - 0x81u) * 157u + (byte - offset);

            switch (*pointer) {
                case 1133u:
                    return { U'\xCA', U'\x304' };
                case 1135u:
                    return { U'\xCA', U'\x30C' };
                case 1164u:
                    return { U'\xEA', U'\x304' };
                case 1166u:
                    return { U'\xEA', U'\x30C' };
                default:
                    break;
            }
        }

        optional<code_point_t> const code_point =
            pointer == null ? null : index_code_point(m_index, *pointer);

        if (code_point != null) return *code_point;

        if (infra::is_ascii_byte(to_byte(byte))) {
            prepend(stream, byte);
        }

        return error;
    }

    if (infra::is_ascii_byte(to_byte(byte))) {
        return code_point_t { byte };
    }

    if (between(byte, 0x81u, 0xFEu)) {
        m_big5_lead = byte;
        return continue_;
    }

    return error;
}

decoder_ptr big5_decoder::
create() const
{
    return std::make_unique<big5_decoder>();
}

/*
 * big5_encoder
 */
big5_encoder::
big5_encoder()
{}

result_t big5_encoder::
handler(stream_t&, token_t const code_point)
{
    if (code_point == end_of_stream) return finished;

    if (infra::is_ascii_code_point(code_point)) {
        return to_byte(code_point);
    }

    auto const pointer = index_big5_pointer(code_point);

    if (pointer == null) return { error, code_point };

    auto const lead = *pointer / 157u + 0x81u;
    auto const trail = *pointer % 157u;

    auto const offset = trail < 0x3Fu ? 0x40u : 0x62u;

    return { to_byte(lead), to_byte(trail + offset) };
}

encoder_ptr big5_encoder::
create() const
{
    return std::make_unique<big5_encoder>();
}

} // namespace whatwg::encoding::chinese
