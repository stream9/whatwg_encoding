#include "types.hpp"

#include "algorithm.hpp"

#include <cassert>
#include <charconv>
#include <cstdint>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <system_error>
#include <vector>
#include <iomanip>

#include <boost/algorithm/string.hpp>

namespace fs = std::filesystem;
namespace ba = boost::algorithm;

namespace whatwg::encoding {

using pointer_t = uint32_t;

static byte_sequence_t
slurp_file(fs::path const& file_path)
{
    using ios = std::ios;

    std::ifstream ifs { file_path, ios::in | ios::binary | ios::ate };

    auto const length = ifs.tellg();
    ifs.seekg(0, ios::beg);

    byte_sequence_t text(length, '\0');
    ifs.read(text.data(), length);

    return text;
}

static auto
split(byte_sequence_view_t const s, char const sep)
{
    std::vector<byte_sequence_view_t> result;

    size_t start = 0, end = 0;

    while (end != s.npos) {
        end = s.find_first_of(sep, start);
        auto const length = end == s.npos ? s.npos : end - start;

        result.push_back(s.substr(start, length));

        start = end + 1;
    }

    return result;
}

static auto
lines(byte_sequence_view_t const s)
{
    return split(s, '\n');
}

static void
ltrim(byte_sequence_view_t& s)
{
    size_t count = 0;

    for (auto const c: s) {
        if (c != ' ' && c != '\r' && c != '\n' && c != '\t') {
            break;
        }
        ++count;
    }

    s.remove_prefix(count);
}

static pointer_t
parse_pointer(byte_sequence_view_t s)
{
    pointer_t pointer;

    ltrim(s);

    auto const [_, ec] = std::from_chars(s.begin(), s.end(), pointer);
    assert(ec == std::errc()); (void)ec;

    return pointer;
}

static code_point_t
parse_code_point(byte_sequence_view_t s)
{
    //code_point_t code_point;
    uint32_t code_point;

    if (infra::starts_with(s, "0x")) {
        s.remove_prefix(2);
    }

    auto const [_, ec] = std::from_chars(s.begin(), s.end(), code_point, 16);
    assert(ec == std::errc()); (void)ec;

    return code_point;
}

static auto
parse_line(byte_sequence_view_t const s)
{
    auto i = s.find_first_of(0x09); // TAB
    assert(i != s.npos);

    auto const pointer = s.substr(0, i);
    ++i;

    auto const j = s.find_first_of(0x09, i);

    auto const code_point = s.substr(i, j - i);

    return std::make_pair(
        parse_pointer(pointer),
        parse_code_point(code_point)
    );
}

template<typename Callback>
void
parse_index_file(fs::path const& file_path,
                 Callback callback)
{
    auto const& text = slurp_file(file_path);

    for (auto const line: lines(text)) {
        if (line.empty()) continue;
        if (starts_with(line, '\x23')) continue; // 0x23 = '#'

        callback(parse_line(line));
    }
}

byte_sequence_t
get_array_name(fs::path const file_path)
{
    byte_sequence_t name = file_path.stem().u8string();

    ba::replace_all(name, "-", "_");

    return name;
}

void convert_index(fs::path const& file_path)
{
    auto const name = get_array_name(file_path);

    std::cout << "static uint32_t const " << name << "[][2] = {\n";

    size_t num = 0, col = 0;

    parse_index_file(file_path,
        [&](auto const item) {
            auto const& [pointer, code_point] = item;

            if (col == 0) std::cout << "    ";

            std::cout << "{ " << std::setw(5) << pointer << ", "
                      << std::setw(6) << code_point << "}, ";

            if (++col == 4) {
                std::cout << std::endl;
                col = 0;
            }

            ++num;
        }
    );

    if (col != 0) std::cout << "\n";

    std::cout << "};\n\n"
        << "size_t constexpr num_" << name << " = " << num << ";" << std::endl;
}

} // namespace whatwg::encoding

int main(int argc, char* argv[])
{
    if (argc == 1) {
        std::cout << "Usage: " << argv[0] << " <filename>\n";
        return 1;
    }

    fs::path file_path { argv[1] };
    if (!fs::exists(file_path)) {
        std::cout << "file doesn't exist: " << argv[1] << std::endl;
        return 1;
    }

    whatwg::encoding::convert_index(file_path);

    return 0;
}

