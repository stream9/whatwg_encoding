#include "result.hpp"

#include <cassert>
#include <ostream>

namespace whatwg::encoding {

result_t::result_t(finished_t) : m_type { type::finished } {}
result_t::result_t(continue_t) : m_type { type::continue_ } {}
result_t::result_t(error_t) : m_type { type::error } {}

result_t::
result_t(error_t, code_point_t const c)
    : m_type { type::error }
    , m_length { 1 }
{
    m_buf[0] = c;
}

result_t::
result_t(token_t const t)
    : m_type { type::tokens }
    , m_length { 1 }
{
    m_buf[0] = t;
}

result_t::
result_t(token_t const t1, token_t const t2)
    : m_type { type::tokens }
    , m_length { 2 }
{
    m_buf[0] = t1;
    m_buf[1] = t2;
}

result_t::
result_t(token_t const t1, token_t const t2, token_t const t3)
    : m_type { type::tokens }
    , m_length { 3 }
{
    m_buf[0] = t1;
    m_buf[1] = t2;
    m_buf[2] = t3;
}

result_t::
result_t(token_t const t1, token_t const t2,
         token_t const t3, token_t const t4)
    : m_type { type::tokens }
    , m_length { 4 }
{
    m_buf[0] = t1;
    m_buf[1] = t2;
    m_buf[2] = t3;
    m_buf[3] = t4;
}

result_t::
result_t(byte_sequence_view_t const s)
    : m_type { type::tokens }
{
    assert(s.size() <= 4);

    m_length = static_cast<uint16_t>(s.size());

    for (size_t i = 0; i < m_length; ++i) {
        m_buf[i] = static_cast<unsigned char>(s[i]);
    }
}

bool result_t::
empty() const
{
    return m_length == 0;
}

size_t result_t::
size() const
{
    return m_length;
}

token_t result_t::
at(size_t const i) const
{
    return m_buf[i];
}

bool result_t::
is_finished() const
{
    return m_type == type::finished;
}

bool result_t::
is_continue() const
{
    return m_type == type::continue_;
}

bool result_t::
is_error() const
{
    return m_type == type::error;
}

bool result_t::
is_tokens() const
{
    return m_type == type::tokens;
}

result_t::const_iterator result_t::
begin() const
{
    return m_buf.begin();
}

result_t::const_iterator result_t::
end() const
{
    return m_buf.begin() + m_length;
}

token_t result_t::
operator[](size_t const i) const
{
    return at(i);
}

bool result_t::
operator==(result_t const& rhs) const
{
    auto is_same_buf = [&]() {
        for (size_t i = 0; i < m_length; ++i) {
            if (m_buf[i] != rhs.m_buf[i]) return false;
        }
        return true;
    };

    if (m_type == rhs.m_type) {
        if (m_length == rhs.m_length) {
            return is_same_buf();
        }
    }

    return false;
}

bool result_t::
operator!=(result_t const& rhs) const
{
    return !(*this == rhs);
}

// LCOV_EXCL_START
void result_t::
print_tokens(std::ostream& os) const
{
    os << std::hex << std::uppercase;
    os << "{ ";
    for (size_t i = 0; i < m_length; ++i) {
        os << "0x" << m_buf[i] << ", ";
    }
    os << "}";
}

std::ostream&
operator<<(std::ostream& os, result_t const& r)
{
    using type = result_t::type;

    switch (r.m_type) {
        case type::finished:
            os << finished;
            break;
        case type::continue_:
            os << continue_;
            break;
        case type::error:
            os << error;

            if (!r.empty()) {
                os << " ";
                r.print_tokens(os);
            }
            break;
        case type::tokens:
            r.print_tokens(os);
            break;
        default:
            assert(false);
            break;
    }

    return os;
}

std::ostream&
operator<<(std::ostream& os, finished_t const&)
{
    return os << "[finished]";
}

std::ostream&
operator<<(std::ostream& os, continue_t const&)
{
    return os << "[continue]";
}

std::ostream&
operator<<(std::ostream& os, error_t const&)
{
    return os << "[error]";
}
// LCOV_EXCL_STOP

} // namespace whatwg::encoding
