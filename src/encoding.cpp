#include "encoding.hpp"

#include "encoder_decoder.ipp"

#include "encodings/replacement.hpp"
#include "encodings/single_byte.hpp"
#include "encodings/utf16.hpp"
#include "encodings/utf8.hpp"
#include "encodings/chinese_simplified.hpp"
#include "encodings/chinese_traditional.hpp"
#include "encodings/euc_jp.hpp"
#include "encodings/iso_2022_jp.hpp"
#include "encodings/shift_jis.hpp"
#include "encodings/euc_kr.hpp"
#include "encodings/user_defined.hpp"

#include <algorithm>
#include <array>
#include <cassert>

#include <boost/algorithm/string.hpp>

#include <whatwg/infra/code_point.hpp>

namespace ba = boost::algorithm;

namespace whatwg::encoding {

using id = encoding::id;

static void
remove_leading_trailing_space(byte_sequence_view_t& sv)
{
    auto is_ascii_whitespace =
            [](auto c) { return infra::is_ascii_whitespace(c); };

    sv.remove_prefix(
        to_size(
            std::count_if(sv.begin(), sv.end(), is_ascii_whitespace)
        )
    );

    sv.remove_suffix(
        to_size(
            std::count_if(sv.rbegin(), sv.rend(), is_ascii_whitespace)
        )
    );
}

static byte_sequence_view_t
id_to_name(id const i)
{
    static char const* const names[] = {
        "UTF-8", "IBM866", "ISO-8859-2", "ISO-8859-3", "ISO-8859-4", "ISO-8859-5",
        "ISO-8859-6", "ISO-8859-7", "ISO-8859-8", "ISO-8859-8-I", "ISO-8859-10",
        "ISO-8859-13", "ISO-8859-14", "ISO-8859-15", "ISO-8859-16", "KOI8-R",
        "KOI8-U", "macintosh", "windows-874", "windows-1250", "windows-1251",
        "windows-1252", "windows-1253", "windows-1254", "windows-1255",
        "windows-1256", "windows-1257", "windows-1258", "x-mac-cyrillic", "GBK",
        "gb18030", "Big5", "EUC-JP", "ISO-2022-JP", "Shift_JIS", "EUC-KR",
        "replacement", "UTF-16BE", "UTF-16LE", "x-user-defined",
    };

    return names[static_cast<int>(i)];
}

static optional<id>
label_to_id(byte_sequence_view_t const label)
{
    struct item_t {
        char const* label;
        encoding::id id;
    };
    static item_t const table[] = {
        { "866", id::ibm866 },
        { "ansi_x3.4-1968", id::windows_1252 },
        { "arabic", id::iso_8859_6 },
        { "ascii", id::windows_1252 },
        { "asmo-708", id::iso_8859_6 },
        { "big5", id::big5 },
        { "big5-hkscs", id::big5 },
        { "chinese", id::gbk },
        { "cn-big5", id::big5 },
        { "cp1250", id::windows_1250 },
        { "cp1251", id::windows_1251 },
        { "cp1252", id::windows_1252 },
        { "cp1253", id::windows_1253 },
        { "cp1254", id::windows_1254 },
        { "cp1255", id::windows_1255 },
        { "cp1256", id::windows_1256 },
        { "cp1257", id::windows_1257 },
        { "cp1258", id::windows_1258 },
        { "cp819", id::windows_1252 },
        { "cp866", id::ibm866 },
        { "csbig5", id::big5 },
        { "cseuckr", id::euc_kr },
        { "cseucpkdfmtjapanese", id::euc_jp },
        { "csgb2312", id::gbk },
        { "csibm866", id::ibm866 },
        { "csiso2022jp", id::iso_2022_jp },
        { "csiso2022kr", id::replacement },
        { "csiso58gb231280", id::gbk },
        { "csiso88596e", id::iso_8859_6 },
        { "csiso88596i", id::iso_8859_6 },
        { "csiso88598e", id::iso_8859_8 },
        { "csiso88598i", id::iso_8859_8_i },
        { "csisolatin1", id::windows_1252 },
        { "csisolatin2", id::iso_8859_2 },
        { "csisolatin3", id::iso_8859_3 },
        { "csisolatin4", id::iso_8859_4 },
        { "csisolatin5", id::windows_1254 },
        { "csisolatin6", id::iso_8859_10 },
        { "csisolatin9", id::iso_8859_15 },
        { "csisolatinarabic", id::iso_8859_6 },
        { "csisolatincyrillic", id::iso_8859_5 },
        { "csisolatingreek", id::iso_8859_7 },
        { "csisolatinhebrew", id::iso_8859_8 },
        { "cskoi8r", id::koi8_r },
        { "csksc56011987", id::euc_kr },
        { "csmacintosh", id::macintosh },
        { "csshiftjis", id::shift_jis },
        { "cyrillic", id::iso_8859_5 },
        { "dos-874", id::windows_874 },
        { "ecma-114", id::iso_8859_6 },
        { "ecma-118", id::iso_8859_7 },
        { "elot_928", id::iso_8859_7 },
        { "euc-jp", id::euc_jp },
        { "euc-kr", id::euc_kr },
        { "gb18030", id::gb18030 },
        { "gb2312", id::gbk },
        { "gbk", id::gbk },
        { "gb_2312", id::gbk },
        { "gb_2312-80", id::gbk },
        { "greek", id::iso_8859_7 },
        { "greek8", id::iso_8859_7 },
        { "hebrew", id::iso_8859_8 },
        { "hz-gb-2312", id::replacement },
        { "ibm819", id::windows_1252 },
        { "ibm866", id::ibm866 },
        { "iso-2022-cn", id::replacement },
        { "iso-2022-cn-ext", id::replacement },
        { "iso-2022-jp", id::iso_2022_jp },
        { "iso-2022-kr", id::replacement },
        { "iso-8859-1", id::windows_1252 },
        { "iso-8859-10", id::iso_8859_10 },
        { "iso-8859-11", id::windows_874 },
        { "iso-8859-13", id::iso_8859_13 },
        { "iso-8859-14", id::iso_8859_14 },
        { "iso-8859-15", id::iso_8859_15 },
        { "iso-8859-16", id::iso_8859_16 },
        { "iso-8859-2", id::iso_8859_2 },
        { "iso-8859-3", id::iso_8859_3 },
        { "iso-8859-4", id::iso_8859_4 },
        { "iso-8859-5", id::iso_8859_5 },
        { "iso-8859-6", id::iso_8859_6 },
        { "iso-8859-6-e", id::iso_8859_6 },
        { "iso-8859-6-i", id::iso_8859_6 },
        { "iso-8859-7", id::iso_8859_7 },
        { "iso-8859-8", id::iso_8859_8 },
        { "iso-8859-8-e", id::iso_8859_8 },
        { "iso-8859-8-i", id::iso_8859_8_i },
        { "iso-8859-9", id::windows_1254 },
        { "iso-ir-100", id::windows_1252 },
        { "iso-ir-101", id::iso_8859_2 },
        { "iso-ir-109", id::iso_8859_3 },
        { "iso-ir-110", id::iso_8859_4 },
        { "iso-ir-126", id::iso_8859_7 },
        { "iso-ir-127", id::iso_8859_6 },
        { "iso-ir-138", id::iso_8859_8 },
        { "iso-ir-144", id::iso_8859_5 },
        { "iso-ir-148", id::windows_1254 },
        { "iso-ir-149", id::euc_kr },
        { "iso-ir-157", id::iso_8859_10 },
        { "iso-ir-58", id::gbk },
        { "iso8859-1", id::windows_1252 },
        { "iso8859-10", id::iso_8859_10 },
        { "iso8859-11", id::windows_874 },
        { "iso8859-13", id::iso_8859_13 },
        { "iso8859-14", id::iso_8859_14 },
        { "iso8859-15", id::iso_8859_15 },
        { "iso8859-2", id::iso_8859_2 },
        { "iso8859-3", id::iso_8859_3 },
        { "iso8859-4", id::iso_8859_4 },
        { "iso8859-5", id::iso_8859_5 },
        { "iso8859-6", id::iso_8859_6 },
        { "iso8859-7", id::iso_8859_7 },
        { "iso8859-8", id::iso_8859_8 },
        { "iso8859-9", id::windows_1254 },
        { "iso88591", id::windows_1252 },
        { "iso885910", id::iso_8859_10 },
        { "iso885911", id::windows_874 },
        { "iso885913", id::iso_8859_13 },
        { "iso885914", id::iso_8859_14 },
        { "iso885915", id::iso_8859_15 },
        { "iso88592", id::iso_8859_2 },
        { "iso88593", id::iso_8859_3 },
        { "iso88594", id::iso_8859_4 },
        { "iso88595", id::iso_8859_5 },
        { "iso88596", id::iso_8859_6 },
        { "iso88597", id::iso_8859_7 },
        { "iso88598", id::iso_8859_8 },
        { "iso88599", id::windows_1254 },
        { "iso_8859-1", id::windows_1252 },
        { "iso_8859-15", id::iso_8859_15 },
        { "iso_8859-1:1987", id::windows_1252 },
        { "iso_8859-2", id::iso_8859_2 },
        { "iso_8859-2:1987", id::iso_8859_2 },
        { "iso_8859-3", id::iso_8859_3 },
        { "iso_8859-3:1988", id::iso_8859_3 },
        { "iso_8859-4", id::iso_8859_4 },
        { "iso_8859-4:1988", id::iso_8859_4 },
        { "iso_8859-5", id::iso_8859_5 },
        { "iso_8859-5:1988", id::iso_8859_5 },
        { "iso_8859-6", id::iso_8859_6 },
        { "iso_8859-6:1987", id::iso_8859_6 },
        { "iso_8859-7", id::iso_8859_7 },
        { "iso_8859-7:1987", id::iso_8859_7 },
        { "iso_8859-8", id::iso_8859_8 },
        { "iso_8859-8:1988", id::iso_8859_8 },
        { "iso_8859-9", id::windows_1254 },
        { "iso_8859-9:1989", id::windows_1254 },
        { "koi", id::koi8_r },
        { "koi8", id::koi8_r },
        { "koi8-r", id::koi8_r },
        { "koi8-ru", id::koi8_u },
        { "koi8-u", id::koi8_u },
        { "koi8_r", id::koi8_r },
        { "korean", id::euc_kr },
        { "ksc5601", id::euc_kr },
        { "ksc_5601", id::euc_kr },
        { "ks_c_5601-1987", id::euc_kr },
        { "ks_c_5601-1989", id::euc_kr },
        { "l1", id::windows_1252 },
        { "l2", id::iso_8859_2 },
        { "l3", id::iso_8859_3 },
        { "l4", id::iso_8859_4 },
        { "l5", id::windows_1254 },
        { "l6", id::iso_8859_10 },
        { "l9", id::iso_8859_15 },
        { "latin1", id::windows_1252 },
        { "latin2", id::iso_8859_2 },
        { "latin3", id::iso_8859_3 },
        { "latin4", id::iso_8859_4 },
        { "latin5", id::windows_1254 },
        { "latin6", id::iso_8859_10 },
        { "logical", id::iso_8859_8_i },
        { "mac", id::macintosh },
        { "macintosh", id::macintosh },
        { "ms932", id::shift_jis },
        { "ms_kanji", id::shift_jis },
        { "replacement", id::replacement },
        { "shift-jis", id::shift_jis },
        { "shift_jis", id::shift_jis },
        { "sjis", id::shift_jis },
        { "sun_eu_greek", id::iso_8859_7 },
        { "tis-620", id::windows_874 },
        { "unicode-1-1-utf-8", id::utf_8 },
        { "us-ascii", id::windows_1252 },
        { "utf-16", id::utf_16le },
        { "utf-16be", id::utf_16be },
        { "utf-16le", id::utf_16le },
        { "utf-8", id::utf_8 },
        { "utf8", id::utf_8 },
        { "visual", id::iso_8859_8 },
        { "windows-1250", id::windows_1250 },
        { "windows-1251", id::windows_1251 },
        { "windows-1252", id::windows_1252 },
        { "windows-1253", id::windows_1253 },
        { "windows-1254", id::windows_1254 },
        { "windows-1255", id::windows_1255 },
        { "windows-1256", id::windows_1256 },
        { "windows-1257", id::windows_1257 },
        { "windows-1258", id::windows_1258 },
        { "windows-31j", id::shift_jis },
        { "windows-874", id::windows_874 },
        { "windows-949", id::euc_kr },
        { "x-cp1250", id::windows_1250 },
        { "x-cp1251", id::windows_1251 },
        { "x-cp1252", id::windows_1252 },
        { "x-cp1253", id::windows_1253 },
        { "x-cp1254", id::windows_1254 },
        { "x-cp1255", id::windows_1255 },
        { "x-cp1256", id::windows_1256 },
        { "x-cp1257", id::windows_1257 },
        { "x-cp1258", id::windows_1258 },
        { "x-euc-jp", id::euc_jp },
        { "x-gbk", id::gbk },
        { "x-mac-cyrillic", id::x_mac_cyrillic },
        { "x-mac-roman", id::macintosh },
        { "x-mac-ukrainian", id::x_mac_cyrillic },
        { "x-sjis", id::shift_jis },
        { "x-user-defined", id::x_user_defined },
        { "x-x-big5", id::big5 },
    };

    auto constexpr num = sizeof(table) / sizeof(item_t);

    auto* const begin = table;
    auto* end = &table[num];

    struct comp {
        bool operator()(item_t const& lhs, byte_sequence_view_t const rhs)
        {
            return ba::lexicographical_compare(lhs.label, rhs, m_less);
        }

        bool operator()(byte_sequence_view_t const& lhs, item_t const rhs)
        {
            return ba::lexicographical_compare(lhs, rhs.label, m_less);
        }

        ba::is_iless m_less;
    } is_less;

    auto const it = std::lower_bound(begin, end, label, is_less);
    if (it != end && !is_less(label, *it)) {
        return it->id;
    }
    else {
        return {};
    }
}

static encoder_ptr
make_encoder(id const id_)
{
    switch (id_) {
        case id::utf_8:
            return std::make_unique<utf8::encoder>();
        case id::ibm866: case id::iso_8859_2: case id::iso_8859_3:
        case id::iso_8859_4: case id::iso_8859_5: case id::iso_8859_6:
        case id::iso_8859_7: case id::iso_8859_8: case id::iso_8859_8_i:
        case id::iso_8859_10: case id::iso_8859_13: case id::iso_8859_14:
        case id::iso_8859_15: case id::iso_8859_16: case id::koi8_r:
        case id::koi8_u: case id::macintosh: case id::windows_874:
        case id::windows_1250: case id::windows_1251: case id::windows_1252:
        case id::windows_1253: case id::windows_1254: case id::windows_1255:
        case id::windows_1256: case id::windows_1257: case id::windows_1258:
        case id::x_mac_cyrillic:
            return std::make_unique<single_byte::encoder>(id_to_name(id_));
        case id::gbk:
            return std::make_unique<chinese::gbk_encoder>();
        case id::gb18030:
            return std::make_unique<chinese::gb18030_encoder>();
        case id::big5:
            return std::make_unique<chinese::big5_encoder>();
        case id::euc_jp:
            return std::make_unique<japanese::euc_jp_encoder>();
        case id::iso_2022_jp:
            return std::make_unique<japanese::iso_2022_jp_encoder>();
        case id::shift_jis:
            return std::make_unique<japanese::shift_jis_encoder>();
        case id::euc_kr:
            return std::make_unique<korean::euc_kr_encoder>();
        case id::x_user_defined:
            return std::make_unique<user_defined::encoder>();
        case id::replacement:
        case id::utf_16be:
        case id::utf_16le:
        default:
            break;
    }

    return nullptr;
}

static decoder_ptr
make_decoder(id const id_)
{
    switch (id_) {
        case id::utf_8:
        default:
            return std::make_unique<utf8::decoder>();
        case id::ibm866: case id::iso_8859_2: case id::iso_8859_3:
        case id::iso_8859_4: case id::iso_8859_5: case id::iso_8859_6:
        case id::iso_8859_7: case id::iso_8859_8: case id::iso_8859_8_i:
        case id::iso_8859_10: case id::iso_8859_13: case id::iso_8859_14:
        case id::iso_8859_15: case id::iso_8859_16: case id::koi8_r:
        case id::koi8_u: case id::macintosh: case id::windows_874:
        case id::windows_1250: case id::windows_1251: case id::windows_1252:
        case id::windows_1253: case id::windows_1254: case id::windows_1255:
        case id::windows_1256: case id::windows_1257: case id::windows_1258:
        case id::x_mac_cyrillic:
            return std::make_unique<single_byte::decoder>(id_to_name(id_));
        case id::gbk:
            return std::make_unique<chinese::gbk_decoder>();
        case id::gb18030:
            return std::make_unique<chinese::gb18030_decoder>();
        case id::big5:
            return std::make_unique<chinese::big5_decoder>();
        case id::euc_jp:
            return std::make_unique<japanese::euc_jp_decoder>();
        case id::iso_2022_jp:
            return std::make_unique<japanese::iso_2022_jp_decoder>();
        case id::shift_jis:
            return std::make_unique<japanese::shift_jis_decoder>();
        case id::euc_kr:
            return std::make_unique<korean::euc_kr_decoder>();
        case id::replacement:
            return std::make_unique<replacement::decoder>();
        case id::utf_16be:
            return std::make_unique<utf16::utf16be_decoder>();
        case id::utf_16le:
            return std::make_unique<utf16::utf16le_decoder>();
        case id::x_user_defined:
            return std::make_unique<user_defined::decoder>();
    }
}

/*
 * encoding
 */
encoding::
encoding(id const i)
    : m_id { i }
{}

byte_sequence_view_t encoding::
name() const
{
    return id_to_name(m_id);
}

encoder const* encoding::
encoder() const
{
    if (!m_encoder) {
        m_encoder = make_encoder(m_id);
    }

    return m_encoder.get();
}

decoder const& encoding::
decoder() const
{
    if (!m_decoder) {
        m_decoder = make_decoder(m_id);
    }

    return *m_decoder;
}

encoding const*
get_an_encoding(byte_sequence_view_t label)
{
    constexpr auto failure = nullptr;

    auto constexpr num_id = static_cast<int>(id::count);
    static std::array<optional<encoding>, num_id> table; //LCOV_EXCL_LINE

    remove_leading_trailing_space(label);

    auto const i = label_to_id(label);

    if (i) {
        auto& e = table[to_size(*i)];
        if (!e) e.emplace(*i);

        return &*e;
    }
    else {
        return failure;
    }
}

encoding const&
get_an_output_encoding(encoding const& e)
{
    if (e.name() == "replacement" ||
        e.name() == "UTF-16BE" || e.name() == "UTF-16LE")
    {
        auto const utf8 = get_an_encoding("UTF-8");
        assert(utf8);
        return *utf8;
    }

    return e;
}

code_point_stream_t
decode(byte_stream_t& stream, encoding const& fallback)
{
    byte_sequence_t buffer;
    bool m_bom_seen_flag = false;
    auto* encoding = &fallback;

    read(stream, buffer, 3);

    size_t constexpr num_row = 3;
    byte_sequence_view_t bom_table[num_row][2] {
        { "\xEF\xBB\xBF", "UTF-8"    },
        { "\xFE\xFF",     "UTF-16BE" },
        { "\xFF\xFE",     "UTF-16LE" },
    };

    for (size_t i = 0; i < num_row; ++i) {
        if (infra::starts_with(buffer, bom_table[i][0])) {
            encoding = get_an_encoding(bom_table[i][1]);
            m_bom_seen_flag = true;
            break;
        }
    }

    if (!m_bom_seen_flag) {
        prepend(stream, buffer);
    }
    else if (encoding->name() != "UTF-8" && buffer.size() == 3) {
        assert(m_bom_seen_flag);

        prepend(stream, to_token(buffer.back()));
    }

    code_point_stream_t output;

    run(encoding->decoder(), stream, output);

    return output;
}

code_point_stream_t
utf8_decode(byte_stream_t& stream)
{
    byte_sequence_t buffer;

    read(stream, buffer, 3);

    if (buffer != "\xEF\xBB\xBF") {
        prepend(stream, buffer);
    }

    code_point_stream_t output;

    run(utf8::decoder(), stream, output);

    return output;
}

code_point_stream_t
utf8_decode_without_bom(byte_stream_t& stream)
{
    code_point_stream_t output;

    run(utf8::decoder(), stream, output);

    return output;
}

optional<code_point_stream_t>
utf8_decode_without_bom_or_fail(byte_stream_t& stream)
{
    auto constexpr failure = std::nullopt;
    code_point_stream_t output;

    auto const potential_error =
        run(utf8::decoder(), stream, output, error_mode::fatal);

    if (potential_error.is_error()) return failure;

    return output;
}

byte_stream_t
encode(code_point_stream_t& stream, encoding const& encoding)
{
    assert(encoding.name() != "replacement" ||
           encoding.name() != "UTF-16BE" || encoding.name() != "UTF-16LE");

    byte_stream_t output;

    auto const* encoder = encoding.encoder();
    assert(encoder);

    run(*encoder, stream, output, error_mode::html);

    return output;
}

byte_stream_t
utf8_encode(code_point_stream_t& stream)
{
    byte_stream_t output;

    run(utf8::encoder(), stream, output, error_mode::html);

    return output;
}

} // namespace whatwg::encoding
