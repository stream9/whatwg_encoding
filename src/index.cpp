#include "index.hpp"

#include "algorithm.hpp"
#include "index_data.hpp"

#include <algorithm>
#include <cassert>
#include <mutex>
#include <vector>

#include <boost/container/flat_map.hpp>

namespace whatwg::encoding {

static byte_sequence_view_t
index_filename(byte_sequence_view_t const encoding_name)
{
    static char const* table[][2] = {
        { "IBM866", "index-ibm866.txt" },
        { "ISO-8859-2", "index-iso-8859-2.txt" },
        { "ISO-8859-3", "index-iso-8859-3.txt" },
        { "ISO-8859-4", "index-iso-8859-4.txt" },
        { "ISO-8859-5", "index-iso-8859-5.txt" },
        { "ISO-8859-6", "index-iso-8859-6.txt" },
        { "ISO-8859-7", "index-iso-8859-7.txt" },
        { "ISO-8859-8", "index-iso-8859-8.txt" },
        { "ISO-8859-8-I", "index-iso-8859-8.txt" },
        { "ISO-8859-10", "index-iso-8859-10.txt" },
        { "ISO-8859-13", "index-iso-8859-13.txt" },
        { "ISO-8859-14", "index-iso-8859-14.txt" },
        { "ISO-8859-15", "index-iso-8859-15.txt" },
        { "ISO-8859-16", "index-iso-8859-16.txt" },
        { "KOI8-R", "index-koi8-r.txt" },
        { "KOI8-U", "index-koi8-u.txt" },
        { "macintosh", "index-macintosh.txt" },
        { "windows-874", "index-windows-874.txt" },
        { "windows-1250", "index-windows-1250.txt" },
        { "windows-1251", "index-windows-1251.txt" },
        { "windows-1252", "index-windows-1252.txt" },
        { "windows-1253", "index-windows-1253.txt" },
        { "windows-1254", "index-windows-1254.txt" },
        { "windows-1255", "index-windows-1255.txt" },
        { "windows-1256", "index-windows-1256.txt" },
        { "windows-1257", "index-windows-1257.txt" },
        { "windows-1258", "index-windows-1258.txt" },
        { "x-mac-cyrillic", "index-x-mac-cyrillic.txt" },
        { "gb18030", "index-gb18030.txt" },
        { "Big5", "index-big5.txt" },
        { "jis0208", "index-jis0208.txt" },
        { "jis0212", "index-jis0212.txt" },
        { "ISO-2022-JP katakana", "index-iso-2022-jp-katakana.txt" },
        { "EUC-KR", "index-euc-kr.txt" },
    };

    auto constexpr num_table = sizeof(table) / sizeof(table[0]);
    auto constexpr last_index = num_table - 1;

    for (size_t i = 0; i < last_index; ++i) {
        if (table[i][0] == encoding_name)
            return table[i][1];
    }

    assert(encoding_name == table[last_index][0]);
    return table[last_index][1];
}

static auto const&
code_point_map(byte_sequence_view_t const filename)
{
    using code_point_map_t = std::unordered_map<pointer_t, code_point_t>;

    static boost::container::flat_map<byte_sequence_view_t, code_point_map_t> maps; //LCOV_EXCL_LINE
    static std::mutex mutex;

    std::lock_guard<std::mutex> guard(mutex);

    auto const it = maps.find(filename);
    if (it == maps.end()) {
        auto& map = maps[filename];
        auto const& data = index_data(filename);

        map.reserve(data.size());
        for (auto const item: data) {
            auto const pointer = item[0];
            auto const code_point = item[1];

            map.emplace(pointer, code_point);
        }

        return map;
    }
    else {
        return it->second;
    }
}

static auto const&
pointer_map(byte_sequence_view_t const filename)
{
    using pointer_map_t = std::unordered_multimap<code_point_t, pointer_t>;

    static boost::container::flat_map<byte_sequence_view_t, pointer_map_t> maps; //LCOV_EXCL_LINE
    static std::mutex mutex;

    std::lock_guard<std::mutex> guard(mutex);

    auto const it = maps.find(filename);
    if (it == maps.end()) {
        auto& map = maps[filename];
        auto const& data = index_data(filename);

        map.reserve(data.size());
        for (auto const item: data) {
            auto const pointer = item[0];
            auto const code_point = item[1];

            map.emplace(code_point, pointer);
        }

        return map;
    }
    else {
        return it->second;
    }
}

static std::vector<std::pair<pointer_t, code_point_t>>
index_gb18030_ranges(bool const sort_by_pointer)
{
    std::vector<std::pair<pointer_t, code_point_t>> table;

    auto const data = index_data("index-gb18030-ranges.txt");

    table.reserve(data.size());

    for (auto* const item: data) {
        auto const pointer = item[0];
        auto const code_point = item[1];

        table.emplace_back(pointer, code_point);
    }

    if (sort_by_pointer) {
        std::sort(table.begin(), table.end(),
            [](auto const& lhs, auto const& rhs) {
                return lhs.first < rhs.first;
            });
    }
    else {
        std::sort(table.begin(), table.end(),
            [](auto const& lhs, auto const& rhs) {
                return lhs.second < rhs.second;
            });
    }

    return table;
}

static auto
index_jis0208()
{
    std::unordered_multimap<code_point_t, pointer_t> table;

    auto const data = index_data("index-jis0208.txt");

    table.reserve(data.size());

    for (auto* const item: data) {
        auto const pointer = item[0];
        auto const code_point = item[1];

        if (between(pointer, 8272u, 8835u)) continue;

        table.emplace(code_point, pointer);
    }

    return table;
}

static auto
index_big5()
{
    boost::container::flat_multimap<code_point_t, pointer_t> table;

    auto const data = index_data("index-big5.txt");

    for (auto* const item: data) {
        auto const pointer = item[0];
        auto const code_point = item[1];

        if (pointer < (0xA1u - 0x81u) * 157) continue;

        table.emplace(code_point, pointer);
    }

    return table;
}

template<typename Ranges, typename Value, typename Compare>
auto const&
last_item_that_is_equal_to_or_less_than(
    Ranges const& ranges,
    Value const value,
    Compare const comp)
{
    auto const it = std::lower_bound(
                    ranges.begin(), ranges.end(), value, comp);
    assert(it != ranges.begin());
    assert(it != ranges.end());

    return *(it - 1);
}

template<typename MultiMap>
pointer_t
last_pointer(MultiMap const& index, code_point_t const code_point)
{
    auto it = index.upper_bound(code_point);

    assert(it != index.begin() && it != index.end());

    return (--it)->second;
}

template<typename MultiMap>
optional<pointer_t>
index_pointer(MultiMap const& index, code_point_t const code_point)
{
    auto const it = index.find(code_point);
    if (it == index.end()) {
        return null;
    }
    else {
        return it->second;
    }
}

/*
 * code_point_index
 */
code_point_index::
code_point_index(byte_sequence_view_t const encoding_name)
{
    auto const filename = index_filename(encoding_name);
    assert(!filename.empty());

    m_map = &code_point_map(filename);
}

optional<code_point_t> code_point_index::
find(pointer_t const p) const
{
    auto const it = m_map->find(p);
    if (it == m_map->end()) {
        return null;
    }
    else {
        return it->second;
    }
}

/*
 * pointer_index
 */
pointer_index::
pointer_index(byte_sequence_view_t const encoding_name)
{
    auto const filename = index_filename(encoding_name);
    assert(!filename.empty());

    m_map = &pointer_map(filename);
}

optional<pointer_t> pointer_index::
find(code_point_t const c) const
{
    auto const it = m_map->find(c);
    if (it == m_map->end()) {
        return null;
    }
    else {
        return it->second;
    }
}

/*
 * free function
 */
optional<code_point_t>
index_code_point(code_point_index const& index, pointer_t const p)
{
    return index.find(p);
}

optional<pointer_t>
index_pointer(pointer_index const& index, code_point_t const c)
{
    return index.find(c);
}

optional<code_point_t>
index_gb18030_ranges_code_point(pointer_t const pointer)
{
    static auto const ranges = index_gb18030_ranges(true); //LCOV_EXCL_LINE

    if ((pointer > 39419u && pointer < 189000u) || pointer > 1237575u) {
        return null;
    }

    if (pointer == 7457u) return U'\xE7C7';

    auto const last_item = last_item_that_is_equal_to_or_less_than(
        ranges, pointer,
        [](auto const& lhs, auto const& rhs) {
            return lhs.first <= rhs;
        });

    auto const& [offset, code_point_offset] = last_item;

    return code_point_t { code_point_offset + pointer - offset };
}

optional<pointer_t>
index_gb18030_ranges_pointer(code_point_t const code_point)
{
    static auto const ranges = index_gb18030_ranges(false); // LCOV_EXCL_LINE

    if (code_point == U'\xE7C7') return 7457u;

    auto const last_item = last_item_that_is_equal_to_or_less_than(
        ranges, code_point,
        [](auto const& lhs, auto const& rhs) {
            return lhs.second <= rhs;
        });

    auto const& [pointer_offset, offset] = last_item;

    return pointer_t { pointer_offset + code_point - offset };
}

optional<pointer_t>
index_shift_jis_pointer(code_point_t const code_point)
{
    static auto const index = index_jis0208(); // LCOV_EXCL_LINE

    return index_pointer(index, code_point);
}

optional<pointer_t>
index_big5_pointer(code_point_t const code_point)
{
    static auto const index = index_big5(); // LCOV_EXCL_LINE

    switch (code_point) {
        case 0x2550u:
        case 0x255Eu:
        case 0x2561u:
        case 0x256Au:
        case 0x5341u:
        case 0x5345u:
            return last_pointer(index, code_point);
        default:
            break;
    }

    return index_pointer(index, code_point);
}

} // namespace whatwg::encoding
