#include "text_decoder.hpp"

#include "encoder_decoder.ipp"
#include "error.hpp"
#include "exception.hpp"
#include "stream.hpp"

namespace whatwg::encoding {

text_decoder::
text_decoder(byte_sequence_view_t const label/*= "utf-8"*/,
             text_decoder::options const o /*= {}*/)
{
    auto const encoding = get_an_encoding(label);

    if (!encoding || encoding->name() == "replacement") {
        THROW(std::system_error(errc::range_error));
    }

    this->encoding(*encoding);

    if (test(o, options::fatal)) {
        this->error_mode(error_mode::fatal);
    }

    if (test(o, options::ignore_bom)) {
        this->ignore_bom(true);
    }
}

string_t text_decoder::
decode(byte_sequence_view_t const input/*= ""*/,
       bool stream/*= false*/)
{
    assert(m_decoder);

    if (!m_do_not_flush_flag) {
        m_decoder = this->encoding().decoder().create();
        m_stream = "";
        this->bom_seen(false);
    }

    if (stream) {
        m_do_not_flush_flag = true;
    }
    else {
        m_do_not_flush_flag = false;
    }

    if (!input.empty()) {
        push(m_stream, input);
    }

    stream_t output;

    while (true) {
        auto const token = read(m_stream);

        if (token == end_of_stream && m_do_not_flush_flag) {
            return this->serialize_stream(output);
        }
        else {
            auto const result = process(
                    *m_decoder, token, m_stream, output, this->error_mode());

            if (result.is_finished()) {
                return this->serialize_stream(output);
            }
            else if (result.is_error()) {
                THROW(std::system_error(errc::type_error));
            }
        }
    }
}

} // namespace whatwg::encoding
