#include "index_data.hpp"

#include "index_ibm866.hpp"
#include "index_iso_8859_2.hpp"
#include "index_iso_8859_3.hpp"
#include "index_iso_8859_4.hpp"
#include "index_iso_8859_5.hpp"
#include "index_iso_8859_6.hpp"
#include "index_iso_8859_7.hpp"
#include "index_iso_8859_8.hpp"
#include "index_iso_8859_10.hpp"
#include "index_iso_8859_13.hpp"
#include "index_iso_8859_14.hpp"
#include "index_iso_8859_15.hpp"
#include "index_iso_8859_16.hpp"
#include "index_koi8_r.hpp"
#include "index_koi8_u.hpp"
#include "index_macintosh.hpp"
#include "index_windows_874.hpp"
#include "index_windows_1250.hpp"
#include "index_windows_1251.hpp"
#include "index_windows_1252.hpp"
#include "index_windows_1253.hpp"
#include "index_windows_1254.hpp"
#include "index_windows_1255.hpp"
#include "index_windows_1256.hpp"
#include "index_windows_1257.hpp"
#include "index_windows_1258.hpp"
#include "index_x_mac_cyrillic.hpp"
#include "index_gb18030.hpp"
#include "index_gb18030_ranges.hpp"
#include "index_big5.hpp"
#include "index_jis0208.hpp"
#include "index_jis0212.hpp"
#include "index_iso_2022_jp_katakana.hpp"
#include "index_euc_kr.hpp"

namespace whatwg::encoding {

template<class T> struct typeis;

template<typename T>
auto
make_range(T const begin, size_t const length)
{
    return boost::make_iterator_range(begin, begin + length);
}

index_data_range
index_data(byte_sequence_view_t const filename)
{
    if (filename == "index-ibm866.txt") {
        return make_range(index_ibm866, num_index_ibm866);
    }
    else if (filename == "index-iso-8859-2.txt") {
        return make_range(index_iso_8859_2, num_index_iso_8859_2);
    }
    else if (filename == "index-iso-8859-3.txt") {
        return make_range(index_iso_8859_3, num_index_iso_8859_3);
    }
    else if (filename == "index-iso-8859-4.txt") {
        return make_range(index_iso_8859_4, num_index_iso_8859_4);
    }
    else if (filename == "index-iso-8859-5.txt") {
        return make_range(index_iso_8859_5, num_index_iso_8859_5);
    }
    else if (filename == "index-iso-8859-6.txt") {
        return make_range(index_iso_8859_6, num_index_iso_8859_6);
    }
    else if (filename == "index-iso-8859-7.txt") {
        return make_range(index_iso_8859_7, num_index_iso_8859_7);
    }
    else if (filename == "index-iso-8859-8.txt") {
        return make_range(index_iso_8859_8, num_index_iso_8859_8);
    }
    else if (filename == "index-iso-8859-10.txt") {
        return make_range(index_iso_8859_10, num_index_iso_8859_10);
    }
    else if (filename == "index-iso-8859-13.txt") {
        return make_range(index_iso_8859_13, num_index_iso_8859_13);
    }
    else if (filename == "index-iso-8859-14.txt") {
        return make_range(index_iso_8859_14, num_index_iso_8859_14);
    }
    else if (filename == "index-iso-8859-15.txt") {
        return make_range(index_iso_8859_15, num_index_iso_8859_15);
    }
    else if (filename == "index-iso-8859-16.txt") {
        return make_range(index_iso_8859_16, num_index_iso_8859_16);
    }
    else if (filename == "index-koi8-r.txt") {
        return make_range(index_koi8_r, num_index_koi8_r);
    }
    else if (filename == "index-koi8-u.txt") {
        return make_range(index_koi8_u, num_index_koi8_u);
    }
    else if (filename == "index-macintosh.txt") {
        return make_range(index_macintosh, num_index_macintosh);
    }
    else if (filename == "index-windows-874.txt") {
        return make_range(index_windows_874, num_index_windows_874);
    }
    else if (filename == "index-windows-1250.txt") {
        return make_range(index_windows_1250, num_index_windows_1250);
    }
    else if (filename == "index-windows-1251.txt") {
        return make_range(index_windows_1251, num_index_windows_1251);
    }
    else if (filename == "index-windows-1252.txt") {
        return make_range(index_windows_1252, num_index_windows_1252);
    }
    else if (filename == "index-windows-1253.txt") {
        return make_range(index_windows_1253, num_index_windows_1253);
    }
    else if (filename == "index-windows-1254.txt") {
        return make_range(index_windows_1254, num_index_windows_1254);
    }
    else if (filename == "index-windows-1255.txt") {
        return make_range(index_windows_1255, num_index_windows_1255);
    }
    else if (filename == "index-windows-1256.txt") {
        return make_range(index_windows_1256, num_index_windows_1256);
    }
    else if (filename == "index-windows-1257.txt") {
        return make_range(index_windows_1257, num_index_windows_1257);
    }
    else if (filename == "index-windows-1258.txt") {
        return make_range(index_windows_1258, num_index_windows_1258);
    }
    else if (filename == "index-x-mac-cyrillic.txt") {
        return make_range(index_x_mac_cyrillic, num_index_x_mac_cyrillic);
    }
    else if (filename == "index-gb18030.txt") {
        return make_range(index_gb18030, num_index_gb18030);
    }
    else if (filename == "index-gb18030-ranges.txt") {
        return make_range(index_gb18030_ranges, num_index_gb18030_ranges);
    }
    else if (filename == "index-big5.txt") {
        return make_range(index_big5, num_index_big5);
    }
    else if (filename == "index-jis0208.txt") {
        return make_range(index_jis0208, num_index_jis0208);
    }
    else if (filename == "index-jis0212.txt") {
        return make_range(index_jis0212, num_index_jis0212);
    }
    else if (filename == "index-iso-2022-jp-katakana.txt") {
        return make_range(index_iso_2022_jp_katakana, num_index_iso_2022_jp_katakana);
    }
    else {
        assert(filename == "index-euc-kr.txt");
        return make_range(index_euc_kr, num_index_euc_kr);
    }
}

} // namespace whatwg::encoding
