#include "error.hpp"

#include <cassert>

namespace whatwg::encoding {

class encoding_category_impl : public std::error_category
{
public:
    char const* name() const noexcept override
    {
        return "WHATWG Encoding";
    }

    std::string message(int const code) const noexcept override
    {
        switch (static_cast<errc>(code)) {
            case errc::stream_read_error:
                return "internal error happens while reading from stream";
            case errc::stream_set_error:
                return "internal error happens while setting data into stream";
            case errc::stream_prepend_error:
                return "internal error happens while prepending data to stream";
            default:
                return "unknown error";
        }
    }
};

std::error_category const&
encoding_category()
{
    static encoding_category_impl impl; // LCOV_EXCL_LINE

    return impl;
}

} // namespace whatwg::encoding
