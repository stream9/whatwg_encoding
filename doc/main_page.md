# Overview
## Namespace
::whatwg::encoding

## High level API

Name                                               | Description          | Header |
---------------------------------------------------|----------------------|--------------
[TextDecoder](@ref whatwg::encoding::text_decoder) | Generic text decoder | <whatwg/encoding/text_decoder.hpp>
[TextEncoder](@ref whatwg::encoding::text_encoder) | Generic text encoder | <whatwg/encoding/text_encoder.hpp>

## Low level API

Name                                                    | Description | Header |
--------------------------------------------------------|---------------------------------------------------------------|-------------------------------------
[get an encoding](@ref get_an_encoding())               | Get an encoding object.                                       | <whatwg/encoding/encoding.hpp>
[get an output encoding](@ref get_an_output_encoding()) | Get an encoding object for output.                            | ^
[run](@ref run())                                       | Run an encoding's decoder or encoder against an input stream. | <whatwg/encoding/encoder_decoder.hpp>
[process](@ref process())                               | Process a token with an encoding's encoder or decoder.        | ^

## Hooks for standards

Name                                                       | Description | Header |
-----------------------------------------------------------|-----------------------------------------|-----------------
[decode](@ref decode())                                    | Decode input stream.                    | <whatwg/encoding/encoding.hpp>
[UTF-8 decode](@ref utf8_decode())                         | Decode input stream with UTF-8 encoding.| ^
[UTF-8 decode without BOM](@ref utf8_decode_without_bom()) | ^                                       | ^
[UTF-8 decode without BOM or fail](@ref utf8_decode_without_bom_or_fail()) | ^                       | ^
[encode](@ref encode())                                    | Encode input stream.                    | ^
[UTF-8 encode](@ref utf8_encode())                         | Encode input stream with UTF-8 encoding.| ^
